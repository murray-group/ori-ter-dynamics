function [matp_mid,matp_pole,matp_mixd,matp_rest]=sort_matp_localization_at_division(cycle_list,complete_cycle_ids,fluor_channel)
matp_mid=[];
matp_pole=[];
matp_mixd=[];
matp_rest=[];
count=0;
fc=strcat('foci',num2str(fluor_channel));
for i=1:numel(complete_cycle_ids)
    cycle=cycle_list(complete_cycle_ids(i));
    cycle.cycle_id
    if isfield(cycle.(fc),'inferred_position_relative') && ~isempty(cycle.(fc).inferred_position_relative)
        pos=abs(cycle.(fc).inferred_position_relative(end,:,1));
        if any(~isnan(pos))
            count=count+1;
            if size(cycle.(fc).start_and_stop,1)==2 & sum(~isnan(pos))==2 & pos(~isnan(pos))>=cycle.lengths(end)/6
                matp_pole=[matp_pole cycle.cycle_id];
            elseif pos(~isnan(pos))<cycle.lengths(end)/6
                matp_mid=[matp_mid cycle.cycle_id];
            elseif size(cycle.(fc).start_and_stop,1)==2 & sum(~isnan(pos))==2 & any(pos(~isnan(pos))>=cycle.lengths(end)/6) & any(pos(~isnan(pos))<cycle.lengths(end)/6)
                matp_mixd=[matp_mixd cycle.cycle_id];
            else
                matp_rest=[matp_rest cycle.cycle_id];
            end
        end
    end
end
count

disp("Population with MatP at pole")
numel(matp_pole)/count
disp("Population with MatP at mid-cell")
numel(matp_mid)/count
disp("Population with MatP at mid-cell and pole")
numel(matp_mixd)/count
end