function paired_difference(data1,data2,range,quantiles,datalabels,Ylabel)

%paired difference plot, optimised for discrete data
%range is the left y-axis limits

%the lower and upper percentiles to remove
lp=quantiles(1);
up=quantiles(2);


full=1;%1 for full violin, 0 for half
markersize=7;%marker size for dots on distribution

interval=min(diff(unique(data1)));%whats the discrete interval of the data, i.e. the data is n*interval where n is an integer

small_padding=0.002;
big_padding=0.005;

%remove the top and bottom percentile
tmp=data1(data1<=quantile(data1,up) & data2<=quantile(data2,up) & data1>=quantile(data1,lp) & data2>=quantile(data2,lp));
data2=data2(data1<=quantile(data1,up) & data2<=quantile(data2,up)& data1>=quantile(data1,lp) & data2>=quantile(data2,lp));
data1=tmp;

get_edges=@(x) [(min(x)-interval):interval:max(x)]+interval/2;


f1=histcounts(data1,get_edges(data1),'Normalization','pdf');
x1=min(data1):interval:max(data1);

f2=histcounts(data2,get_edges(data2),'Normalization','pdf');
x2=min(data2):interval:max(data2);


d=data2-data1;
fd=histcounts(d,get_edges(d),'Normalization','pdf');
xd=min(d):interval:max(d);


    disp(['Mean,SD and median of ' datalabels{1} ' are ' num2str(mean(data1)) ', ' num2str(std(data1)) ' and ' num2str(median(data1)) ' respectively'])
    disp(['Mean,SD and median of ' datalabels{2} ' are ' num2str(mean(data2)) ', ' num2str(std(data2)) ' and ' num2str(median(data2)) ' respectively'])
    disp(['Mean,SD and median of difference are ' num2str(mean(d)) ', ' num2str(std(d)) ' and ' num2str(median(d)) ' respectively'])


figure;
clf;
c=lines(3);%standard colours

%first axis, the data distributions

if full==0
    patch([0,f1,0],[x1(1),x1,x1(end)],c(1,:))

    patch([0,f2,0]+max(f1)+big_padding,[x2(1),x2,x2(end)],c(2,:))
else
    patch([f1,-fliplr(f1)],[x1,fliplr(x1)],c(1,:))
    
    hold on;  
        plot([f1,-fliplr(f1)],[x1,fliplr(x1)],'.k','MarkerSize',markersize)
    hold off;

    patch([f2,-fliplr(f2)]+max(f1)+big_padding+full*max(f2),[x2,fliplr(x2)],c(2,:))
    
    hold on;  
        plot([f2,-fliplr(f2)]+max(f1)+big_padding+full*max(f2),[x2,fliplr(x2)],'.k','MarkerSize',markersize)
    hold off;
end
%axisrange=[min(x1(1),x2(2))-30,max(x1(end),x2(end))];% the range on the y axis
axisrange=range;
ylim(axisrange);

xlim([-small_padding-max(f1)*full,max(f1)+(full+1)*(max(f2))+big_padding+small_padding]);%% the range on the x axis

ylabel(Ylabel);
xticks([(1-full)*(max(f1)/2),max(f1)+big_padding+(full+1)*max(f2)/2]);
xticklabels({datalabels{1},datalabels{2}})

ax1 = gca;
ax1.Position(3)=0.45;%reduce the width of the axis. normal width is 0.77 of the figure.
ax1.Color="none";


% second axis, the difference distribution
ax2_pos = ax1.Position;
ax2_pos(1)=ax2_pos(1)+0.5;%move the axis to the right by 0.5
ax2_pos(3)=0.25; %set the width
ax2 = axes('Position', ax2_pos, 'XAxisLocation', 'bottom', 'YAxisLocation', 'right',"Color","none");% create the axis


patch(ax2,[0,-fd,0],[xd(1),xd,xd(end)],c(3,:))

hold on;

plot(-fd,xd,'.k','MarkerSize',markersize)

plot(ax2,0,nanmedian(d),'.k','MarkerSize',20);
% L=sum(~isnan(d));
% SEM = nanstd(d)/sqrt(L);               % Standard Error
% ts = tinv([0.025  0.975],L-1);      % T-Score
% CI = nanmean(d) + ts*SEM; 
%CI=bootci(1000,@median,data);%%bootstrap for median,
% line(ax2,[0,0],CI,'LineWidth',1,'Color','k');
hold off;


xlim(ax2,[-small_padding-max(fd),0]);%the range of the x axis
ylim(ax2,axisrange-median(data1));%the range of the y axis, off-set by median(data1) from that of ax1

%yticks are not showing at the appropriate intervals, fix
ax2.YTick=ax1.YTick;
delta=ax2.YTick(2)-ax2.YTick(1);
ax2.YTick=[fliplr(-delta:(-delta):ax2.YLim(1)),0:delta:ax2.YLim(2)];


%ax2.TickDir='in';
ax2.Layer = 'top';%put the ticks on top of the patch
xticks(ax2,[-(max(f2)+small_padding)/2]);
xticklabels({datalabels{3}})

ylabel(ax2,'difference');



% %make a third axis for the lines
ax3_pos = ax1.Position;
ax3_pos(3)=0.75;%same as the first but the combined width of the other two plus the gap.
ax3 = axes('Position', ax3_pos, 'XAxisLocation', 'bottom', 'YAxisLocation', 'right',"Color","none",'Visible',0);
ax3.YLim=ax1.YLim;% y ange is the same as ax1
xlim(ax3,ax1.XLim(1)+[0,0.75/0.45*diff(ax1.XLim)]) %set the x range to match the scaling of ax1

%draw the lines
line(ax3,[0,ax3.XLim(2)],[median(data1),median(data1)],'Color','k')
line(ax3,[max(f1)+big_padding+full*max(f2),ax3.XLim(2)],[median(data2),median(data2)],'Color','k')

end

