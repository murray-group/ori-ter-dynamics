function plot_fluor_intensity_vs_time(cycle_list,cycle_ids,fluor_channel)
fc=cat(2,'fluor',num2str(fluor_channel),'_lp');

    time_max = 0;
    %cycle_ids = 1:numel(cycle_list);
    for i = 1:numel(cycle_ids) %% To find out total number of frames
        cycle = cycle_list(cycle_ids(i));
        time = cycle.times(end);
        if time>time_max,time_max=time;end
    end
    time_count = zeros(1,time_max); %% To count the total number of cells in each time point bins
    for i = 1:numel(cycle_ids)
        cycle = cycle_list(cycle_ids(i));
        time_count(cycle.times(1):cycle.times(end)) = time_count(cycle.times(1):cycle.times(end))+1;
    end
    time_vs_length = cell(1,time_max);
    for i = 1:time_max
        time_vs_length{i} = zeros(1,time_count(i));
    end
    time_count = ones(1,time_max);
    for i = 1:numel(cycle_ids)
        cycle = cycle_list(cycle_ids(i));
        
        
        
        
        intensities=cellfun(@mean, cycle.(fc));
        for j = 1:numel(cycle.times)
            t = cycle.times(j);
            I=intensities(j);
            time_vs_length{t}(time_count(t)) = I;
            time_count(t) = time_count(t) + 1;
        end
    end
    x = 1:10:time_max;
    means = cellfun(@mean,time_vs_length);
    std = cellfun(@std,time_vs_length);
    counts = cellfun(@numel,time_vs_length);
    ste = std./sqrt(counts);
    figure
    plot_with_err(x,means(x),ste(x),[0,0,1],0.2);
    xlabel('Time (frame number)')
    ylabel('Fluorescence intensity');
    legend('Intensity','SEM')
    title('Fluorescence intensity vs time')
end