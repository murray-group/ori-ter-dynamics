function plot_avg_fluor_intensity_vs_time(cycle_list,cycle_ids,fluor_channel)
fc=cat(2,'fluor',num2str(fluor_channel),'_lp');
time_max = 0;
for i = 1:numel(cycle_ids) %% To find out total number of frames
    cycle = cycle_list(cycle_ids(i));
    time = cycle.times(end);
    if time>time_max,time_max=time;end
end
time_count = zeros(1,time_max); %% To count the total number of cells in each time point bins
for i = 1:numel(cycle_ids)
    cycle = cycle_list(cycle_ids(i));
    time_count(cycle.times(end)) = time_count(cycle.times(end))+1;
end
time_vs_fluor = cell(1,time_max);
for i = 1:time_max
    time_vs_fluor{i} = zeros(1,time_count(i));
end
time_count = ones(1,time_max);
for i = 1:numel(cycle_ids)
    cycle = cycle_list(cycle_ids(i));
    t=cycle.times(end);
    time_vs_fluor{t}(time_count(t))=mean(cellfun(@mean, cycle.(fc)));
    time_count(t)=time_count(t)+1;
end
x = 1:10:time_max;
means = cellfun(@mean,time_vs_fluor);
std = cellfun(@std,time_vs_fluor);
counts = cellfun(@numel,time_vs_fluor);
ste = std./sqrt(counts);
figure
plot_with_err(x,means(x),ste(x),[0,0,1],0.2);
xlabel('Sampling time (frame)')
ylabel('Mean Fluorescence Intensity');
legend('Intensity','SEM')
title('Average cell cycle fluorescence intensity vs time')
end
