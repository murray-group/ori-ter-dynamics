function plot_avg_duration_rate_vs_time(cycle_list,cycle_ids)
%growth_rates=growth_rate(cycle_list,cycle_ids);
time_max = 0;
for i = 1:numel(cycle_ids) %% To find out total number of frames
    cycle = cycle_list(cycle_ids(i));
    time = cycle.times(end);
    if time>time_max,time_max=time;end
end
time_count = zeros(1,time_max); %% To count the total number of cells in each time point bins
for i = 1:numel(cycle_ids)
    cycle = cycle_list(cycle_ids(i));
    time_count(cycle.times(1):cycle.times(end)) = time_count(cycle.times(1):cycle.times(end))+1;
end
time_vs_growth = cell(1,time_max);
for i = 1:time_max
    time_vs_growth{i} = zeros(1,time_count(i));
end
time_count = ones(1,time_max);
for i = 1:numel(cycle_ids)
    
    cycle = cycle_list(cycle_ids(i));
    duration=repmat(cycle.duration,[1 cycle.duration]);
    for j = 1:numel(cycle.times)
        t = cycle.times(j);
        g = duration(j);
        time_vs_growth{t}(time_count(t)) = g;
        time_count(t) = time_count(t) + 1;
    end
end
x = 1:10:time_max;
means = cellfun(@mean,time_vs_growth);
std = cellfun(@std,time_vs_growth);
counts = cellfun(@numel,time_vs_growth);
ste = std./sqrt(counts);
%figure(1)
plot_with_err(x,means(x),ste(x),[0,0,1],0.2);
xlabel('Sampling time (frame)')
ylabel('Duration (frames)');
legend('Duration','SEM')
title('Average cell cycle duration vs time')
end

function growth_rates=growth_rate(cycle_list,cycle_ids)

    growth_rates = zeros(numel(cycle_ids),1);
    for i = 1:numel(cycle_ids)
        cycle_id = cycle_ids(i);
        cycle = cycle_list(cycle_id);
        if cycle.times(1) > -1
            areas = log(cycle.areas);
            mdl = fitlm(1:numel(areas),areas);
            growth_rate = mdl.Coefficients{2,1};
            growth_rates(i) = growth_rate;
        end
    end
end