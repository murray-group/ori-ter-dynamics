
function plot_growth_rate_vs_time(cycle_list,cycle_ids)

time_max = 0;
for i = 1:numel(cycle_ids) %% To find out total number of frames
    cycle = cycle_list(cycle_ids(i));
    time = cycle.times(end);
    if time>time_max,time_max=time;end
end
time_count = zeros(1,time_max); %% To count the total number of cells in each time point bins
for i = 1:numel(cycle_ids)
    cycle = cycle_list(cycle_ids(i));
    time_count(cycle.times(1):cycle.times(end)) = time_count(cycle.times(1):cycle.times(end))+1;
end
time_vs_growth = cell(1,time_max);
for i = 1:time_max
    time_vs_growth{i} = zeros(1,time_count(i));
end
time_count = ones(1,time_max);
for i = 1:numel(cycle_ids)
    
    cycle = cycle_list(cycle_ids(i));
    growth=zeros(1,cycle.duration);
    growth(2:end-1)=(cycle.areas(3:end)./cycle.areas(1:end-2))/2;
    growth(1)=cycle.areas(2)/cycle.areas(1)/2;
    growth(end)=cycle.areas(end)/cycle.areas(end-1)/2;
    
    for j = 1:numel(cycle.times)
        t = cycle.times(j);
        g = log(growth(j))./5;
        time_vs_growth{t}(time_count(t)) = g;
        time_count(t) = time_count(t) + 1;
    end
end
x = 1:10:time_max;
means = cellfun(@mean,time_vs_growth);
std = cellfun(@std,time_vs_growth);
counts = cellfun(@numel,time_vs_growth);
ste = std./sqrt(counts);
figure
plot_with_err(x,means(x),ste(x),[0,0,1],0.2);
xlabel('Sampling time (frame)')
ylabel('Growth rate (min^-1)');
legend('Growth rate','SEM')
title('Growth rate vs time')
end

