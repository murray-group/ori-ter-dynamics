%% Script to sort out cells with high fluorescence signal

function [cycle_ids_fluor_filtered]=fluor_filtering(cycle_list,complete_cycle_ids,fluor_channel,percentile)

mean_intensities=mean_intensities_of_cell_cycles(cycle_list,complete_cycle_ids,fluor_channel);
threshold=prctile(mean_intensities,percentile);
cycle_ids_fluor_filtered=[];
for i=1:numel(complete_cycle_ids)
    if mean_intensities(i)<=threshold
        cycle_ids_fluor_filtered=[cycle_ids_fluor_filtered;complete_cycle_ids(i)];
    end
end
end

    