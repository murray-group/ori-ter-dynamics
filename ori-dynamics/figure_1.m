%% Script for figure 1

%% Identify those cell cycles that start with one ori and at some point have 2 or more ori

[one_ori_cycle_ids,~]=ori_nos_cycle_ids(cycle_list,no_mother_cycle_ids,2);

%% Example cell cycles with ori and matp foci marked

figure
cycle = cycle_list(one_ori_cycle_ids(1856));
fluor_name = {'phase' 'fluor1_subtracted' 'fluor2_subtracted'} ;
for i=1:numel(fluor_name)
    figure
    display_cycle_with_range(cycle,channel_list,0,fluor_name{i},1,1:1:cycle.duration);
    
end

%% Ori and MatP foci kymographs

ori_foci_kymograph2(cycle_list,one_ori_cycle_ids,100,24,1); %MatP foci kymograph

ori_foci_kymograph2(cycle_list,one_ori_cycle_ids,100,24,2); %Ori foci kymograph

%% Synched kymograph



