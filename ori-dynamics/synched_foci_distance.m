function distance=synched_foci_distance(varargin)
cycle_list=varargin{1};
cycle_ids=varargin{2};
fc=['foci',num2str(varargin{3})];
synch_time_fn=varargin{4};
args_for_synch_func=varargin(5:nargin);



k=1;
for i=cycle_ids'
    if isfield(cycle_list(i).(['foci',num2str(varargin{3})]),'position_relative') &&...
            ~isempty(cycle_list(i).(['foci',num2str(varargin{3})]).position_relative)
        for j=1:size(cycle_list(i).(fc).position_relative(1,:,1),2)
            k=k+1;
        end
    end
end
distance=nan(k,121);
k=1;
I=synch_time_fn(cycle_list,cycle_ids,args_for_synch_func{:});
for i=1:numel(cycle_ids)
    if isfield(cycle_list(cycle_ids(i)).(fc),'position_relative') &&...
            ~isempty(cycle_list(cycle_ids(i)).(fc).position_relative)
        if ~isnan(I(i)) && ~isempty(I(i)) && I(i)>0 && size(cycle_list(cycle_ids(i)).(fc).position_relative(:,:,1),2)==2
            growth=cycle_list(cycle_ids(i)).lengths(2:end)./cycle_list(cycle_ids(i)).lengths(1:end-1);
            pos_diff=abs(diff(cycle_list(cycle_ids(i)).(fc).position_relative(:,:,1)./cycle_list(cycle_ids(i)).lengths,1,2));
            %pos_diff(2:end)=pos_diff(2:end)./growth;
            distance(k,60-I(i)+1:59)=(pos_diff(1:I(i)-1))';
            distance(k,60:60+(length(pos_diff)-I(i)))=(pos_diff(I(i):end))';
            k=k+1;
            
        end
    end
end
distance=distance./5;
figure
errorbar((-59:1:61)*5,nanmean(distance),nanstd(distance)./sqrt(sum(~isnan(distance))))
xlim([-100 150])
ylabel("Distance")
xlabel("Time (min)")
end