%function velocity_rel_ori_matp(cycle_list,cycle_ids,fluor_channel_matP,fluor_channel_ori,fr)
fluor_channel_matp=1;
fluor_channel=2;


fc1=strcat("foci",string(fluor_channel_matp)); %MatP foci
fc2=strcat("foci",string(fluor_channel_ori)); %Ori foci

edges=linspace(-40,40,80); % bin edges to sort ori velocity based on ori position relative to MatP position
middles=(edges(1:end-1)+edges(2:end))/2;

ori_rel_matp=NaN(30000,1);
v_rel=NaN(30000,1); % Vector that would contain ori velocities w.r.t ori position relative to MatP
cnt=0;
for i=cycle_ids'
    if isfield(cycle_list(i).(fc1),'position_relative') && isfield(cycle_list(i).(fc2),'position_relative') &&...
            ~isempty(cycle_list(i).(fc1).position_relative) && ~isempty(cycle_list(i).(fc2).position_relative)
        matp_foci_counts=sum(~isnan(cycle_list(i).(fc1).position_relative(:,:,1)),2); % counts number of MatP foci in each frame
        I=find(matp_foci_counts>1,1); % Find the first time in the cell cycle when more than one MatP foci was found
        
        if ~isempty(I)
            I=I-1; % Index of frame until two MatP foci appear in a cell cycle
        else
            I=cycle_list(i).duration; % when there was only one MatP foci throughout the cell cycle
        end
        
        temp1=cycle_list(i).(fc1).position_relative(1:I,:,1);
        temp1(isnan(temp1))=0;
        temp1=sum(temp1,2);
        temp1(temp1==0)=nan;
        matp_pos=temp1; % vector with MatP focus positions that will be used to calculate relative distance of Ori later
        if I>3
            v=diff(cycle_list(i).(fc2).position_relative(1:fr:I,:,1))*cycle_list(i).pole; % velocity of Ori
            x=cycle_list(i).(fc2).position_relative(1:fr:I-fr,:,1)*cycle_list(i).pole-matp_pos(1:fr:I-fr)*cycle_list(i).pole; % ori positions relative to MatP, % matrix-column, row-wise subtraction
            v=v(:);
            x=x(:);
            I=~isnan(x) & ~isnan(v);
            x=x(I);
            v=v(I);
            
            v_ori(cnt+1:cnt+numel(v))=v;
            ori_rel_matp(cnt+1:cnt+numel(v))=x;
            cnt=cnt+numel(v);
        end
    end
end

[~,~,bins]=histcounts(ori_rel_matp,edges); % Binning ori positions relative to matP

for k=1:length(edges)-1
    v_mean(k)=nanmean(v_ori(bins==k));
    v_std(k)=nanstd(v_ori(bins==k));
    v_sem(k)=v_std(k)./sqrt(numel(v_ori(bins==k)));
end

figure
shadederror_sem(middles*0.067,v_mean*0.067./fr,(v_mean*0.067./fr)+(v_std*0.067./fr),(v_mean*0.067./fr)-(v_std*0.067./fr),v_sem*0.067/fr,"Foci distances from frame of splitting");
%plot(middles*0.067,v_mean*0.067./fr)
hold on
xline(0)
yline(0)
hold off
xlim([-1 1])
ylim([-0.4 0.3])
xlabel("Ori position relative to MatP (um)")
ylabel("Ori velocity (um/frame)")
%end

