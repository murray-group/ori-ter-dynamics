%% Function to find distance between two foci

function foci_distances=foci_distance_absolute(cycle_list,complete_cycle_ids,fluor_channel,num_foci)

foci_distances=[];
fc=strcat("foci",string(fluor_channel));

% if ~isfield(cycle_list(complete_cycle_ids(1)).(fc),'foci_position_major')
%     for i=1:numel(cycle_list)
%         [cycle_list(i).(fc).('foci_position_major')]=cycle_list(i).(fc).position_relative(:,:,1)./cycle_list(i).major;
%     end
% end
foci_distances=nan(38,23047);
for i=1:numel(complete_cycle_ids)
   cycle=cycle_list(complete_cycle_ids(i));
   I=get_time_first_1_to_2_split(cycle,fluor_channel);
   ori_focis_nos=sum(~isnan(cycle.(fc).position(I:end,:,1)),2);
   for j=1:numel(ori_focis_nos)
       if ori_focis_nos(j)==num_foci
           temp1=abs(diff((cycle.(fc).position(I+j-1,~isnan(cycle.(fc).position(I+j-1,:,1)),1))));
           %temp1(1,2)=j./cycle.duration;
           
           foci_distances(j,i)=temp1;
           elseif ori_focis_nos(j)==1
           foci_distances(j,i)=0;
       end
   end
end
% figure
% histogram(foci_distances(:,1))
% xlabel("Absolute distance between ori foci pairs (3 foci only, pixels)")
end
