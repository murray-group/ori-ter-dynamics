%% Generating ori foci distance vs time heatmap

% Relative

foci_distances=foci_distance_relative(cycle_list,complete_cycle_ids(one_to_two),2,2);
[N,~,~,binx,biny]=histcounts2(foci_distances(:,2),foci_distances(:,1));
N=N';
figure
imagesc(linspace(0,1,19),linspace(min(foci_distances(:,1)),max(foci_distances(:,1)),44),N)
ax.YDir='normal'
xlabel("Relative cell age from birth")
ylabel("Relative distance between foci")
title("Heatmap of foci distance(relative) over time")

% Absolute
foci_distances=foci_distance_absolute(cycle_list,complete_cycle_ids(one_to_two),2,2);
[N,~,~,binx,biny]=histcounts2(foci_distances(:,2),foci_distances(:,1));
N=N';
figure
imagesc(linspace(0,1,19),linspace(min(foci_distances(:,1)),max(foci_distances(:,1)),44),N)
ax.YDir='normal'
xlabel("Relative cell age from birth")
ylabel("Distance between foci (absolute)")
title("Heatmap of foci distance(pixels) over time")
