function focivelocity=foci_velocity(varargin) % args are cycle_list,cycle_ids,foci whose velocity needs to be calculated,sync function, sync fn arguments
cycle_list=varargin{1};
cycle_ids=varargin{2};
fc=['foci',num2str(varargin{3})];
synch_time_fn=varargin{4};
args_for_synch_func=varargin(5:nargin);



k=1;
for i=cycle_ids'
    
    for j=1:size(cycle_list(i).(fc).position_relative(1,:,1),2)
            k=k+1;
    end
end
focivelocity=nan(k,121);
k=1;
for i=cycle_ids'
    
    I=synch_time_fn(cycle_list(i),args_for_synch_func{:});
    if ~isnan(I) && ~isempty(I)
        for j=1:size(cycle_list(i).(fc).position_relative(1,:,1),2)
            v=gradient(cycle_list(i).(fc).position_relative(:,j,1));
            focivelocity(k,60-I+1:59)=abs(v(1:I-1))';
            focivelocity(k,60:60+(length(v)-I))=abs(v(I:end))';
            k=k+1;
        end
    end
end
figure
errorbar((-59:1:61)*5,nanmean(focivelocity)*0.067,nanstd(focivelocity)*0.067./sqrt(sum(~isnan(focivelocity))))
xlim([-100 100])
ylabel("Micrometers per frame (5 min)")
xlabel("Time (min)")
end
