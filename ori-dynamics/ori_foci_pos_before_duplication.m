fc='foci3';
ori_dup_frames=a;
ori_pos_at_dup=nan(numel(complete_cycle_ids),1);
for i=1:length(complete_cycle_ids)
    cycle=cycle_list(complete_cycle_ids(i));
    if ~isnan(ori_dup_frames(i)) && ori_dup_frames(i)>0
        ori_pos_at_dup(i,1)=cycle.(fc).position_relative(ori_dup_frames(i),1,1)*-cycle.pole;
    end
end
        