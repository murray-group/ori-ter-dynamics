%% Script to find the number of ori foci at birth and division

function [ori_foci_at_birth,ori_foci_at_division]=ori_foci_nos(cycle_list,complete_cycle_ids,fluor_channel)

fc=strcat("foci",string(fluor_channel));

for i=1:numel(complete_cycle_ids)
    cycle=cycle_list(complete_cycle_ids(i));
    if ~isempty(cycle.(fc).position)
    ori_foci_at_birth(i)=sum(~isnan(cycle.(fc).position(1,:,1)),2);
    ori_foci_at_division(i)=sum(~isnan(cycle.(fc).position(end,:,1)),2);
    else
    ori_foci_at_birth(i)=nan;
    ori_foci_at_division(i)=nan;
end
end
