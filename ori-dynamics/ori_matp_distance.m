function [ori_matp_dist,ori_matp_coloc,cyc_duration]=ori_matp_distance(cycle_list,complete_cycle_ids)

fluor_channel1=1;
fluor_channel2=2;

rel=0;
fc1=strcat('foci',num2str(fluor_channel1));
fc2=strcat('foci',num2str(fluor_channel2));
k=1;
for i=complete_cycle_ids
    %i
    cycle=cycle_list(i);
    if isfield(cycle.(fc1),'position_relative') && ~isempty(cycle.(fc1).position_relative)...
            && isfield(cycle.(fc2),'position_relative') && ~isempty(cycle.(fc2).position_relative)
        
        temp1=[];
        temp2=[];
        
        
        for j=1:size(cycle.(fc1).position_relative,2)
            if rel==1
                temp1=((cycle.(fc2).position_relative-cycle.(fc1).position_relative(:,j,:))./cycle.lengths).^2;
                temp1=min(sqrt(temp1(:,:,1)+temp1(:,:,2)),[],2);
                temp2=[temp2 temp1];
            else
                temp1=((cycle.(fc2).position_relative-cycle.(fc1).position_relative(:,j,:))).^2;
                temp1=min(sqrt(temp1(:,:,1)+temp1(:,:,2)),[],2);
                temp2=[temp2 temp1];
            end
        end
        ori_matp_dist{i}=temp2;
        ori_matp_coloc{k}=any(temp2<=3.9,2); %% translates to approx. 260 nm
        cyc_duration(k)=cycle.duration;
        k=k+1;
    end
end
end