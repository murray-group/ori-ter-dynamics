complete_cycle_ids = get_complete_cycle_ids(cycle_list);
idx_complete=ismember(1:numel(cycle_list),complete_cycle_ids); % idx is logical indices

mothers=get_mother_cycle_ids(channel_list,cycle_list);
idx_mothers=ismember(1:numel(cycle_list),mothers); % idx is logical indices
idx=idx_complete & ~idx_mothers; %complete but not mothers
cycle_ids=1:numel(cycle_list);
cycle_ids=cycle_ids(idx);

%%
synched_kymograph(cycle_list,complete_cycle_ids,2,@get_time_first_1_to_2_split,2)

%%
b=arrayfun( @(S) get_time_first_1_to_2_split(S,2), cycle_list(complete_cycle_ids) );
cut_off=4;
nframes=9;
b2=get_ori_split(cycle_list,complete_cycle_ids,2,cut_off,nframes);
b3=arrayfun( @(S) get_stable_time_at_middle(S,1,3.6), cycle_list(complete_cycle_ids) );
%%
w=3.6;
nframes=5;
b4=get_MatP_stable_at_middle(cycle_list,cycle_ids,1,w,nframes);
%%
figure(5)
edges=-20.5:1:50.5;
histogram(b,edges)
hold on;
histogram(b2,edges);
hold off;

%%
paired_difference(b2,b4,[-0.6,1],[0,1],{'ori','MatP','diff'},'time')