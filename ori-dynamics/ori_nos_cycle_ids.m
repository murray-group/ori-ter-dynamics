%% Script to identify cycles that start with only one or two ori focus

function [one_ori_cycle_ids,two_ori_cycle_ids]=ori_nos_cycle_ids(cycle_list,complete_cycle_ids,fluor_channel)

one_ori_cycle_ids=[];
two_ori_cycle_ids=[];
fc=strcat("foci",string(fluor_channel));

for i=1:numel(complete_cycle_ids)
    
    cycle=cycle_list(complete_cycle_ids(i));
    if isfield(cycle.(fc),'position_relative') && isfield(cycle.(fc),'position_relative') &&...
            ~isempty(cycle.(fc).position_relative) && ~isempty(cycle.(fc).position_relative)
    temp1=sum(~isnan(cycle.(fc).position(1,:,1)),2);
    if temp1==1
        one_ori_cycle_ids=[one_ori_cycle_ids;cycle.cycle_id];
    elseif temp1==2
        two_ori_cycle_ids=[two_ori_cycle_ids;cycle.cycle_id];
    end
    end
end
end
