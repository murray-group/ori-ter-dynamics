%% parameters
D = 2e-4/0.0668^2*60*5;%diffusion in pixel^2 per minute
false_negative = 0.10;
false_positive = 0.005;
frame_skips = 3;
thresh_possible_connections = false_negative^(frame_skips+1);
thresh_possible_connections = max(0.0001,thresh_possible_connections);
foci_field = 'foci2';


%% create a filter of all cycles which have proper relatives to extend the data before and after the cycle
filter_data_with_foci = false(1,numel(cycle_list));%select cycles with foci
for cycle_it = 1:numel(cycle_list)
    cycle = cycle_list(cycle_it);
    filter_data_with_foci(cycle_it) = sum(~isnan(cycle.(foci_field).intensity),'all')>0;
end
cycle_list = filter_cycle_list(cycle_list,filter_data_with_foci);
filter_has_relatives = false(1,numel(cycle_list));
for i = 1:numel(cycle_list) % create a filter list with all cycles with the appropraite relation graph
    x = cycle_list(i);
    if sum(~isnan(x.relation_graph.parent)) == 1 
        p = cycle_list(x.relation_graph.parent);
        if sum(~isnan(x.relation_graph.children)) == 2 && sum(~isnan(p.relation_graph.children)) == 2
            filter_has_relatives(i) = true;
        end
    end
end
sum(filter_has_relatives)

%% run Astar
cycle_list_inferred = cycle_list(filter_has_relatives); %at the end this list will contain the inferred cycles
reduce = 1;%reduce the number of cycles to analyze to shorten runtime (2 means every second cycle is used)
cycle_list_inferred = cycle_list_inferred(1:reduce:end);
padding = 5;%get this many frames from the cycles before
parfor cycle_it = 1:numel(cycle_list_inferred) % constrained A*
    cycle = cycle_list_inferred(cycle_it);
    disp(strcat("(a) cycle: ",string(cycle_it)))
    
    tic;
    n_foci = sum(~isnan(cycle.(foci_field).position(:,:,1)),2);
    [pos,pos_original,~] = pad_position(cycle,cycle_list,padding,foci_field);
    if all(isnan(pos));pos(end,end,:)=42;end%catching an error where only nan would make matlab crash

    [cmf,cm,fd] = create_cost_matrix(pos_original,D,thresh_possible_connections ...
        ,false_negative,cycle.duration+2*padding...
        ,pos_original);
    edges = trackingAstar(cmf,100000,0.1);
    
    % add addition fields to each cycle like how much the position matrix extends into parent and childern
    t = toc;
    cycle.(foci_field).edges = edges;
    cycle.(foci_field).n_foci = n_foci;
    cycle.(foci_field).cm = cm;
    cycle.(foci_field).fd = fd;
    cycle.(foci_field).padding = padding;
    cycle.(foci_field).t1 = t;
    cycle_list_inferred(cycle_it) = cycle;
end


%% Stitching
parfor cycle_it = 1:numel(cycle_list_inferred) %stitching
    disp(strcat("(s) cycle: ",string(cycle_it)))
    cycle = cycle_list_inferred(cycle_it);
    padding = cycle.(foci_field).padding;
    tic;
    [trajectories_matrix,start_and_stop] = stitching(cycle.(foci_field).edges,cycle.(foci_field).cm,cycle.(foci_field).fd,false_positive,cycle.duration+2*padding);
    
    t = toc;
    n_inferred_foci = zeros(numel(cycle.(foci_field).n_foci)+2*padding,1);
    for i = 1:size(start_and_stop,1)
        n_inferred_foci(start_and_stop(i,1):start_and_stop(i,2)) = n_inferred_foci(start_and_stop(i,1):start_and_stop(i,2))+1;
    end
    
    %remove padding from result
    cycle.(foci_field).position = trajectories_matrix(padding+1:end-padding,:,:);
    cycle.(foci_field).n_inferred_foci = n_inferred_foci(padding+1:end-padding);
    cycle.(foci_field).t2 = t;
    cycle_list_inferred(cycle_it) = cycle;
end



%% function pad position
function [pos_all,pos_original,l_all] = pad_position(cycle,cycle_list,t,foci_field)

    relation_graph = cycle.relation_graph;
    parent = cycle_list(relation_graph.parent);
    siblings = parent.relation_graph.children;
    sibling = cycle_list(siblings(siblings~=cycle.cycle_id));
    is_on_top = on_top(cycle,sibling);
    child1 = cycle_list(relation_graph.children(1));
    child2 = cycle_list(relation_graph.children(2));
    [~,child1,child2] = on_top(child1,child2);
    position_children = cat(2,child1.(foci_field).position(1:t,:,:),child2.(foci_field).position(1:t,:,:));
    position_children(:,1:size(child1.(foci_field).position,2),1) = position_children(:,1:size(child1.(foci_field).position,2),1) + child2.lengths(1:t);
    position_current = cycle.(foci_field).position(:,:,:);
    position_parent = parent.(foci_field).position(end-t+1:end,:,:);
    pl = parent.lengths(end-t+1:end)/2;
    ppy = position_parent(:,:,1);
    if ~is_on_top
        f = ppy<pl;
        ppy(f) = nan;
        ppy = ppy-pl;
    else
        f = ppy>=pl;
        ppy(f) = nan;
    end
    position_parent(:,:,1) = ppy;
    l_all = cat(1,pl,cycle.lengths,child2.lengths(1:t)+child1.lengths(1:t));


    n_trajectories = max([size(position_parent,2),size(position_current,2),size(position_children,2)]);
    position_parent = add_nans(position_parent,n_trajectories);
    position_current = add_nans(position_current,n_trajectories);
    position_children = add_nans(position_children,n_trajectories);
    pos_all = cat(1,position_parent,position_current,position_children);
    pos_original = pos_all;
    pos_all(:,:,1) = pos_all(:,:,1)./l_all*min(l_all);%+(max(l_all)-l_all)/2;%
    %if on_top(cycle,sibling)
    %    l_all
    %    figure()
    %    plot(pos_all(:,:,1),'x')
    %end
end

function [on_top,sibling1,sibling2] = on_top(sibling1,sibling2)
    sibling1_center = mean(sibling1.bbs(1,:,1)); 
    sibling2_center = mean(sibling2.bbs(1,:,1)); 
    on_top = sibling1_center < sibling2_center;
    if on_top
        tmp = sibling1;
        sibling1 = sibling2;
        sibling2 = tmp;
    end  
end

function [position] = add_nans(position,n_trajectories)
    position = cat(2,position,nan(size(position,1),n_trajectories-size(position,2),2));
end

