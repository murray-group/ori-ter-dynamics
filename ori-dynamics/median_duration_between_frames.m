function median_durations=median_duration_between_frames(cycle_list,a,b)


cycle_ids=[];
median_durations=[];
for i=1:length(cycle_list)
    if (all(cycle_list(i).times>=a & cycle_list(i).times<=b))
        cycle_ids=[cycle_ids cycle_list(i).cycle_id];
    end
end
median_durations=median([cycle_list(cycle_ids).duration]);
end