%% Function to calculate foci distance over time for cycles that start with one foci and end with two foci

function dot=foci_dot(cycle_list,complete_cycle_ids,fluor_channel)

fc=strcat("foci",string(fluor_channel));

[ori_foci_at_birth,ori_foci_at_division]=ori_foci_nos(cycle_list,complete_cycle_ids,fluor_channel);
one_to_two_ids=find((ori_foci_at_birth==1) & (ori_foci_at_division==2));
orisplitframe=ori_split_frame(cycle_list,complete_cycle_ids,fluor_channel);
figure
for i=1:numel(one_to_two_ids)
    cycle=cycle_list(complete_cycle_ids(one_to_two_ids(i)));
    temp1=sum(~isnan(cycle.(fc).position_relative(:,:,1)),2);
    j=1;
    while temp1(j)<=1
        j=j+1;
    end
    orisplitframe=j;
    k=1;
    while j<=cycle.duration
        temp1=abs(diff((cycle.(fc).position(j,~isnan(cycle.(fc).position(j,:,1)),1))));
        temp1=max(temp1);
        if isempty(temp1)
            temp1=nan;
        end
        dot{i}(k,1)=temp1;
        dot{i}(k,2)=j-orisplitframe;
        dot{i}(k,3)=(j-orisplitframe)/(cycle.duration-orisplitframe);
        k=k+1;
        j=j+1;
    end
    plot(dot{i}(:,1))
    hold on
end
hold off
end

