%% Histogram of fluorescence signal intensities of cells

function [mean_intensities]=mean_intensities_of_cell_cycles(cycle_list,complete_cycle_ids,fluor_channel)

fc=strcat("fluor",string(fluor_channel),"_lp");
for i=1:numel(complete_cycle_ids)
    cycle=cycle_list(complete_cycle_ids(i));
    mean_cell_intensity=[];
    for j=1:cycle.duration
        mean_cell_intensity(j)=mean(cycle.(fc){j});
    end
    mean_intensities(i)=mean(mean_cell_intensity);
end
end
