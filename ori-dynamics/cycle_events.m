%% Script to generate a table/matrix of various events in a cell cycle
%{
1. cycle id
2. No. of ori foci at birth
3. No. of ori foci at division
4. Frame at which ori first split
5. Frame at which ori segregate
6. Cell length at which ori first split
7. Cell length at which ori first segregate
8. Age at which ori first split
9. Age at which ori first segregate
10. Frame at which MatP first leave the pole
11. Frame at which MatP starts to remain at mid-cell
12. Age at which MatP first leave the pole
13. Age at which MatP starts to remain at mid-cell
14. Cell length at which MatP leave the pole
15. Cell length at which MatP associate with mid-cell

%}

function cyclelistofevents=cycle_events(cycle_list,complete_cycle_ids,fluor_channel_ori,fluor_channel_matp)

% fc1=strcat("foci",string(fluor_channel_ori));
% fc2=strcat("foci",string(fluor_channel_matp));

[ori_foci_at_birth,ori_foci_at_division]=ori_foci_nos(cycle_list,complete_cycle_ids,fluor_channel_ori);
orisplitframe=ori_split_frame(cycle_list,complete_cycle_ids,fluor_channel_ori);
orisegregate=ori_at_quarter(cycle_list,complete_cycle_ids,fluor_channel_ori);

cyclelistofevents=[];
for i=1:numel(complete_cycle_ids)
    cycle=cycle_list(complete_cycle_ids(i));
    cyclelistofevents(i).cycle_id=cycle.cycle_id;
    cyclelistofevents(i).ori_num_birth=ori_foci_at_birth(i);
    cyclelistofevents(i).ori_num_div=ori_foci_at_division(i);
    cyclelistofevents(i).ori_split_frame=orisplitframe(i);
    cyclelistofevents(i).ori_segregate_frame=orisegregate(i,1);

    if isnan(orisplitframe(i))
        cyclelistofevents(i).ori_split_cell_length=nan;
    else
    cyclelistofevents(i).ori_split_cell_length=cycle.lengths(orisplitframe(i));
    end
    
    if isnan(orisegregate(i))
        cyclelistofevents(i).ori_segregate_cell_length=nan;
    else        
    cyclelistofevents(i).ori_segregate_cell_length=cycle.lengths(orisegregate(i));
    end
    cyclelistofevents(i).ori_split_age=orisplitframe(i)./cycle.duration;
    cyclelistofevents(i).ori_segregate_age=orisegregate(i,3);
    cyclelistofevents(i).matp_leave_pole_frame=get_first_time_to_middle(cycle,fluor_channel_matp,1/3);
    cyclelistofevents(i).matp_to_middle_frame=get_last_time_to_middle(cycle,fluor_channel_matp,1/3);
    cyclelistofevents(i).matp_leave_pole_age=get_first_time_to_middle(cycle,fluor_channel_matp,1/3)./cycle.duration;
    cyclelistofevents(i).matp_to_middle_age=get_last_time_to_middle(cycle,fluor_channel_matp,1/3)./cycle.duration;
    if isempty(cyclelistofevents(i).matp_to_middle_age)
        cyclelistofevents(i).matp_to_middle_age=nan;
    end
    if isempty(cyclelistofevents(i).matp_leave_pole_frame)||isnan(cyclelistofevents(i).matp_leave_pole_frame)
        cyclelistofevents(i).matp_leave_pole_length=nan;
    else
    cyclelistofevents(i).matp_leave_pole_length=cycle.lengths(cyclelistofevents(i).matp_leave_pole_frame);
    end
    if isempty(cyclelistofevents(i).matp_to_middle_frame) || isnan(cyclelistofevents(i).matp_to_middle_frame)
        cyclelistofevents(i).matp_to_middle_length=nan;
    else
    cyclelistofevents(i).matp_to_middle_length=cycle.lengths(cyclelistofevents(i).matp_to_middle_frame);
    end

end
end
