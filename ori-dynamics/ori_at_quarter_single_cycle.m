%% Cell age at which ori foci move to quarter positions
function I=ori_at_quarter(cycle,fluor_channel,w)

I=[];
fc=strcat("foci",string(fluor_channel));

if ~isfield(cycle.(fc),'foci_position_major')
    
        [cycle.(fc).('foci_position_major')]=cycle.(fc).position_relative(:,:,1)./cycle.major;
   
end

    ori_focis_nos=sum(~isnan(cycle.(fc).position_relative(:,:,1)),2);
    I=nan;
    
    for j=1:numel(ori_focis_nos)
        
        if ori_focis_nos(j)==2
            temp1=abs(diff((cycle.(fc).foci_position_major(j,~isnan(cycle.(fc).foci_position_major(j,:))))));
            if temp1>=0.3514 %&& temp1<=0.5305
                I=j;
                break;
            end
        end
        
    end



% figure
% histogram(ori_at_quarter_age(:,3),23)
% xlabel("Cell age at which ori moves to quarter positions (one foci birth)")
% 
% figure
% histogram(ori_at_quarter_age(:,1),23)
% xlabel("Frame at which ori moves to quarter positions (one foci birth)")


end

