function focivelocity=foci_velocity2(varargin)
cycle_list=varargin{1};
cycle_ids=varargin{2};
fc=['foci',num2str(varargin{3})];
synch_time_fn=varargin{4};
args_for_synch_func=varargin(5:nargin);



k=1;
for i=cycle_ids'
    
    for j=1:size(cycle_list(i).(fc).position_relative(1,:,1),2)
            k=k+1;
    end
end
focivelocity=nan(k,121);
k=1;
for i=cycle_ids'
    
    I=synch_time_fn(cycle_list(i),args_for_synch_func{:});
    if ~isnan(I) && ~isempty(I)
        for j=1:size(cycle_list(i).(fc).position_relative(1,:,1),2)
            v=gradient(cycle_list(i).(fc).position_relative(:,j,1));
            v=-v.*sign(cycle_list(i).(fc).position_relative(:,j,1));
            focivelocity(k,60-I+1:59)=(v(1:I-1))';
            focivelocity(k,60:60+(length(v)-I))=(v(I:end))';
            k=k+1;
        end
    end
end
figure
errorbar((-59:1:61)*5,nanmean(focivelocity)*.067,nanstd(focivelocity)*.067./sqrt(sum(~isnan(focivelocity))))
xlim([-100 100])
ylabel("Micrometers per frame (5 min)")
xlabel("Time (min)")
end
