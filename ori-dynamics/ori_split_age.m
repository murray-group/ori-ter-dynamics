%% Function to identify the cell age at which ori first splits into two foci

function ori_split_age=ori_split_age(cycle_list,complete_cycle_ids,fluor_channel)

fc=strcat("foci",string(fluor_channel));
if ~isfield(cycle_list(complete_cycle_ids(1)).(fc),'foci_position_major')
    for i=1:numel(cycle_list)
        [cycle_list(i).(fc).('foci_position_major')]=cycle_list(i).(fc).position_relative(:,:,1)./cycle_list(i).major;
    end
end

for i=1:numel(complete_cycle_ids)
    cycle=cycle_list(complete_cycle_ids(i));
    temp1=sum(~isnan(cycle.(fc).foci_position_major),2);
    j=1;
    while temp1(j)<=1
        j=j+1;
        if j>cycle.duration
            j=nan;
            break;
        end
    end
    ori_split_age(i)=j./cycle.duration;
end

histogram(ori_split_age,23)

