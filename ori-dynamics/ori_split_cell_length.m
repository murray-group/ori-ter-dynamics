%% Function to find the cell length at which ori splits

function ori_split_length=ori_split_cell_length(cycle_list, complete_cycle_ids, fluor_channel)

ori_frame_split=ori_split_frame(cycle_list,complete_cycle_ids,fluor_channel);

for i=1:numel(ori_frame_split)
    cycle=cycle_list(complete_cycle_ids(i));
    if ~isnan(ori_frame_split(i))
        ori_split_length(i)=cycle.lengths(ori_frame_split(i));
    else
        ori_split_length(i)=nan;
    end
end
end

