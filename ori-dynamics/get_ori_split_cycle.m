function b=get_ori_split_cycle(cycle,fluor_channel)
    
%%%Finds the frame at which 2 ori are first observed. First uses
%%%get_time_first_1_to_2_split. Then for cells in which that function
%%%failed and which have 2 ori at birth, goes to the parent cell and looks
%%%in the appropriate cell half of that cell. The parent cell need not be
%%%in cycle_ids


%fluor_channel=2;
    c=0;
    c2=0;
    b=NaN(size(cycle_ids));
    
    
    for i=1:length(cycle_ids)
        b=get_time_first_1_to_2_split(cycle_list((i)),2);
        
        
        if isnan(b) && sum(~isnan(cycle.(['foci',num2str(fluor_channel)]).position(1,:,1)),2)==2
            parent=cycle.relation_graph.parent;
            if isnan(parent)
             
                continue;
            end
            I=find(cycle_list(parent).relation_graph.children==cycle_ids(i));% I=1, the child is the lower cell (large position values), for I=2 the child is the upper cell (lower position values)
            
            positions=cycle_list(parent).(['foci',num2str(fluor_channel)]).position(:,:,1);
            if I==1
                positions(positions<cycle_list(parent).lengths/2)=nan;
            else
                positions(positions>cycle_list(parent).lengths/2)=nan;
            end
            
            n_spots=sum(~isnan(positions),2);
            I=find(n_spots==1,1,'last')-length(n_spots);% 0 if the last frame, -1 of the second last
            if ~isempty(I)
                b(i)=I+1;
            end
        end    
    end
    disp([num2str(c), ' cycles with 2 ori at birth']);
    disp([num2str(c2), ' of those do not have a parent']);
end