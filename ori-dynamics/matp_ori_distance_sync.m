function matp_ori_distance=matp_ori_distance_sync(varargin) % args are cycle_list,cycle_ids,foci whose velocity needs to be calculated,sync function, sync fn arguments
cycle_list=varargin{1};
cycle_ids=varargin{2};
fc=['foci',num2str(varargin{3})];
synch_time_fn=varargin{4};
args_for_synch_func=varargin(5:nargin);


[ori_matp_dist,~,~]=ori_matp_distance(cycle_list,cycle_ids');

k=1;
for i=cycle_ids'
   
    k=k+size(ori_matp_dist{i},2);
   

end
matp_ori_distance=nan(k,121);
k=1;
for i=cycle_ids'
    
    I=synch_time_fn(cycle_list(i),args_for_synch_func{:});
    if ~isnan(I) && ~isempty(I)
        for j=1:size(ori_matp_dist{i},2)
            %i
            v=ori_matp_dist{i}(:,j);
            matp_ori_distance(k,60-I+1:59)=v(1:I-1)';
            matp_ori_distance(k,60:60+(length(v)-I))=v(I:end)';
            k=k+1;
        end
    end
end
%figure
errorbar((-59:1:61)*5,nanmean(matp_ori_distance)*0.067,nanstd(matp_ori_distance)*0.067./sqrt(sum(~isnan(matp_ori_distance))))
xlim([-100 100])
ylabel("Ori-MatP dist/length")
xlabel("Time(min)")
end
