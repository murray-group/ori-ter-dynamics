function ori_foci_kymograph(cycle_list,complete_cycle_ids,nbins_y,nbins_x,fluor_channel)

fc=strcat("foci",string(fluor_channel));

complete_cycles=cycle_list(complete_cycle_ids);
bin_edges_x=linspace(0,1,nbins_x+1);
bin_edges_y=linspace(-0.51,0.51,nbins_y+1);
data=zeros(nbins_x,nbins_y);
%ndata=ones(nbins_x,nbins_y);

for i=1:numel(complete_cycles)
    if isfield(complete_cycles(i).(['foci',num2str(fluor_channel)]),'position_relative') && isfield(complete_cycles(i).(['foci',num2str(fluor_channel)]),'position_relative') &&...
            ~isempty(complete_cycles(i).(['foci',num2str(fluor_channel)]).position_relative) && ~isempty(complete_cycles(i).(['foci',num2str(fluor_channel)]).position_relative)
        [complete_cycles(i).(fc).('foci_position_major')]=complete_cycles(i).(fc).position_relative(:,:,1)./complete_cycles(i).lengths;
        norm_length=linspace(0,1,complete_cycles(i).duration);
        [~,~,bins_x]=histcounts(norm_length,bin_edges_x);
        if (complete_cycles(i).pole==1)
            [~,~,bins_y]=histcounts(complete_cycles(i).(fc).foci_position_major,bin_edges_y);
        elseif(complete_cycles(i).pole==-1)
            [~,~,bins_y]=histcounts(-complete_cycles(i).(fc).foci_position_major,bin_edges_y);
        end
        
        
        for k=1:size(bins_y,2)
            for j=1:complete_cycles(i).duration
                if ~isnan(complete_cycles(i).(fc).foci_position_major(j,k)) &&...
                        abs(complete_cycles(i).(fc).foci_position_major(j,k))<0.51
                    data(bins_x(j),bins_y(j,k))=data(bins_x(j),bins_y(j,k))+1;
                end
            end
        end
    end
end
ndata=sum(data,2);
data=data./ndata;
sum(data,2)
data=data';
figure
imagesc(data)
newdata=nan(nbins_y,nbins_x);
growth_rate = 1.01;
newdatabins_y = round(power(growth_rate,linspace(0,log(2)/log(growth_rate),nbins_x))*nbins_y/2); %exponential

for i=1:size(data,2)
    lb=round((nbins_y-newdatabins_y(i))/2)+1;
    temp1=resample(data(:,i),newdatabins_y(i),nbins_y);
    newdata(lb:lb+newdatabins_y(i)-1,i)=temp1;
end

figure
ndata=sum(newdata,1,'omitnan');%created  by robin
newdata=newdata./ndata;%created  by robin
imagesc(newdata)
imAlpha=ones(size(newdata));
    imAlpha(isnan(newdata))=0;
    imagesc(newdata,'AlphaData',imAlpha);
    set(gca,'color',[1 1 1]);
    
    x = linspace(0,1,5);
    xticks = x*(nbins_x-1)+1;
    xlabel("Relative cell age")
    set(gca,'XTick',xticks, 'XTickLabel', x);
    
    y = linspace(0,2,5);
    yticks = x*(nbins_y-1)+1;
    set(gca,'YTick',yticks, 'YTickLabel', -1*y+1);
    ylabel("Relative cell length")
    %imagesc(canvas)
    colorbar

% figure
% imagesc(linspace(0,1,nbins_x),linspace(0.51,-0.51,nbins_y),data)
% ax=gca;
% ax.YDir='normal';
% 
% xlabel("Relative cell age")
% ylabel("Relative position inside cell")

end