%% Script to create a table of foci distances and corresponding MatP position inside the cell

function focidistandmatppos=foci_dist_and_matp_pos(cycle_list,complete_cycle_ids,fluor_channel_ori,fluor_channel_matp)

fc1=strcat("foci",string(fluor_channel_ori));
fc2=strcat("foci",string(fluor_channel_matp));

if ~isfield(cycle_list(complete_cycle_ids(1)).(fc1),'foci_position_major')
    for i=1:numel(cycle_list)
        [cycle_list(i).(fc1).('foci_position_major')]=cycle_list(i).(fc1).position_relative(:,:,1)./cycle_list(i).major;
    end
end

if ~isfield(cycle_list(complete_cycle_ids(1)).(fc2),'foci_position_major')
    for i=1:numel(cycle_list)
        [cycle_list(i).(fc2).('foci_position_major')]=cycle_list(i).(fc2).position_relative(:,:,1)./cycle_list(i).major;
    end
end
focidistandmatppos=[];
parfor i=1:numel(complete_cycle_ids)
    cycle=cycle_list(complete_cycle_ids(i));
    I=get_time_first_1_to_2_split(cycle,fluor_channel_ori);
    ori_focis_no=sum(~isnan(cycle.(fc1).position_relative(I:end,:,1)),2);
   for j=1:numel(ori_focis_no)
       if ori_focis_no(j)==2
           temp1=abs(diff((cycle.(fc1).foci_position_major(I+j-1,~isnan(cycle.(fc1).foci_position_major(I+j-1,:))))));
           temp2=abs(cycle.(fc2).foci_position_major(I+j-1,:));
           temp2=temp2(~isnan(temp2));
           if numel(temp2)==2
               temp1=repmat(temp1,2,1);
           elseif isempty(temp2)
               temp2=nan;
           end
           
           focidistandmatppos=[focidistandmatppos;[temp1 temp2']];
       end
   end
end
end