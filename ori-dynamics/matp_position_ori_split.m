%% Function to find the relative position of MatP along long axis when ori first splits

function matp_location=matp_position_ori_split(cycle_list,complete_cycle_ids,ori_frame_split, fluor_channel)

matp_location=[];
fc=strcat("foci",string(fluor_channel));

if ~isfield(cycle_list(complete_cycle_ids(1)).(fc),'foci_position_major')
    for i=1:numel(cycle_list)
        [cycle_list(i).(fc).('foci_position_major')]=cycle_list(i).(fc).position_relative(:,:,1)./cycle_list(i).major;
    end
end

for i=1:numel(complete_cycle_ids)
    cycle=cycle_list(complete_cycle_ids(i));
    if ~isnan(ori_frame_split(i))
        matp_location(i)=abs(cycle.(fc).foci_position_major(ori_frame_split(i)));
    else
        matp_location(i)=nan;
    end
end
end

