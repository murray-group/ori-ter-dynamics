function I=get_time_last_1_to_2_split(cycle,fluor_channel)
        %cycle.cycle_id
        n_spots=sum(~isnan(cycle.(['foci',num2str(fluor_channel)]).position(:,:,1)),2);
        I=find(any(n_spots<2,2),1,'last')+1;%%the first frame at which there are 2 foci
        if ~isempty(I)
        if isempty(find(n_spots==1,1)) || find(n_spots==1,1)>I || any(n_spots(1:I-1)>2)
            I=nan;
        end
        end
        if isempty(I)
            I=nan;
        end
        
end