I=[];
I_age=[];
for i=1:numel(one)
    cycle=cycle_list(complete_cycle_ids(i));
    I(i)=get_last_time_to_middle(cycle,1,1/3);
    I_age(i)=I(i)./cycle.duration;
end

figure
histogram(I)
figure
histogram(I_age)