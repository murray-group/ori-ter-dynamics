%% Script to find Matp pos minus ori pos

function matpcrossori=matp_cross_ori(cycle,fluor_channel_ori,fluor_channel_matp)

fc1=strcat("foci",string(fluor_channel_ori));
fc2=strcat("foci",string(fluor_channel_matp));

if ~isfield(cycle.(fc1),'position_relative') || ~isfield(cycle.(fc2),'position_relative') || isempty(cycle.(fc1).position_relative) || isempty(cycle.(fc2).position_relative) 
    matpcrossori=NaN;
    return;
end
        out=NaN(cycle.duration,1);
        for i=1:cycle.duration
            
            z=(squeeze(cycle.(fc2).position_relative(i,:,1))-squeeze...
                (cycle.(fc1).position_relative(i,:,1))')*cycle.pole; %The function squeeeze might not be needed here for this to work
            %z=((cycle.(fc2).position_relative(i,:,1))-(cycle.(fc1).position_relative(i,:,1))')*cycle.pole;
            out(i)=any(z(:)>0) && any(z(:)<0); % To consider only the cycles that has two foci and matp is in the middle
            %out(i)=any(z(:)>0); % Find the first time when MatP and Ori
            %cross each other
        end
        I=find(out,1);

        if isempty(I)
            I=nan;
        end
        matpcrossori=I;

end