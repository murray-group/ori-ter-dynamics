%% Plot foci positions of individual cell cycles

function plot_foci_positions(cycle,fluor_channel_1,fluor_channel_2)

fc1=strcat("foci",string(fluor_channel_1));
fc2=strcat("foci",string(fluor_channel_2));
cycle.cycle_id
figure
plot(1:cycle.duration,cycle.lengths/max(cycle.lengths),'k',1:cycle.duration,-cycle.lengths/max(cycle.lengths),'k');
hold on
plot(cycle.(fc1).inferred_position_relative(:,:,1)*(-cycle.pole)/max(cycle.lengths)*2,'o-','MarkerFaceColor','r','Color','r');
plot(cycle.(fc2).inferred_position_relative(:,:,1)*(-cycle.pole)/max(cycle.lengths)*2,'o-','MarkerFaceColor','g','Color','g');
hold off
xlabel("Frames (5 min interval)")
ylabel("Position inside cell")
% yticks = linspace(-1,1,5);
% set(gca,'YTick',yticks);
end