function n_lost=lost_number_of_foci(cycle_list)
for i=1:length(cycle_list)
    if ~isempty(cycle_list(i).foci3.position)
        for frame=1:cycle_list(i).duration
            
            n_spots{i,1}(frame)=sum(~isnan(cycle_list(i).foci3.position(frame,:,1)));
            
        end
        n_lost(i)=sum(diff(n_spots{i})<0);
    end
end

figure
edges=(-0.5:1:15.5);
histogram(n_lost,edges)
end
