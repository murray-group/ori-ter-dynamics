function hist_to_line(data, flag)

% Plot the histogram
% h=histogram(data,'Normalization', 'count'); % plot the histogram with count normalization
% hold on % hold on to the current plot so we can add to it
% Convert the histogram to an area plot

if flag 
 [counts, centers] = histcounts(data); % get the bin counts and centers
width = (centers(1:end-1) + centers(2:end))/2; % calculate the width of each bin
area(width, smooth(counts/sum(counts)/(centers(2)-centers(1)))) % plot the area plot   

else
    [counts, centers] = histcounts(data); % get the bin counts and centers
width = (centers(1:end-1) + centers(2:end))/2; % calculate the width of each bin
area(width, smooth(counts)) % plot the area plot
end

% Add labels and legend
% xlabel('Data')
% ylabel('Count')
% legend('Histogram', 'Area Plot')
end