n_bins_x=121;
n_bins_y=200;
bin_edges_x=(-60:1:60);
bin_edges_y=linspace(-0.5,0.5,200);
data=zeros(n_bins_y,n_bins_x);
ndata=zeros(1,n_bins_x);

fc='fluor1_lp';%['fluor',num2str(fluor_channel),'_lp'];

for i = matp_pole_ids'
    i
    I=get_stable_time_at_middle(cycle_list(i),2,3.6);
    if isempty(I) || isnan(I)
        continue;
    end
    for j=1:cycle_list(i).duration
        
        if cycle_list(i).pole==1
            data(:,60-I+j+1)=data(:,60-I+j+1)+interp1(linspace(-0.5,0.5,cycle_list(i).lengths(j)),cycle_list(i).(fc){j}./max(cycle_list(i).(fc){j}),bin_edges_y');
        else
            data(:,60-I+j+1)=data(:,60-I+j+1)+flip(interp1(linspace(-0.5,0.5,cycle_list(i).lengths(j)),cycle_list(i).(fc){j}./max(cycle_list(i).(fc){j}),bin_edges_y'));
        end
        ndata(1,60-I+j+1)=ndata(1,60-I+j+1)+1;
    end
end
data=data./ndata;
figure
imagesc(bin_edges_x*5,-bin_edges_y,data)
ax=gca;
ax.YDir='normal';
xlabel("Time (min)")
ylabel("Relative position (old pole - new pole)")