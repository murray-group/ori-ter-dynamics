function data=synched_kymograph(varargin)
if(nargin<5)
    disp('error')
end
cycle_list=varargin{1};
cycle_ids=varargin{2};
foci_channel=varargin{3};
synch_time_fn=varargin{4};%% a function that takes a cell cycle and some
% number of other paramaters and return the frame at which an event occurs
args_for_synch_func=varargin(5:nargin);% the parameters to be passed through to synch_time_fn

if isempty(cycle_ids)
    cycle_ids=[1:length(cycle_list)]';
end
T0=20;%number of frames before event
T1=20;%number of frames after event
n_pos_bins=51; %use odd number of bins
frame_rate=5;
ndata=zeros(1,41);

pos_edges=linspace(-0.51,0.51,n_pos_bins+1);
frame_edges=linspace(-T0-0.5,T1+0.5,T0+T1+2);
data=zeros(n_pos_bins,T0+T1+1);
counter=0;

for i = cycle_ids
    
    if isfield(cycle_list(i).(['foci',num2str(varargin{3})]),'position_relative') &&...
            ~isempty(cycle_list(i).(['foci',num2str(varargin{3})]).position_relative)
        I=synch_time_fn(cycle_list(i),args_for_synch_func{:});
        if isempty(I)
            continue;
        end
        foci=cycle_list(i).(['foci',num2str(foci_channel)]);
        frames=[];
        positions=[];
        for frame=1:cycle_list(i).duration
            pos=foci.position_relative(frame,:,1)/cycle_list(i).lengths(frame)*(-cycle_list(i).pole);
            pos=pos(~isnan(pos));
            frames=[frames frame*ones(size(pos))];
            positions=[positions pos];
        end
        N=histcounts2(positions,frames-I,pos_edges,frame_edges);
        data=data+N;
        counter=counter+1;
        ndata(find(sum(N),1):find(sum(N),1,'last'))=ndata(find(sum(N),1):find(sum(N),1,'last'))+1;
    end
end
data=data./ndata;%


figure

y_middles=(pos_edges(1:end-1)+pos_edges(2:end))/2;
t_middles=(frame_edges(1:end-1)+frame_edges(2:end))/2;
imagesc(frame_rate*(t_middles),y_middles,data)
ax=gca;
ax.YDir='normal';
xlabel('Time (min)')
ylabel('relative position (old pole-new pole)')
ax.TickDir='out'
end
