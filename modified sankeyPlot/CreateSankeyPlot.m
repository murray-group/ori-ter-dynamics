function CreateSankeyPlot(InputData,labels,timePointNames,C)

% Modified by sean.murray (04.02.2022)
% Flow is coloured according to column C (default C=1)

% Written by roman.kuster@alumni.ethz.ch (24.06.2021)
% Function to create a sankey plot / alluvial flow diagram with numerical
% data stored in InputData (e.g. categories 0, 1, and 2). Each row in 
% InputData represents a subject, and each column represents a timepoint.
% The entries in InputData reflect the group a particular subject was
% assigned to at a particular timepoint, and the plot visualises the flow
% of group membership over all timepoints.
%
% - size(InputData,1) corresponds to the number of subjects
% - size(InputData,2) corresponds to the number of timepoints
% - entries represent the category a subject belongs to at a given timepoint
%
% Example Data with 5 subjects, 4 timepoints, and 3 categories
% InputData = [0,0,1,0;2,1,2,2;1,1,0,1;0,1,1,2;1,2,2,2];
% Te highest category (2 in example data) is assigned to the top bar, the
% lowest category to the bottom bar. Can be modified by change the order in
% categories. The bars are coloured from green (top) to red (bottom), and 
% the percentage of subjects belonging to each category is indicated in the
% middle of the bar.
%
% Function calls the modified function sankey_alluvialflow initially
% written by Alexander Carmeli (Alluvial flow diagram, available from: 
% www.mathworks.com/matlabcentral/fileexchange/66746-alluvial-flow-diagram)
% and modified by Ranran Wang (Sankey Diagram, available from:
% www.mathworks.com/matlabcentral/fileexchange/75813-sankey-diagram).


%% Detect categories used:

for i=1:size(InputData,2)
    tmp=sort(unique(InputData(:,i)),'ascend');
    I=find(isnan(tmp),1);
    if ~isempty(I)
        tmp=tmp(1:(I-1));
    end
    categories{i}=tmp;
end



if(isempty(labels) || length(labels)~= size(InputData,2))
    for i=1:size(InputData,2)
        labels{i}=cellstr(num2str(categories{i}));
    end
end

%% stacked Bars for each category at each timepoint t:
for t = 1:size(InputData,2)
    for c = 1:length(categories{t})
        Bars{t}(1,c) = sum(InputData(:,t)==categories{t}(c)); % sum categories...
        if(isempty(timePointNames))
        timePointNames{t} = ['Time ',num2str(t)];
        end
    end
end


%% Change between timepoint t and t+1 for each category:
if isempty(C)
C=1;
end


for t = 1:size(InputData,2)-1
    for c0 =1:length(categories{C})
        I=InputData(:,C)==categories{C}(c0);
    for c1 = 1:length(categories{t}) % change from 1 category at t...
        for c2 = 1:length(categories{t+1}) % to all other categories at t+1
            Change{t}{c0}(c1,c2) = sum(InputData(I,t) == categories{t}(c1) & InputData(I,t+1) == categories{t+1}(c2));
        end
    end
    end
end


%% Define figure appearance:
% Colors (from green at the top to red at the bottom)
% Colors(1:length(categories0),1) = [0.25: 0.5/(length(categories0)-1) :0.75]; % red
% Colors(1:length(categories0),2) = [0.75: -0.5/(length(categories0)-1) :0.25]; % green
% Colors(1:length(categories0),3) = repmat(0.15,length(categories0),1); % blue
Colors=lines(length(categories{C}));
% Transparency (FaceAlpha) of change-flow:
ChangeTransparancy = 0.5;
% Timepoints on horizontal axis:
X = 0:size(InputData,2)-1;
% Width of the bars:
BarWidth = 30;


%% Create the Figure
figure();
y1_category_points=[];

for t=1:size(InputData,2)-1
    y1_category_points=sankey_alluvialflow(Bars{t}, Bars{t+1}, Change{t}, X(t), X(t+1), y1_category_points,Colors,ChangeTransparancy,BarWidth);
end


%% Add text (% in each category and timepoint)
ymax = 1.1 * size(InputData,1);
gap = (ymax-size(InputData,1)) / (length(categories{1})-1);
for t = 1:size(InputData,2)
    for c = 1:length(categories{t})
        percent=roundn(Bars{t}(c)/sum(~isnan(InputData(:,i)))*100,0);
        NumbToPrint = num2str(percent);
        if(percent>5)
        %text(X(t), Bars{t}(c)/2 + sum(Bars{t}(1:c-1)) + (c-1)*gap, [NumbToPrint,'%'],'HorizontalAlignment','center','Color',[0 0 0])
        text(X(t), Bars{t}(c)/2 + sum(Bars{t}(1:c-1)) + (c-1)*gap, [labels{t}{c},' (',num2str(NumbToPrint),'%)'], 'FontSize', 10, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'Rotation', 90);
        end
    end
    %text(X(t), Bars{t}(c) + sum(Bars{t}(1:c-1)) + (c+1)*gap, timePointNames{t},'HorizontalAlignment','center','Interpreter','None')
    text(X(t), -gap, timePointNames{t},'HorizontalAlignment','center','Interpreter','None')
end
ax=gca;
ax.YDir='normal';


%     Place left labels
%     text(zeros(1, size(change{c0}, 1)) - 0.01, ...
%          y1_category_points + Bars2./2, ...
%          labels, 'FontSize', 12, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', 'Rotation', 90);



return
