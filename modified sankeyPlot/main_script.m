load('C:\Ismath\oriC-matP mat file\combined_data_after_foci_sorting.mat')
%%

%sigmas=[];
%intensities=[];
for i=1:length(cycle_list)
    for frame=1:cycle_list(i).duration
    n_spots{i,1}(frame)=sum(~isnan(cycle_list(i).foci2.position(frame,:,1)));
    end
    n_lost(i)=sum(diff(n_spots{i})<0);
    %sigmas=[sigmas;cycle_list(i).foci1.sigma(~isnan(cycle_list(i).foci1.sigma(:)))];
    %intensities=[intensities;cycle_list(i).foci1.intensity(~isnan(cycle_list(i).foci1.intensity(:)))];
end

figure(1)
histogram(n_lost)

%%
synched_kymograph(cycle_list,complete_cycle_ids',1,@get_time_first_1_to_2_split,2);
title('MatP synched to 1->2 ori splitting')

synched_kymograph(cycle_list,complete_cycle_ids',2,@get_time_first_1_to_2_split,2);
title('ori synched to 1->2 ori splitting')
%%
synched_kymograph(cycle_list,complete_cycle_ids',1,@get_first_time_to_middle,1,1/3);%middle one third
title('MatP synched to MatP first arriving in middle')

synched_kymograph(cycle_list,complete_cycle_ids',2,@get_first_time_to_middle,1,1/3);%middle one third
title('ori synched to MatP first arriving in middle')
%%
synched_kymograph(cycle_list,complete_cycle_ids',1,@get_last_time_to_middle,1,1/6);
title('MatP synched to MatP last time to middle')

synched_kymograph(cycle_list,complete_cycle_ids',2,@get_last_time_to_middle,1,1/6);
title('ori synched to MatP last time to middle')
%%
synched_kymograph(cycle_list,complete_cycle_ids',1,@get_last_time_at_middle,1,1/6);
title('MatP synched to MatP last time at middle')

synched_kymograph(cycle_list,complete_cycle_ids',2,@get_last_time_at_middle,1,1/6);
title('ori synched to MatP last time at middle')
%%
j=1;
ori_at_birth=NaN(length(complete_cycle_ids),1);
ori_at_division=NaN(length(complete_cycle_ids),1);
ori_when_MatP_first_middle=NaN(length(complete_cycle_ids),1);
ori_when_MatP_last_middle=NaN(length(complete_cycle_ids),1);
y=[];
for i=complete_cycle_ids'
    cycle=cycle_list(i);
    ori_at_birth(j)=sum(~isnan(cycle.foci2.position(1,:,1)));
    
    I=get_first_time_to_middle(cycle,1,1/6);

    if(~isempty(I))
        ori_when_MatP_first_middle(j)=sum(~isnan(cycle.foci2.position(I,:,1)));
    end
    
    I=get_last_time_to_middle(cycle,1,1/3);
    y=[y I];
    if(~isempty(I))
        ori_when_MatP_last_middle(j)=sum(~isnan(cycle.foci2.position(I,:,1)));
    end
    ori_at_division(j)=sum(~isnan(cycle.foci2.position(end,:,1)));
    j=j+1;
end

%%
tmp=n_lost(complete_cycle_ids);
I=~isnan(ori_when_MatP_first_middle) & tmp'==0 ;

%CreateSankeyPlot: the second arguement are the labels of the values of each variable. It
%is cell array of cell array. The ith the cell array contains the lables
%for the ith variable (the ith column of the input data). It's length must
%be correspond to the number of distinct values that variable takes. If
%this argument is empty, labels 1..n are used for each variable.
%The third variable is a cell array contaning the labels of the variables

CreateSankeyPlot([ori_at_birth(I),ori_when_MatP_first_middle(I),ori_at_division(I)],[],{'birth','MatP first to middle' ,'division'})


I=n_lost(complete_cycle_ids)==0;
%CreateSankeyPlot([ori_at_birth,ori_at_division],[],{'birth' ,'division'})
