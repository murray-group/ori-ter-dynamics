figure
pos=[];
cycle_ids=cycle_ids_screened;
temp1=[];
cycle_ids_screened=[];
for i=1:numel(a)
    if ~isnan(a(i)) && a(i)>0 && ~isempty(cycle_list(cycle_ids(i)).foci2.inferred_position_relative)
        temp1=(cycle_list(cycle_ids(i)).foci2.inferred_position_relative(a(i),:,1)./cycle_list(cycle_ids(i)).lengths(a(i)))';
        pos=[pos; temp1];
    else
    pos=[pos;nan];    
    end
    if all(abs(temp1)<0.2)
        cycle_ids_screened=[cycle_ids_screened cycle_ids(i)];
    end
end

histogram(pos)
title("Ori foci pos before duplication - zapB")
xlabel("Relative position")