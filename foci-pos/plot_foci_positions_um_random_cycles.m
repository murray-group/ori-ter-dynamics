cycles=randsample(complete_cycle_ids,15)%[120 1200 2400 4600 7000];
%%
%cycles_wt =[43088,5929,52243,47242,2738,53536,32305,65018,53506,3803,8104,26780,42189,53985,9046,32073,28068,5199,4501,44683];
%cycles_c20=[23046,1386,26093,21904,22442,30846,5209,25161,4312,30363,20785,9068,11126,29749,3931,15679,11162,22883,7207,29557]
%cycles_zapB=[52716       60122       27611       43443       10001 9910       61384       20453       51781       57903]
%cycles_zapB[70172       48896        5858       45254       27205       16822       43352       47107        1467       58189]
colors = viridis(10);
i=1;
j=1;
figure
m=1;
while m<=10
    
    
    cycle=cycle_list(cycles(i));
    b=get_stable_time_at_middle(cycle,1,2.4);
    if ~isnan(b)
        
        fluor_channel_1=1;
        %function plot_foci_positions_relative_sync_matp(cycle,fluor_channel_1)
        hold on
        fc1=strcat("foci",string(fluor_channel_1));
        relative_positions=cycle.(fc1).inferred_position_relative(:,:,1)*(-cycle.pole).*0.067;%./cycle.lengths;
        synched_positions=nan(11,size(cycle.foci1.inferred_position_relative(:,:,1),2));
        index_start=b-min(10,b-1);
        index_stop=min(cycle.duration,b+10);
        for k=1:size(synched_positions,2)
            synched_positions(11-min(10,b-1):11+min(cycle.duration-b,10),k)=relative_positions(index_start:index_stop)';
        end
        cycle.cycle_id
        plot(synched_positions,'o-','Color',colors(j,:))%,'MarkerFaceColor','r','Color','r');
        xlabel("Time synched to MatP relocalisation (minutes)")
        ylabel('Position along long axis (\mu m)')
        %ylim([-0.5 0.5])
        xticks=1:1:21;
        xticklabels=-50:5:50;
        % yticks = linspace(-1,1,5);
        set(gca,'XTick',xticks);
        set(gca,'XTickLabel',xticklabels);
        xlim([1 21])
        ylim([-1.5 1.5])
        yline(0.160,'--')
        yline(-0.160,'--')
        hold off
        m=m+1;
        j=j+1;
        
    end
    i=i+1;
    
end



