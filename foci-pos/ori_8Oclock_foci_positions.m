%% Identify cell cycles with 2 tracks and good foci tracking
fc1="foci2"; % 8O clock foci
fc2="foci3"; % ori foci
cycle_ids_good=[];
for i=1:length(complete_cycle_ids)
    cycle=cycle_list(complete_cycle_ids(i));
    if ~isempty(cycle.(fc1).inferred_position_relative) && ~isempty(cycle.(fc2).inferred_position_relative)...
            && size(cycle.(fc1).start_and_stop,1)==2 && size(cycle.(fc2).start_and_stop,1)==2 %Remove cycles with no foci positions as well as foci tracks not equal to 2
        miss_before1=sum(isnan(cycle.(fc1).inferred_position_relative(:,1,1)))+sum(isnan(cycle.(fc1).inferred_position_relative(cycle.(fc1).start_and_stop(2,1):end,2,1)));
        %miss_after1=sum(isnan(cycle.(fc1).inferred_position_relative(cycle.(fc1).start_and_stop(2,1):end,1,1)))+sum(isnan(cycle.(fc1).inferred_position_relative(cycle.(fc1).start_and_stop(2,1):end,2,1)));
        total1=2*cycle.duration-cycle.(fc1).start_and_stop(2,1);
        miss_before2=sum(isnan(cycle.(fc2).inferred_position_relative(:,1,1)))+sum(isnan(cycle.(fc2).inferred_position_relative(cycle.(fc2).start_and_stop(2,1):end,2,1)));
        %miss_after2=sum(isnan(cycle.(fc2).inferred_position_relative(cycle.(fc2).start_and_stop(2,1):end,1,1)))+sum(isnan(cycle.(fc2).inferred_position_relative(cycle.(fc2).start_and_stop(2,1):end,2,1)));
        total2=2*cycle.duration-cycle.(fc2).start_and_stop(2,1);
        if miss_before1/total1<0.5 && miss_before2/total2<0.5
            cycle_ids_good=[cycle_ids_good complete_cycle_ids(i)];
        end
    end
end

%% Generate the difference pairs for single focus pairs - At birth
single_focus_dist=[];
orientations=cell(length(cycle_ids_good),1);
for i=1:length(cycle_ids_good)
    
    cycle=cycle_list(cycle_ids_good(i));
    split_frame=min(cycle.(fc1).start_and_stop(2,1),cycle.(fc2).start_and_stop(2,1));
    if split_frame~=1
        foci1=cycle.(fc1).inferred_position_relative(1,1,1)*-cycle.pole;
        foci2=cycle.(fc2).inferred_position_relative(1,1,1)*-cycle.pole;
        if foci1>foci2
            orientations{i}='NP-Left-Ori-OP';
        elseif foci2>foci1
            orientations{i}='NP-Ori-Left-OP';
        end
        single_focus_dist=[single_focus_dist; (cycle.(fc1).inferred_position_relative(1,1,1)-cycle.(fc2).inferred_position_relative(1,1,1))*-cycle.pole];
    end
end

data = orientations;
data(cellfun(@isempty, data)) = [];
% Count occurrence of each unique combination
[C, ~, labels] = unique(data);
counts = histcounts(labels, 1:numel(C)+1);

% Calculate percentages
percentages_birth = counts / numel(data) * 100;

figure
bar(categorical(C,'ordinal',true),percentages_birth);
xlabel('Orientations');
ylabel('Percentage of cells');
title('Occurrence of orientations - At birth');
figure
histogram(single_focus_dist*.067)
ylabel("count")
xlabel("distance between ori and 8Oclock focus (\mum)")
title("For single foci pairs")


%% Generate the difference pairs for single focus pairs - At ori split
single_focus_dist=[];
orientations=cell(length(cycle_ids_good),1);
counter=0;
for i=1:length(cycle_ids_good)
    
    cycle=cycle_list(cycle_ids_good(i));
    split_frame=cycle.(fc2).start_and_stop(2,1);
    if split_frame>2 && sum(~isnan(cycle.(fc1).inferred_position_relative(split_frame-1,:,1)))==1
        counter=counter+1;
        foci1=cycle.(fc1).inferred_position_relative(split_frame-1,1,1)*-cycle.pole;
        foci2=cycle.(fc2).inferred_position_relative(split_frame-1,1,1)*-cycle.pole;
        foci1=foci1(~isnan(foci1))
        foci2=foci2(~isnan(foci2))
        if foci1>foci2
            orientations{i}='NP-Left-Ori-OP';
        elseif foci2>foci1
            orientations{i}='NP-Ori-Left-OP';
        end
        single_focus_dist=[single_focus_dist; foci1-foci2];
    end
end

data = orientations;
data(cellfun(@isempty, data)) = [];
% Count occurrence of each unique combination
[C, ~, labels] = unique(data);
counts = histcounts(labels, 1:numel(C)+1);

% Calculate percentages
percentages_ori_duplication = counts / numel(data) * 100;

figure
bar(categorical(C,'ordinal',true),percentages_ori_duplication);
xlabel('Orientations');
ylabel('Percentage of cells');
title('Occurrence of orientations - At ori duplication');
figure
histogram(single_focus_dist*.067)
ylabel("count")
xlabel("distance between ori and 8Oclock focus (\mum)")
title("For single foci pairs")

%% Generate the difference pairs for single focus pairs - At 8Oclock split
% single_focus_dist=[];
% orientations=cell(length(cycle_ids_good),1);
% counter=0;
% for i=1:length(cycle_ids_good)
%     
%     cycle=cycle_list(cycle_ids_good(i));
%     split_frame=cycle.(fc1).start_and_stop(2,1);
%     if split_frame>2 && sum(~isnan(cycle.(fc2).inferred_position_relative(split_frame-2,:,1)))==1
%         counter=counter+1;
%         foci1=cycle.(fc1).inferred_position_relative(split_frame-2,:,1)*-cycle.pole
%         foci2=cycle.(fc2).inferred_position_relative(split_frame-2,:,1)*-cycle.pole
%         foci1=foci1(~isnan(foci1))
%         foci2=foci2(~isnan(foci2))
%         if foci1>foci2
%             orientations{i}='NP-Left-Ori-OP';
%         elseif foci2>foci1
%             orientations{i}='NP-Ori-Left-OP';
%         end
%         single_focus_dist=[single_focus_dist; foci1-foci2];
%     end
% end
% 
% data = orientations;
% data(cellfun(@isempty, data)) = [];
% % Count occurrence of each unique combination
% [C, ~, labels] = unique(data);
% counts = histcounts(labels, 1:numel(C)+1);
% 
% % Calculate percentages
% percentages = counts / numel(data) * 100;
% 
% figure
% bar(categorical(C,'ordinal',true),percentages);
% xlabel('Orientations');
% ylabel('Percentage of cells');
% title('Occurrence of orientations - At ori duplication');
% figure
% histogram(single_focus_dist*.067)
% ylabel("count")
% xlabel("distance between ori and 8Oclock focus (\mum)")
% title("For single foci pairs")
%% Generate the difference pairs for double foci pairs - last frame
double_focus_dist=[];
orientations=cell(length(cycle_ids_good),1);
k=0;
for i=1:length(cycle_ids_good)
    
    cycle=cycle_list(cycle_ids_good(i));
    if sum(~isnan(cycle.(fc1).inferred_position_relative(end,:,1)))==2 && sum(~isnan(cycle.(fc2).inferred_position_relative(end,:,1)))==2
        k=k+1;
        foci1=cycle.(fc1).inferred_position_relative(end,:,1)*-cycle.pole;
        foci2=cycle.(fc2).inferred_position_relative(end,:,1)*-cycle.pole;
        all_foci_sorted=sort([foci1 foci2],'descend');
        for j=1:numel(all_foci_sorted)
            if ismember(all_foci_sorted(j),foci1)
                orientations{i}=[orientations{i} 'L'];
            else
                orientations{i}=[orientations{i} 'O'];
            end
            
        end
    end
end
data = orientations;
data(cellfun(@isempty, data)) = [];

% Count occurrence of each unique combination
[C, ~, labels] = unique(data);
counts = histcounts(labels, 1:numel(C)+1);

% Calculate percentages
percentages_last_frame = counts / numel(data) * 100;

% Combine 'OOLL' and 'LLOO'
idx_OOLL = find(strcmp(C, 'OOLL'));
idx_LLOO = find(strcmp(C, 'LLOO'));
counts_OOLL_LLOO = sum(counts([idx_OOLL, idx_LLOO]));
percentages_OOLL_LLOO = sum(percentages_last_frame([idx_OOLL, idx_LLOO]));

% Combine 'LOLO' and 'OLOL'
idx_LOLO = find(strcmp(C, 'LOLO'));
idx_OLOL = find(strcmp(C, 'OLOL'));
counts_LOLO_OLOL = sum(counts([idx_LOLO, idx_OLOL]));
percentages_LOLO_OLOL = sum(percentages_last_frame([idx_LOLO, idx_OLOL]));

% Update C, counts, and percentages
C([idx_OOLL, idx_LLOO, idx_LOLO, idx_OLOL]) = [];
counts([idx_OOLL, idx_LLOO, idx_LOLO, idx_OLOL]) = [];
percentages_last_frame([idx_OOLL, idx_LLOO, idx_LOLO, idx_OLOL]) = [];

C = [C; 'OOLL/LLOO'; 'LOLO/OLOL'];
counts = [counts, counts_OOLL_LLOO, counts_LOLO_OLOL];
percentages_last_frame = [percentages_last_frame, percentages_OOLL_LLOO, percentages_LOLO_OLOL]

% Plot
figure
bar(categorical(C,'ordinal',true),percentages_last_frame);
xlabel('Orientations');
ylabel('Percentage of cells');
title('Occurrence of orientations - Before division');

%% Generate the difference pairs for double foci pairs all frames
double_focus_dist=[];
orientations=cell(100000,1);
m=1;
for i=1:length(cycle_ids_good)
    
    cycle=cycle_list(cycle_ids_good(i));
    split_frame=max(cycle.(fc1).start_and_stop(2,1),cycle.(fc2).start_and_stop(2,1));
    for j=split_frame+1:cycle.duration
        
        if sum(~isnan(cycle.(fc1).inferred_position_relative(j,:,1)))==2 && sum(~isnan(cycle.(fc2).inferred_position_relative(j,:,1)))==2
            
            foci1=cycle.(fc1).inferred_position_relative(j,:,1)*-cycle.pole;
            foci2=cycle.(fc2).inferred_position_relative(j,:,1)*-cycle.pole;
            all_foci_sorted=sort([foci1 foci2],'descend');
            for k=1:numel(all_foci_sorted)
                if ismember(all_foci_sorted(k),foci1)
                    orientations{m}=[orientations{m} 'L'];
                else
                    orientations{m}=[orientations{m} 'O'];
                end
                
            end
            m=m+1;
        end
    end
end
data = orientations;
data(cellfun(@isempty, data)) = [];

% Count occurrence of each unique combination
[C, ~, labels] = unique(data);
counts = histcounts(labels, 1:numel(C)+1);

% Calculate percentages
percentages = counts / numel(data) * 100;

% Combine 'OOLL' and 'LLOO'
idx_OOLL = find(strcmp(C, 'OOLL'));
idx_LLOO = find(strcmp(C, 'LLOO'));
counts_OOLL_LLOO = sum(counts([idx_OOLL, idx_LLOO]));
percentages_OOLL_LLOO = sum(percentages([idx_OOLL, idx_LLOO]));

% Combine 'LOLO' and 'OLOL'
idx_LOLO = find(strcmp(C, 'LOLO'));
idx_OLOL = find(strcmp(C, 'OLOL'));
counts_LOLO_OLOL = sum(counts([idx_LOLO, idx_OLOL]));
percentages_LOLO_OLOL = sum(percentages([idx_LOLO, idx_OLOL]));

% Update C, counts, and percentages
C([idx_OOLL, idx_LLOO, idx_LOLO, idx_OLOL]) = [];
counts([idx_OOLL, idx_LLOO, idx_LOLO, idx_OLOL]) = [];
percentages([idx_OOLL, idx_LLOO, idx_LOLO, idx_OLOL]) = [];

C = [C; 'OOLL/LLOO'; 'LOLO/OLOL'];
counts = [counts, counts_OOLL_LLOO, counts_LOLO_OLOL];
percentages = [percentages, percentages_OOLL_LLOO, percentages_LOLO_OLOL]

% Plot
figure
bar(categorical(C,'ordinal',true),percentages);
xlabel('Orientations');
ylabel('Percentage of cells');
title('Occurrence of orientations - After replication');
