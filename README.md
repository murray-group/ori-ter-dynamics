Matlab scripts used for the article

Ismath Sadhir & Seán M. Murray, Mid-cell migration of the chromosomal terminus is coupled to origin segregation in Escherichia coli, bioRxiv https://doi.org/10.1101/2023.03.24.534095
