function [mother_cycle_ids,other_cycle_ids]=get_the_mothers_out(cycle_list,channel_list)
complete_cycle_ids = get_complete_cycle_ids(cycle_list);
idx_complete=ismember(1:numel(cycle_list),complete_cycle_ids); % idx is logical indices

mother_cycle_ids=get_mother_cycle_ids(channel_list,cycle_list);
idx_mothers=ismember(1:numel(cycle_list),mother_cycle_ids); % idx is logical indices
idx=idx_complete & ~idx_mothers; %complete but not mothers
other_cycle_ids=1:numel(cycle_list);
other_cycle_ids=other_cycle_ids(idx);
end