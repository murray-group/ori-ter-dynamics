function growth_rates=growth_rate(cycle_list,cycle_ids)

    growth_rates = nan(numel(cycle_ids),1);
fieldsToKeep = {'cycle_id', 'areas','times'};
cycle_list=rmfield(cycle_list, setdiff(fieldnames(cycle_list), fieldsToKeep)); % To reduce ram usage when cycle_list is broadcasted in a parfor loop
    parfor i = 1:numel(cycle_ids)
        i
        if ~mod(i,1000)
            disp(strcat("First ",num2str(i)," are done"))
        end
        cycle_id = cycle_ids(i);
        cycle = cycle_list(cycle_id);
        if cycle.times(1) > -1
            areas = log(cycle.areas);
            mdl = fitlm(1:numel(areas),areas);
            growth_rate = mdl.Coefficients{2,1};
            if growth_rate>=0
            growth_rates(i) = growth_rate;
            end
        end
    end
end