%% Run startrack before you run the commands



%% Complete cycle ids
complete_cycle_ids = get_complete_cycle_ids(cycle_list);

%% Kymograph of ori foci positions - cell age

%Ori
foci_kymograph(cycle_list,complete_cycle_ids,100,25,2,-.75);
colormap(viridis(500))
%%
%MatP
foci_kymograph(cycle_list,complete_cycle_ids,100,25,1,0);
colormap(viridis(500))

%% MatP synched to MatP relocalization

synched_kymograph2(cycle_list,complete_cycle_ids,1,@get_MatP_stable_at_middle,1,2.4,3);
hold on
xline(0,'--r')
hold off
set(gca,'Xtick',-100:25:100)
set(gca,'XtickLabel',-100:25:100)
colormap(viridis(500))
colorbar

%% Ori synched to MatP relocalizaton

synched_kymograph2(cycle_list,complete_cycle_ids,2,@get_MatP_stable_at_middle,1,2.4,3);
hold on
xline(0,'--r')
hold off
set(gca,'Xtick',-100:25:100)
set(gca,'XtickLabel',-100:25:100)
colormap(viridis(500))
colorbar

%% MatP relative to ori duplication
synched_kymograph2(cycle_list,complete_cycle_ids,1,@get_ori_split,2,4,9);
hold on
xline(0,'--r')
hold off
set(gca,'Xtick',-100:25:100)
set(gca,'XtickLabel',-100:25:100)
colormap(viridis(500))
colorbar
%% Ori velocity synched to matp relocalization separated by poles

% velocity towards the nearest pole
forward_velocity_oldvsnewpole(cycle_list,complete_cycle_ids',2,@get_MatP_stable_at_middle,1,2.4,3);
xline(0,'--')
yline(0,'--')
%% MatP velocity synched to matp relocalization

% Velocity towards old pole
forward_velocity_matp(cycle_list,complete_cycle_ids',1,@get_MatP_stable_at_middle,1,2.4,3);
xline(0,'--')
yline(0,'--')

%% Paired difference - frames/minutes
a=get_ori_split(cycle_list,complete_cycle_ids,2,4,9);
b=get_MatP_stable_at_middle(cycle_list,complete_cycle_ids,1,2.4,3);
% The above functions return frames where the condition is true for the
% first time.
% -1 - Second last frame of the parent cell cycle
% 0 - Last frame of parent cell cycle
% 1 - First frame of cycle113
% This means, when the frame is converted into time, a subtraction of 1 frame has to be carried out
% because, lets say the condition is true in the first frame of the cell
% cycle, then the value returned would be 1, which when converted into
% times becomes 5 minutes, but in reality it should be 0 because only then
% it makes sense to say the cell was first detected satisfying the
% condition upon the birth of a cell
a=a-1; b=b-1; % Subtraction of one frame before conversion into time/minutes
paired_difference(a*5,...
    b*5,[-50,150],[0,0.99],{'T_{ori}','T_{MatP}','T_{MatP}-T_{ori}'},'Time (min)')