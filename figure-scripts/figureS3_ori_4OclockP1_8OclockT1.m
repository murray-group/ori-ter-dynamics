
%% Script to generate graphs in Figure 2 - ori and Hu

% Execute scripts from top to bottom. Some of the sections appearing later
% in the file might need outputs from sections appearing in the beginning
% for proper execution


%% Complete cycle ids
complete_cycle_ids = get_complete_cycle_ids(cycle_list);

%% Separate complete cycle ids into mother cells and other cycle ids
idx_complete=ismember(1:numel(cycle_list),complete_cycle_ids); % idx is logical indices

mothers=get_mother_cycle_ids(channel_list,cycle_list);
idx_mothers=ismember(1:numel(cycle_list),mothers); % idx is logical indices
idx=idx_complete & ~idx_mothers; %complete but not mothers
cycle_ids=1:numel(cycle_list);
other_cycle_ids=cycle_ids(idx);

%% Kymograph of ori foci positions - cell age

%Ori
[canvas_ori]=foci_kymograph(cycle_list,complete_cycle_ids,100,26,3);
colormap(viridis(500))
%%
%MatP
[canvas_matp]=foci_kymograph(cycle_list,complete_cycle_ids,100,26,2);
colormap(viridis(500))

%% Line profile kymograph and contour plot of Hu-mCh

figure
[canvas_Hu] = kymograph(cycle_list,complete_cycle_ids,100,26,1);
colormap(viridis(500))

%% Contour plot of Hu

figure
M=contourf(linspace(0,1,26),linspace(-1,1,100),canvas_Hu,[474 641]);
%% Get the percentile of signal for the chosen intensity value
get_percentile_contour(canvas_Hu,474)
%% Contour plot of Hu overlaid Hu line profile kymograph
s=contourdata(M);
u_levels=unique([s.level]);
figure
[canvas_Hu] = kymograph(cycle_list,complete_cycle_ids,100,26,1);
colormap(viridis(500))
hold on
for i=1:size(s,2)
    if s(i).level==u_levels(1)
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'LineStyle','--','color','r')
    else
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'color','r')
    end
end
hold off

%% Contour plot of Hu overlaid on ori positions kymograph

[canvas_ori]=foci_kymograph(cycle_list,complete_cycle_ids,100,26,3,-2.4579);
colormap(viridis(500))

hold on
for i=1:size(s,2)
    if s(i).level==u_levels(1)
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'LineStyle','--','color','r')
    else
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'color','r')
    end
end
hold off

%% Contour plot of Hu overlaid on MatP positions kymograph

[canvas_matp]=foci_kymograph(cycle_list,complete_cycle_ids,100,26,2,-1.3476);
colormap(viridis(500))
hold on
for i=1:size(s,2)
    if s(i).level==u_levels(1)
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'LineStyle','--','color','r')
    else
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'color','r')
    end
end
hold off



%% Misc -------------------------------------------------------------------------------------------------------------------------------------
%--------------------------------------------------------------------------------------------------------------------------------------------

%% Overlay of ori positions with Hu
canvas1=canvas_ori;
canvas2=canvas_matp;
figure
canvas_ori2=canvas1;
canvas_ori2=canvas1./max(canvas1);

canvas_Hu2=canvas2;
canvas_Hu2=canvas2./max(canvas2);

% canvas_Hu2(canvas_Hu2<0.20)=0;
% canvas_ori2(canvas_ori2<0.55)=0;

canvas_Hu2=canvas_Hu2;%-0.3;
%canvas_ori2=canvas_ori2-0.1;

image=ones(100,26,3);
image(:,:,2)=canvas_ori2;
image(:,:,1)=canvas_Hu2;
%image(isnan(image))=1;
blue = image(:,:,3);
red = image(:,:,1);
green = image(:,:,2);
nan_values = isnan(image(:,:,1)) | isnan(image(:,:,2));
blue(~nan_values) = 0;
blue(nan_values) = 1;
red(nan_values) = 1;
green(nan_values) = 1;
image(:,:,3)=blue;
image(:,:,1)=red;
image(:,:,2)=green;

imagesc(linspace(0,1,26+1),linspace(-1,1,100+1),image);

xticks = linspace(0,1,5);
%xticks = x*(nbins_x-1)+1;
xlabel("Relative cell age")
set(gca,'XTick',xticks);

yticks = linspace(-1,1,5);
%yticks = x*(nbins_y-1)+1;
set(gca,'YTick',yticks);
ylabel("Relative cell length")

set(gca,'YDir','normal');

% n_buckets_x=26;
% n_buckets_y=100;
% x = linspace(0,1,5);
% xticks = x*(n_buckets_x-1)+1;
% xlabel("relative cell age")
% set(gca,'XTick',xticks, 'XTickLabel', x);
% 
% y = linspace(2,0,5);
% yticks = x*(n_buckets_y-1)+1;
% set(gca,'YTick',yticks, 'YTickLabel', -1*y+1);
% ylabel("relative cell length")





%% Find depth threshold and plot it with SD and SEM (SI)

find_depth_threshold(cycle_list,complete_cycle_ids,26,1);





%% Paired difference - frames/minutes
a=get_ori_split(cycle_list,complete_cycle_ids,3,4,9);
b=get_MatP_stable_at_middle(cycle_list,complete_cycle_ids,2,2.4,3);
% The above functions return frames where the condition is true for the
% first time.
% -1 - Second last frame of the parent cell cycle
% 0 - Last frame of parent cell cycle
% 1 - First frame of cycle
% This means, when the frame is converted into time, a subtraction of 1 frame has to be carried out
% because, lets say the condition is true in the first frame of the cell
% cycle, then the value returned would be 1, which when converted into
% times becomes 5 minutes, but in reality it should be 0 because only then
% it makes sense to say the cell was first detected satisfying the
% condition upon the birth of a cell
a=a-1; b=b-1; % Subtraction of one frame before conversion into time/minutes
paired_difference(a*5,...
    b*5,[-50,150],[0,0.99],{'T_{ori}','T_{MatP}','T_{MatP}-T_{ori}'},'Time (min)')

%% Growth rates of cell cycles (SI)

growth_rates2=growth_rate(cycle_list,complete_cycle_ids);

%% Plotting violins for growth rates
a_trimmed= [];
b_trimmed= [];
a_trimmed= growth_rates;%(growth_rates>=prctile(growth_rates,0) & growth_rates<=prctile(growth_rates,99));
b_trimmed= growth_rates2;%(growth_rates2>=prctile(growth_rates2,0) & growth_rates2<=prctile(growth_rates2,99));

% a_trimmed= growth_rates(growth_rates<=99);
% b_trimmed= growth_rates2(growth_rates2<=99);

%%
a=nan(1,numel(growth_rates));
b=nan(1,numel(growth_rates));
%%
a(1:length(a_trimmed))=a_trimmed;
b(1:length(b_trimmed))=b_trimmed;

%%
d=[a' b'];
figure
violinplot(d./5,{'Fig 1','Fig 2'},'ShowData',false);
ylim([0 0.015])
%%
figure
histogram(growth_rates./5);
xlabel("Growth rate (min^-1)")
ylabel("Count")

%% Cell cycle durations (SI)

durations = zeros(1,numel(complete_cycle_ids));
birth_lengths = zeros(1,numel(complete_cycle_ids));
division_lengths = zeros(1,numel(complete_cycle_ids));
for i = 1:numel(complete_cycle_ids)
    cycle_id = complete_cycle_ids(i);
    cycle = cycle_list(cycle_id);
    durations(i) = cycle.duration;
    birth_lengths(i) = cycle.lengths(1);
    division_lengths(i) = cycle.lengths(end);
end

figure
histogram(durations*5);
xlabel ("Cell cycle durations (min)")
ylabel ("Count")

figure
histogram(birth_lengths*.067,39,'Normalization','pdf')
hold on
histogram(division_lengths*.067,53,'Normalization','pdf')
xlabel("Cell length")
ylabel("Density")
legend('Birth','Division')

hold off

%% Growth rates (violin plot) of Fig.1 and Fig.2 strains


%% Functions

function s=contourdata(c)
%CONTOURDATA Extract Contour Data from Contour Matrix C.
% CONTOUR, CONTOURF, CONTOUR3, and CONTOURC all produce a contour matrix
% C that is traditionally used by CLABEL for creating contour labels.
%
% S = CONTOURDATA(C) extracts the (x,y) data pairs describing each contour
% line and other data from the contour matrix C. The vector array structure
% S returned has the following fields:
%
% S(k).level contains the contour level height of the k-th line.
% S(k).numel contains the number of points describing the k-th line.
% S(k).isopen is True if the k-th contour is open and False if it is closed.
% S(k).xdata contains the x-axis data for the k-th line as a column vector.
% S(k).ydata contains the y-axis data for the k-th line as a column vector.
%
% For example: PLOT(S(k).xdata,S(k).ydata)) plots just the k-th contour.
%
% See also CONTOUR, CONTOURF, CONTOUR3, CONTOURC.
% From the help text of CONTOURC:
%   The contour matrix C is a two row matrix of contour lines. Each
%   contiguous drawing segment contains the value of the contour,
%   the number of (x,y) drawing pairs, and the pairs themselves.
%   The segments are appended end-to-end as
%
%       C = [level1 x1 x2 x3 ... level2 x2 x2 x3 ...;
%            pairs1 y1 y2 y3 ... pairs2 y2 y2 y3 ...]
% D.C. Hanselman, University of Maine, Orono, ME 04469
% MasteringMatlab@yahoo.com
% Mastering MATLAB 7
% 2007-05-22
if nargin<1 || ~isfloat(c) || size(c,1)~=2 || size(c,2)<4
    error('CONTOURDATA:rhs',...
        'Input Must be the 2-by-N Contour Matrix C.')
end
tol=1e-12;
k=1;     % contour line number
col=1;   % index of column containing contour level and number of points
while col<size(c,2); % while less than total columns in c
    s(k).level = c(1,col); %#ok
    s(k).numel = c(2,col); %#ok
    idx=col+1:col+c(2,col);
    s(k).xdata = c(1,idx).'; %#ok
    s(k).ydata = c(2,idx).'; %#ok
    s(k).isopen = abs(diff(c(1,idx([1 end]))))>tol || ...
        abs(diff(c(2,idx([1 end]))))>tol; %#ok
    k=k+1;
    col=col+c(2,col)+1;
end
end

function out=get_percentile_contour(canvas_Hu,s_thresh)


for i=1:size(canvas_Hu,2)
    
    s=canvas_Hu(:,i);%/nansum(canvas_Hu(:,i));
    
    
    S(i)=nansum(s(s>=s_thresh))/nansum(s);
    
end

out=mean(S);

end
