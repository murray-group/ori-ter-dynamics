%% Complete cycle ids
complete_cycle_ids = get_complete_cycle_ids(cycle_list);

%% Run this in case if line profiles aren't present in the cycle_list 
%[cycle_list] = add_lineprofiles(channel_list,cycle_list,1,[1 0],[0,0;0,0]); 

%% Find depth threshold and plot it with SD and SEM (SI)

find_depth_threshold(cycle_list,matp_pole_ids,26,1);

%% Plot line profiles of a cell
m=find([cycle_list(complete_cycle_ids).duration]==25);
plot_lp_hu_cell_cycle(cycle_list(complete_cycle_ids(m(57))),1,0.12898)

%% Paired difference of MatP centralization and nucleoid constriction

a=get_MatP_stable_at_middle(cycle_list,complete_cycle_ids,2,2.4,3);
b=get_times_nucleoid_constrict(cycle_list,complete_cycle_ids,1,0.12898);
% The above functions return frames where the condition is true for the
% first time.
% -1 - Second last frame of the parent cell cycle
% 0 - Last frame of parent cell cycle
% 1 - First frame of cycle
% This means, when the frame is converted into time, a subtraction of 1 frame has to be carried out
% because, lets say the condition is true in the first frame of the cell
% cycle, then the value returned would be 1, which when converted into
% times becomes 5 minutes, but in reality it should be 0 because only then
% it makes sense to say the cell was first detected satisfying the
% condition upon the birth of a cell
%%
a=a-1; b=b-1; % Subtraction of one frame before conversion into time/minutes
%
paired_difference(a*5,...
    b*5,[-30,250],[0,0.99],{'T_{MatP}','T_{Nucleoid}','T_{Nucleoid}-T_{MatP}'},'Time (min)')


%% Frames at which events occur

a=get_ori_split(cycle_list,complete_cycle_ids,3,4,9); % ori splitting

b=get_MatP_stable_at_middle(cycle_list,complete_cycle_ids,2,2.4,3); % matp centralization

c=get_times_nucleoid_constrict(cycle_list,complete_cycle_ids,1,0.12898); % Nucleoid constriction

%% Alluvial plot to show occurrence of events

times=[a' b' c'];
times_sorted=nan(size(times));
for i=1:size(times,1)
    if any(isnan(times(i,:)))
        continue;
    else
        [~,I]=sort(times(i,:));
        times_sorted(i,:)=I;
    end
end

CreateSankeyPlot(times_sorted,[],{'T_{ori}','T_{MatP}','T_{Nucleoid}'},3)

%% Violins for timings of ori splitting, MatP centralization and Nucleoid constriction (SI)
a_trimmed= [];
b_trimmed= [];
c_trimmed= [];
a_trimmed= a(a<=prctile(a,99));
b_trimmed= b(b<=prctile(b,99));
c_trimmed= c(c<=prctile(c,99));
%
a=nan(1,size(complete_cycle_ids,2));
b=nan(1,size(complete_cycle_ids,2));
c=nan(1,size(complete_cycle_ids,2));
a(1:length(a_trimmed))=a_trimmed;
b(1:length(b_trimmed))=b_trimmed;
c(1:length(c_trimmed))=c_trimmed;
%
d=[a' b' c'];
d=(d-1).*5;
%
figure
violinplot(d,{'T_{ori}','T_{MatP}','T_{Nucleoid}'},'ShowData',false);
ylim([-60 250])

%% Lengths at nucleoid constriction overlaid on all cell lengths

all_cell_lengths=cell_lengths(cycle_list,complete_cycle_ids);
cell_len_constr=cell_lengths_at_constriction(cycle_list,complete_cycle_ids,b);
cell_len_ter=cell_lengths_at_centralisation(cycle_list,complete_cycle_ids,a);
%%
figure
[~,N]=histcounts(all_cell_lengths);
[f,xi] = ksdensity(all_cell_lengths*.067,'NumPoints',numel(N)-1);
area(xi,f);
%histogram(birth_lengths*.067,28,'Normalization','pdf')
hold on
[~,N]=histcounts(cell_len_constr);
[f,xi] = ksdensity(cell_len_constr*.067,'NumPoints',numel(N)-1);
area(xi,f);
% histogram(division_lengths*.067,45,'Normalization','pdf')
xlabel("Cell length")
ylabel("PDF")
legend('All','Length at T_{nucl}')
hold off


%% Cell cycle durations (SI)

durations = zeros(1,numel(complete_cycle_ids));
birth_lengths = zeros(1,numel(complete_cycle_ids));
division_lengths = zeros(1,numel(complete_cycle_ids));
for i = 1:numel(complete_cycle_ids)
    cycle_id = complete_cycle_ids(i);
    cycle = cycle_list(cycle_id);
    durations(i) = cycle.duration;
    birth_lengths(i) = cycle.lengths(1);
    division_lengths(i) = cycle.lengths(end);
end


figure
[~,N]=histcounts(birth_lengths);
[f,xi] = ksdensity(birth_lengths*.067,'NumPoints',numel(N)-1);
area(xi,f);
%histogram(birth_lengths*.067,28,'Normalization','pdf')
hold on
[~,N]=histcounts(division_lengths);
[f,xi] = ksdensity(division_lengths*.067,'NumPoints',numel(N)-1);
area(xi,f);

[~,N]=histcounts(cell_len_ter);
[f,xi] = ksdensity(cell_len_ter*.067,'NumPoints',numel(N)-1);
area(xi,f);

[~,N]=histcounts(cell_len_constr);
[f,xi] = ksdensity(cell_len_constr*.067,'NumPoints',numel(N)-1);
area(xi,f);

% histogram(division_lengths*.067,45,'Normalization','pdf')
hold off
xlabel("Cell length")
ylabel("PDF")
legend('Birth','Division','Centralisation','Constriction')

%% Functions

function cell_len_constr=cell_lengths_at_constriction(cycle_list,complete_cycle_ids,b)
cell_len_constr=nan(1,numel(complete_cycle_ids));

for i=1:length(complete_cycle_ids)
    if ~isnan(b(i))
        cell_len_constr(i)=cycle_list(complete_cycle_ids(i)).lengths(b(i));
    end
end
end

function cell_len_ter=cell_lengths_at_centralisation(cycle_list,complete_cycle_ids,a)
cell_len_ter=nan(1,numel(complete_cycle_ids));

for i=1:length(complete_cycle_ids)
    if ~isnan(a(i)) && a(i)>=1
        cell_len_ter(i)=cycle_list(complete_cycle_ids(i)).lengths(a(i));
    end
end
end