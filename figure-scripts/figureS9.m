%% -------- HU-Ori-Ter kymographs on different growth media -----------

%% get complete cell cycle ids
complete_cycle_ids = get_complete_cycle_ids(cycle_list);

%% Separate complete cycle ids into mother cells and other cycle ids
idx_complete=ismember(1:numel(cycle_list),complete_cycle_ids); % idx is logical indices

mothers=get_mother_cycle_ids(channel_list,cycle_list);
idx_mothers=ismember(1:numel(cycle_list),mothers); % idx is logical indices
idx=idx_complete & ~idx_mothers; %complete but not mothers
cycle_ids=1:numel(cycle_list);
other_cycle_ids=cycle_ids(idx);


%% ------------------------------------------------- AB minimal media ---------------------------------------------

%% Line profile kymograph and contour plot of Hu-mCh

figure
[canvas_Hu] = kymograph(cycle_list,other_cycle_ids,100,32,1);
colormap(viridis(500))

%% Kymograph of ori foci positions - cell age

%Ori
[canvas_ori]=foci_kymograph(cycle_list,other_cycle_ids,100,32,3,-2);
colormap(viridis(500))
%%
%MatP
[canvas_matp]=foci_kymograph(cycle_list,complete_cycle_ids,100,32,2,-1);
colormap(viridis(500))

%% Get the percentile of signal for the chosen intensity value
get_percentile_contour(canvas_Hu,447)

%% Contour plot of Hu

figure
M=contourf(linspace(0,1,32),linspace(-1,1,100),canvas_Hu,[447 605]);
%% Contour plot of Hu overlaid Hu line profile kymograph
s=contourdata(M);
u_levels=unique([s.level]);
figure
[canvas_Hu] = kymograph(cycle_list,other_cycle_ids,100,32,1);
colormap(viridis(500))
hold on
for i=1:size(s,2)
    if s(i).level==u_levels(1)
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'LineStyle','--','color','r')
    else
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'color','r')
    end
end
hold off

%% Contour plot of Hu overlaid on ori positions kymograph

[canvas_ori]=foci_kymograph(cycle_list,other_cycle_ids,100,32,3,-2);
colormap(viridis(500))

hold on
for i=1:size(s,2)
    if s(i).level==u_levels(1)
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'LineStyle','--','color','r')
    else
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'color','r')
    end
end
hold off

%% Contour plot of Hu overlaid on MatP positions kymograph

[canvas_matp]=foci_kymograph(cycle_list,other_cycle_ids,100,32,2,-1);
colormap(viridis(500))
hold on
for i=1:size(s,2)
    if s(i).level==u_levels(1)
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'LineStyle','--','color','r')
    else
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'color','r')
    end
end
hold off

%% -------------------------------------------- AB minimal media + Ampicillin -----------------------------------------

%% Line profile kymograph and contour plot of Hu-mCh

figure
[canvas_Hu] = kymograph(cycle_list,other_cycle_ids,100,32,1);
colormap(viridis(500))

%% Kymograph of ori foci positions - cell age

%Ori
[canvas_ori]=foci_kymograph(cycle_list,other_cycle_ids,100,32,3,-2);
colormap(viridis(500))
%%
%MatP
[canvas_matp]=foci_kymograph(cycle_list,complete_cycle_ids,100,26,2,-1);
colormap(viridis(500))

%% Get the percentile of signal for the chosen intensity value
get_percentile_contour(canvas_Hu,438)

%% Contour plot of Hu

figure
M=contourf(linspace(0,1,32),linspace(-1,1,100),canvas_Hu,[438 591]);
%% Contour plot of Hu overlaid Hu line profile kymograph
s=contourdata(M);
u_levels=unique([s.level]);
figure
[canvas_Hu] = kymograph(cycle_list,other_cycle_ids,100,32,1);
colormap(viridis(500))
hold on
for i=1:size(s,2)
    if s(i).level==u_levels(1)
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'LineStyle','--','color','r')
    else
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'color','r')
    end
end
hold off

%% Contour plot of Hu overlaid on ori positions kymograph

[canvas_ori]=foci_kymograph(cycle_list,other_cycle_ids,100,32,3,-2);
colormap(viridis(500))

hold on
for i=1:size(s,2)
    if s(i).level==u_levels(1)
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'LineStyle','--','color','r')
    else
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'color','r')
    end
end
hold off

%% Contour plot of Hu overlaid on MatP positions kymograph

[canvas_matp]=foci_kymograph(cycle_list,other_cycle_ids,100,32,2,-1);
colormap(viridis(500))
hold on
for i=1:size(s,2)
    if s(i).level==u_levels(1)
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'LineStyle','--','color','r')
    else
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'color','r')
    end
end
hold off


%% -------------------------------------------- M9 glycerol minimal media -----------------------------------------

%% Line profile kymograph and contour plot of Hu-mCh

figure
[canvas_Hu] = kymograph(cycle_list,other_cycle_ids,100,32,1);
colormap(viridis(500))

%% Kymograph of ori foci positions - cell age

%Ori
[canvas_ori]=foci_kymograph(cycle_list,other_cycle_ids,100,32,3);
colormap(viridis(500))
%%
%MatP
[canvas_matp]=foci_kymograph(cycle_list,complete_cycle_ids,100,32,2);
colormap(viridis(500))

%% Get the percentile of signal for the chosen intensity value
get_percentile_contour(canvas_Hu,659)

%% Contour plot of Hu

figure
M=contourf(linspace(0,1,32),linspace(-1,1,100),canvas_Hu,[487 659]);
%% Contour plot of Hu overlaid Hu line profile kymograph
s=contourdata(M);
u_levels=unique([s.level]);
figure
[canvas_Hu] = kymograph(cycle_list,other_cycle_ids,100,32,1);
colormap(viridis(500))
hold on
for i=1:size(s,2)
    if s(i).level==u_levels(1)
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'LineStyle','--','color','r')
    else
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'color','r')
    end
end
hold off

%% Contour plot of Hu overlaid on ori positions kymograph

[canvas_ori]=foci_kymograph(cycle_list,other_cycle_ids,100,32,3,-2);
colormap(viridis(500))

hold on
for i=1:size(s,2)
    if s(i).level==u_levels(1)
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'LineStyle','--','color','r')
    else
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'color','r')
    end
end
hold off

%% Contour plot of Hu overlaid on MatP positions kymograph

[canvas_matp]=foci_kymograph(cycle_list,other_cycle_ids,100,32,2,-1);
colormap(viridis(500))
hold on
for i=1:size(s,2)
    if s(i).level==u_levels(1)
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'LineStyle','--','color','r')
    else
        plot(s(i).xdata,s(i).ydata,'LineWidth',1,'color','r')
    end
end
hold off

%% Violin plot for different media conditions

durations_AB_only=[cycle_list(other_cycle_ids).duration]*5;
%%
durations_AB_amp=[cycle_list(other_cycle_ids).duration]*5;
%%
durations_M9_gly=[cycle_list(other_cycle_ids).duration]*5;
%%
durations_M9_glu=[cycle_list(other_cycle_ids).duration]*5;
%%
durations_M9_glu_dual=[cycle_list(other_cycle_ids).duration]*5;

%%

load("F:\tiffs\datasets\Figure S9\durations\durations_AB_amp.mat")
load("F:\tiffs\datasets\Figure S9\durations\durations_AB_only.mat")
load("F:\tiffs\datasets\Figure S9\durations\durations_M9_glu.mat")
load("F:\tiffs\datasets\Figure S9\durations\durations_M9_glu_dual.mat")
load("F:\tiffs\datasets\Figure S9\durations\durations_M9_gly.mat")

%%
[a,b,c,d]=deal(nan(1,numel(durations_M9_glu_dual)));
durations_M9_gly=durations_M9_gly(durations_M9_gly<=400);
durations_AB_only=durations_AB_only(durations_AB_only>=50);
durations_AB_amp=durations_AB_amp(durations_AB_amp>=50);
%a=a(1:length(durations_M9_glu_dual));
a(1:length(durations_M9_glu))=durations_M9_glu;
b(1:length(durations_AB_only))=durations_AB_only;
c(1:length(durations_AB_amp))=durations_AB_amp;
d(1:length(durations_M9_gly))=durations_M9_gly;

violinplot([a' b' c' d'],{'M9 glu','AB','AB amp','M9 gly'},'ShowData',false)
%% ------------------------- Functions --------------------------
function out=get_percentile_contour(canvas_Hu,s_thresh)


for i=1:size(canvas_Hu,2)
    
    s=canvas_Hu(:,i);%/nansum(canvas_Hu(:,i));
    
    
    S(i)=nansum(s(s>=s_thresh))/nansum(s);
    
end

out=mean(S);

end

function s=contourdata(c)
%CONTOURDATA Extract Contour Data from Contour Matrix C.
% CONTOUR, CONTOURF, CONTOUR3, and CONTOURC all produce a contour matrix
% C that is traditionally used by CLABEL for creating contour labels.
%
% S = CONTOURDATA(C) extracts the (x,y) data pairs describing each contour
% line and other data from the contour matrix C. The vector array structure
% S returned has the following fields:
%
% S(k).level contains the contour level height of the k-th line.
% S(k).numel contains the number of points describing the k-th line.
% S(k).isopen is True if the k-th contour is open and False if it is closed.
% S(k).xdata contains the x-axis data for the k-th line as a column vector.
% S(k).ydata contains the y-axis data for the k-th line as a column vector.
%
% For example: PLOT(S(k).xdata,S(k).ydata)) plots just the k-th contour.
%
% See also CONTOUR, CONTOURF, CONTOUR3, CONTOURC.
% From the help text of CONTOURC:
%   The contour matrix C is a two row matrix of contour lines. Each
%   contiguous drawing segment contains the value of the contour,
%   the number of (x,y) drawing pairs, and the pairs themselves.
%   The segments are appended end-to-end as
%
%       C = [level1 x1 x2 x3 ... level2 x2 x2 x3 ...;
%            pairs1 y1 y2 y3 ... pairs2 y2 y2 y3 ...]
% D.C. Hanselman, University of Maine, Orono, ME 04469
% MasteringMatlab@yahoo.com
% Mastering MATLAB 7
% 2007-05-22
if nargin<1 || ~isfloat(c) || size(c,1)~=2 || size(c,2)<4
    error('CONTOURDATA:rhs',...
        'Input Must be the 2-by-N Contour Matrix C.')
end
tol=1e-12;
k=1;     % contour line number
col=1;   % index of column containing contour level and number of points
while col<size(c,2); % while less than total columns in c
    s(k).level = c(1,col); %#ok
    s(k).numel = c(2,col); %#ok
    idx=col+1:col+c(2,col);
    s(k).xdata = c(1,idx).'; %#ok
    s(k).ydata = c(2,idx).'; %#ok
    s(k).isopen = abs(diff(c(1,idx([1 end]))))>tol || ...
        abs(diff(c(2,idx([1 end]))))>tol; %#ok
    k=k+1;
    col=col+c(2,col)+1;
end
end