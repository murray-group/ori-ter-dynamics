load('D:\tiffs\20220624\combined\combined_data.mat')

% Run star track before plotting positions
%% get complete cell cycle ids
complete_cycle_ids = get_complete_cycle_ids(cycle_list);

%% Separate cycles on the basis of MatP position at the last frame in each cycle

[matp_mid,matp_pole,matp_mixd,matp_rest]=sort_matp_localization_at_division(cycle_list,complete_cycle_ids,1);

%%

[matp_mid,matp_pole,matp_mixd,matp_rest,ori_ter_orientation]=sort_matp_localization_at_division_with_birth_orientation(cycle_list,other_cycle_ids,1);

%% Cell age kymograph of MatP

% population with MatP at mid-cell during division

foci_kymograph(cycle_list,matp_mid,100,26,1)
colormap(viridis(500))
title("MatP at mid-cell - MatP")

% Population with MatP at poles during division

foci_kymograph(cycle_list,matp_pole,100,26,1)
colormap(viridis(500))
title("MatP at pole - MatP")

% Population with MatP at poles and midcell during division
% Remember that the pole arrangement is decided according to cells with
% MatP in the pole occupy the top half of the cell
foci_kymograph_cell_age_fig6(cycle_list,matp_mixd,100,26,1)
colormap(viridis(500))
title("MatP at pole and midcell - MatP")



%% Cell age kymograph of Ori

% population with MatP at mid-cell during division
foci_kymograph(cycle_list,matp_mid,100,26,2)
colormap(viridis(500))
title("MatP at mid-cell - Ori")

% Population with MatP at poles during division
foci_kymograph(cycle_list,matp_pole,100,26,2)
colormap(viridis(500))
title("MatP at pole - ori")

% Population with MatP at poles and midcell during division
% Remember that the pole arrangement is decided according to cells with
% MatP in the pole occupy the top half of the cell
foci_kymograph_cell_age_fig6(cycle_list,matp_mixd,100,26,2)
colormap(viridis(500))
title("MatP at pole and midcell - ori")



%% Plot ori and matp foci positions during cell cycle

% MatP at poles
matp_pole_ids=[];
for i=1:numel(matp_pole)
    cycle=cycle_list(matp_pole(i));
    if ~isempty(cycle.foci1.inferred_position_relative) && ~isempty(cycle.foci2.inferred_position_relative)
        if any(~any(isnan(cycle.foci1.inferred_position_relative(:,:,1)))) && any(~any(isnan(cycle.foci2.inferred_position_relative(:,:,1)))) && sum(~isnan(cycle.foci2.inferred_position_relative(end,:,1)))==2
            matp_pole_ids=[matp_pole_ids cycle.cycle_id];
        end
    end
end

plot_foci_positions(cycle_list(matp_pole_ids(53)),1,2)

%% MatP at mid-cell

matp_mid_ids=[];
for i=1:numel(matp_mid)
    cycle=cycle_list(matp_mid(i));
    if any(~any(isnan(cycle.foci1.inferred_position_relative(:,:,1)))) && any(~any(isnan(cycle.foci2.inferred_position_relative(:,:,1)))) && cycle.duration==25
        matp_mid_ids=[matp_mid_ids cycle.cycle_id];
    end
end

plot_foci_positions(cycle_list(matp_mid_ids(1)),1,2)

%% MatP at mid-cell and pole

matp_mixd_ids=[];
for i=1:numel(matp_mixd)
    cycle=cycle_list(matp_mixd(i));
    if any(~any(isnan(cycle.foci1.inferred_position_relative(:,:,1)))) && any(~any(isnan(cycle.foci2.inferred_position_relative(:,:,1)))) && cycle.duration<=26
        matp_mixd_ids=[matp_mixd_ids cycle.cycle_id];
    end
end
%%
plot_foci_positions(cycle_list(matp_mixd_ids(29)),1,2)


% Generate first and last frames using the visualization functions and
% overlay it on Illustrator

%% Growth rate of cells


growth_rate_ori_cfp_matp_pole=growth_rate(cycle_list,matp_pole);
growth_rate_ori_cfp_matp_mid=growth_rate(cycle_list,matp_mid);
growth_rate_ori_cfp_mixd=growth_rate(cycle_list,matp_mixd);
%% Plotting violins
a_trimmed= growth_rate_ori_cfp_mixd;%(growth_rate_ori_cfp_all>=prctile(growth_rate_ori_cfp_all,0) & growth_rate_ori_cfp_all<=prctile(growth_rate_ori_cfp_all,99.99));
b_trimmed= growth_rate_ori_cfp_matp_pole;%(growth_rate_ori_cfp_matp_pole>=prctile(growth_rate_ori_cfp_matp_pole,0) & growth_rate_ori_cfp_matp_pole<=prctile(growth_rate_ori_cfp_matp_pole,99.99));
c_trimmed= growth_rate_ori_cfp_matp_mid;%(growth_rate_ori_cfp_matp_mid>=prctile(growth_rate_ori_cfp_matp_mid,0) & growth_rate_ori_cfp_matp_mid<=prctile(growth_rate_ori_cfp_matp_mid,99.99));
a=nan(1,numel(growth_rate_ori_cfp_matp_mid));
b=nan(1,numel(growth_rate_ori_cfp_matp_mid));
c=nan(1,numel(growth_rate_ori_cfp_matp_mid));

a(1:length(a_trimmed))=a_trimmed;
b(1:length(b_trimmed))=b_trimmed;
c(1:length(c_trimmed))=c_trimmed;

d=[c' a' b'];
figure
violinplot(d./5,{'Midcell','Mixed','Poles'},'ShowData',false);
%ylim([0 0.015])
%%
save('F:\tiffs\datasets\growth_rates\growth_rate_ori_cfp.mat','growth_rate_ori_cfp_all')

%% Total frames for which MatP foci was detected at mid-cell position

[matp_middle_frames_cfp_pole,~]=matp_middle_frames_sum(cycle_list,matp_pole,1);
[matp_middle_frames_cfp_mid,~]=matp_middle_frames_sum(cycle_list,matp_mid,1);

figure
[~,N]=histcounts(matp_middle_frames_cfp_pole);
[f,xi] = ksdensity(matp_middle_frames_cfp_pole,'NumPoints',numel(N)-1);
area(xi,f);

hold on

[~,N]=histcounts(matp_middle_frames_cfp_mid);
[f,xi] = ksdensity(matp_middle_frames_cfp_mid,'NumPoints',numel(N)-1);
area(xi,f);

[~,N]=histcounts(matp_middle_frames_mtq);
[f,xi] = ksdensity(matp_middle_frames_mtq,'NumPoints',numel(N)-1);
area(xi,f);

hold off
xlabel("Total frames")
ylabel("PDF")
legend('Impaired (ori-CFP)','Normal (ori-CFP)','Normal (ori-mTq)')
xlim([0 45])


%%
figure
hist_to_line(matp_middle_frames_cfp_pole,1)
hold on
hist_to_line(matp_middle_frames_cfp_mid,1)
hist_to_line(matp_middle_frames_mtq,1)
hold off

xlabel("Total frames")
ylabel("PDF")
legend('Impaired (ori-CFP)','Normal (ori-CFP)','Normal (ori-mTq)')
xlim([0 45])

%%
[~,ori_matp_coloc_cfp_pole,~]=ori_matp_distance(cycle_list,[matp_pole matp_mixd]);
[~,ori_matp_coloc_cfp_mid,~]=ori_matp_distance(cycle_list,matp_mid);

load('F:\tiffs\datasets\Figure 6 - Ori CFP\ori_matp_colocalization_mtq.mat')

figure
hist_to_line(cellfun(@nansum, ori_matp_coloc_cfp_pole),1)
hold on
hist_to_line(cellfun(@nansum, ori_matp_coloc_cfp_mid),1)
hist_to_line(cellfun(@nansum, ori_matp_coloc_mtq),1)
hold off

xlabel("Total frames")
ylabel("PDF")
legend('Impaired (ori-CFP)','Normal (ori-CFP)','Normal (ori-mTq)')
xlim([0 15])


%% Birth lengths

durations_pole = zeros(1,numel(complete_cycle_ids));
birth_lengths = zeros(1,numel(complete_cycle_ids));
division_lengths = zeros(1,numel(complete_cycle_ids));
for i = 1:numel(complete_cycle_ids)
    cycle_id = complete_cycle_ids(i);
    cycle = cycle_list(cycle_id);
    durations_pole(i) = cycle.duration;
    birth_lengths(i) = cycle.lengths(1);
    division_lengths(i) = cycle.lengths(end);
end
%%
figure
[~,N]=histcounts(birth_lengths);
[f,xi] = ksdensity(birth_lengths*.067,'NumPoints',numel(N)-1);
area(xi,f);
%histogram(birth_lengths*.067,28,'Normalization','pdf')
hold on
[~,N]=histcounts(division_lengths);
[f,xi] = ksdensity(division_lengths*.067,'NumPoints',numel(N)-1);
area(xi,f);
%%
durations_mid = zeros(1,numel(matp_mid));
birth_lengths = zeros(1,numel(matp_mid));
division_lengths = zeros(1,numel(matp_mid));
for i = 1:numel(matp_mid)
    cycle_id = matp_mid(i);
    cycle = cycle_list(cycle_id);
    durations_mid(i) = cycle.duration;
    birth_lengths(i) = cycle.lengths(1);
    division_lengths(i) = cycle.lengths(end);
end
%%
[~,N]=histcounts(birth_lengths);
[f,xi] = ksdensity(birth_lengths*.067,'NumPoints',numel(N)-1);
area(xi,f);
%histogram(birth_lengths*.067,28,'Normalization','pdf')

[~,N]=histcounts(division_lengths);
[f,xi] = ksdensity(division_lengths*.067,'NumPoints',numel(N)-1);
area(xi,f);
% histogram(division_lengths*.067,45,'Normalization','pdf')
xlabel("Cell length")
ylabel("PDF")
legend('Birth-impaired','Division-impaired','Birth-normal','Division-normal')
hold off

% Durations
figure
hist_to_line(durations_pole*5,1)
hold on
hist_to_line(durations_mid*5,1)
hold off

xlabel("Durations")
ylabel("PDF")
legend('Impaired (ori-CFP)','Normal (ori-CFP)')
%% Doubling time of cells

durations_mid = zeros(1,numel(matp_mid));
durations_pole= zeros(1,numel(matp_pole));
durations_mixd=zeros(1,numel(matp_mixd));

for i = 1:numel(matp_mid)
    cycle_id = matp_mid(i);
    cycle = cycle_list(cycle_id);
    durations_mid(i) = cycle.duration;
end
for i = 1:numel(matp_pole)
    cycle_id = matp_pole(i);
    cycle = cycle_list(cycle_id);
    durations_pole(i) = cycle.duration;
end
for i = 1:numel(matp_mixd)
    cycle_id = matp_mixd(i);
    cycle = cycle_list(cycle_id);
    durations_mixd(i) = cycle.duration;
end

%% Plotting violins
a_trimmed= durations_mixd;%(growth_rate_ori_cfp_all>=prctile(growth_rate_ori_cfp_all,0) & growth_rate_ori_cfp_all<=prctile(growth_rate_ori_cfp_all,99.99));
b_trimmed= durations_pole;%(growth_rate_ori_cfp_matp_pole>=prctile(growth_rate_ori_cfp_matp_pole,0) & growth_rate_ori_cfp_matp_pole<=prctile(growth_rate_ori_cfp_matp_pole,99.99));
c_trimmed= durations_mid;%(growth_rate_ori_cfp_matp_mid>=prctile(growth_rate_ori_cfp_matp_mid,0) & growth_rate_ori_cfp_matp_mid<=prctile(growth_rate_ori_cfp_matp_mid,99.99));
a=nan(1,numel(durations_mid));
b=nan(1,numel(durations_mid));
c=nan(1,numel(durations_mid));

a(1:length(a_trimmed))=a_trimmed;
b(1:length(b_trimmed))=b_trimmed;
c(1:length(c_trimmed))=c_trimmed;

d=[c' a' b'];
figure
violinplot(d*5,{'Midcell','Mixed','Poles'},'ShowData',false);

%% Alluvial plot for chromosome orientation

[m,d]=alluvial_plot_for_ter_position(cycle_list,complete_cycle_ids,1);


%% Miscellaneous

% Find fraction of population with parents and children

count=numel(complete_cycle_ids);
p_count=0;
c_count_one=0;
c_count_all=0;
for i=1:numel(complete_cycle_ids)
    cycle=cycle_list(complete_cycle_ids(i));
    if ~isnan(cycle.relation_graph.parent)
        p_count=p_count+1;
    end
    if sum(~isnan(cycle.relation_graph.children))==2
        c_count_all=c_count_all+1;
    end
    if sum(~isnan(cycle.relation_graph.children))==1
        c_count_one=c_count_one+1;
    end
end
disp("Total cycles")
count
disp("Fraction of cycles with a parent")
p_count./count
disp("Fraction of cycles with one child")
c_count_one./count
disp("Fraction of cycles with two children")
c_count_all./count










%%
[m,d]=alluvial_plot_for_ter_position(cycle_list,other_cycle_ids,1);

%%
[m,d]=alluvial_plot_for_ter_position2(cycle_list,[cycle_list.cycle_id],1);
%%
CreateSankeyPlot([m(m==1) d(m==1)],[],{'birth','division'},1)

%%
CreateSankeyPlot([m d],[],{'birth','division'},1)
%% Functions
function [mother_orientation,daughters_orientation]=alluvial_plot_for_ter_position(cycle_list,complete_cycle_ids,fluor_channel)

count=0;
complete_cycles=cycle_list(complete_cycle_ids);
fc=strcat("foci",string(fluor_channel));
mother_orientation=nan(numel(complete_cycles),1);
daughters_orientation=nan(numel(complete_cycles),1);
for i=1:numel(complete_cycles)
    %i
    if isfield(complete_cycles(i).(fc),'inferred_position_relative') && isfield(complete_cycles(i).(fc),'inferred_position_relative') &&...
            ~isempty(complete_cycles(i).(fc).inferred_position_relative) && ~isempty(complete_cycles(i).(fc).inferred_position_relative)...
            && sum(~isnan(complete_cycles(i).relation_graph.children))==2
        %count=count+1
        cycle=complete_cycles(i);
        children=cycle.relation_graph.children;
        child1=cycle_list(children(1));
        child2=cycle_list(children(2));
        if ~isempty(child1.(fc).inferred_position_relative) && ~isempty(child2.(fc).inferred_position_relative)
            if size(cycle.(fc).start_and_stop,1)<=2 && sum(~isnan(child1.(fc).inferred_position_relative(1,:,1)))==1 && sum(~isnan(child2.(fc).inferred_position_relative(1,:,1)))==1
                pos_mother=abs(cycle.(fc).inferred_position_relative(1,:,1));
                
                if pos_mother(~isnan(pos_mother))>=cycle.lengths(1)/6
                    mother_orientation(i)=1;
                elseif pos_mother(~isnan(pos_mother))<cycle.lengths(1)/6
                    mother_orientation(i)=2;
                end
                if ~isnan(mother_orientation(i))
                    
                    
                    pos_child1=abs(child1.(fc).inferred_position_relative(1,:,1));
                    
                    pos_child2=abs(child2.(fc).inferred_position_relative(1,:,1));
                    
                    if pos_child1(~isnan(pos_child1))>=child1.lengths(1)/6 & pos_child2(~isnan(pos_child2))>=child2.lengths(1)/6
                        daughters_orientation(i)=1;
                    elseif  pos_child1(~isnan(pos_child1))<child1.lengths(1)/6 & pos_child2(~isnan(pos_child2))<child2.lengths(1)/6
                        daughters_orientation(i)=2;
                    elseif  (pos_child1(~isnan(pos_child1))>=child1.lengths(1)/6 & pos_child2(~isnan(pos_child2))<child2.lengths(1)/6)...
                            | (pos_child1(~isnan(pos_child1))<child1.lengths(1)/6 & pos_child2(~isnan(pos_child2))>=child2.lengths(1)/6)
                        daughters_orientation(i)=3;
                    end
                end
                
            end
            
        end
        
    end
    
end
end


function [mother_orientation,daughters_orientation]=alluvial_plot_for_ter_position2(cycle_list,complete_cycle_ids,fluor_channel)

count=0;
complete_cycles=cycle_list(complete_cycle_ids);
fc=strcat("foci",string(fluor_channel));
mother_orientation=nan(numel(complete_cycles),1);
daughters_orientation=nan(numel(complete_cycles),1);
for i=1:numel(complete_cycles)
    %i
    if isfield(complete_cycles(i).(fc),'inferred_position_relative') && isfield(complete_cycles(i).(fc),'inferred_position_relative') &&...
            ~isempty(complete_cycles(i).(fc).inferred_position_relative) && ~isempty(complete_cycles(i).(fc).inferred_position_relative)...
            && sum(~isnan(complete_cycles(i).relation_graph.children))==2 && ~isempty(complete_cycles(i).foci2.inferred_position_relative)
        
        cycle=complete_cycles(i);
        children=cycle.relation_graph.children;
        child1=cycle_list(children(1));
        child2=cycle_list(children(2));
        if ~isempty(child1.(fc).inferred_position_relative) && ~isempty(child2.(fc).inferred_position_relative) && ~isempty(child1.foci2.inferred_position_relative) && ~isempty(child2.foci2.inferred_position_relative)
            if sum(~isnan(cycle.(fc).inferred_position_relative(1,:,1)))==1 && sum(~isnan(child1.(fc).inferred_position_relative(1,:,1)))==1 && sum(~isnan(child2.(fc).inferred_position_relative(1,:,1)))==1
                count=count+1
                pos_matp_mother=-cycle.(fc).inferred_position_relative(1,:,1)*cycle.pole;
                pos_ori_mother=-cycle.foci2.inferred_position_relative(1,:,1)*cycle.pole;
                matp_ori_diff_mother=pos_matp_mother(~isnan(pos_matp_mother))-pos_ori_mother(~isnan(pos_ori_mother));
                if all(sign(matp_ori_diff_mother)==1)
                    mother_orientation(i)=1;
                elseif all(sign(matp_ori_diff_mother)==-1)
                    mother_orientation(i)=2;
                end
                
                if ~isnan(mother_orientation(i))
                    
                    
                    pos_matp_child1=-child1.(fc).inferred_position_relative(1,:,1)*child1.pole;
                    pos_ori_child1=-child1.foci2.inferred_position_relative(1,:,1)*child1.pole;
                    matp_ori_diff_child1=pos_matp_child1(~isnan(pos_matp_child1))-pos_ori_child1(~isnan(pos_ori_child1));
                    
                    pos_matp_child2=-child2.(fc).inferred_position_relative(1,:,1)*child2.pole;
                    pos_ori_child2=-child2.foci2.inferred_position_relative(1,:,1)*child2.pole;
                    matp_ori_diff_child2=pos_matp_child2(~isnan(pos_matp_child2))-pos_ori_child2(~isnan(pos_ori_child2));

                    
                    if all(sign(matp_ori_diff_child1)==1) && all(sign(matp_ori_diff_child2)==1)
                        daughters_orientation(i)=1;
                    elseif  all(sign(matp_ori_diff_child1)==-1) && all(sign(matp_ori_diff_child2)==-1)
                        daughters_orientation(i)=2;
                    elseif  all(sign(matp_ori_diff_child1)==1) && all(sign(matp_ori_diff_child2)==-1)...
                            ||  all(sign(matp_ori_diff_child1)==-1) && all(sign(matp_ori_diff_child2)==1)
                        daughters_orientation(i)=3;
                    else
                        mother_orientation(i)=nan;
                    end
                end
                
            end
            
        end
        
    end
    
end
end