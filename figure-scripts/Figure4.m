%% Run startrack before you run the commands



%% Complete cycle ids
complete_cycle_ids = get_complete_cycle_ids(cycle_list);

%% Separate complete cycle ids into mother cells and other cycle ids
idx_complete=ismember(1:numel(cycle_list),complete_cycle_ids); % idx is logical indices

mothers=get_mother_cycle_ids(channel_list,cycle_list);
idx_mothers=ismember(1:numel(cycle_list),mothers); % idx is logical indices
idx=idx_complete & ~idx_mothers; %complete but not mothers
cycle_ids=1:numel(cycle_list);
other_cycle_ids=cycle_ids(idx);

%% MatP synched to MatP relocalization

synched_kymograph2(cycle_list,other_cycle_ids,1,@get_MatP_stable_at_middle,1,2.4,3);
hold on
xline(0,'--r')
hold off
set(gca,'Xtick',-100:25:100)
set(gca,'XtickLabel',-100:25:100)
colormap(viridis(500))

%% Ori synched to MatP relocalizaton

synched_kymograph2(cycle_list,other_cycle_ids,2,@get_MatP_stable_at_middle,1,2.4,3);
hold on
xline(0,'--r')
hold off
set(gca,'Xtick',-100:25:100)
set(gca,'XtickLabel',-100:25:100)
colormap(viridis(500))

%% Ori velocity synched to matp relocalization separated by poles

% velocity towards the nearest pole
forward_velocity_oldvsnewpole(cycle_list,other_cycle_ids',2,@get_MatP_stable_at_middle,1,2.4,3);
xline(0,'--')
yline(0,'--')
%% MatP velocity synched to matp relocalization

% Velocity towards old pole
forward_velocity_matp(cycle_list,other_cycle_ids',1,@get_MatP_stable_at_middle,1,2.4,3);
xline(0,'--')
yline(0,'--')

%% Difference between MatP relocalization index and maximum MatP foci velocity index

[f,t_diff]=forward_velocity_matp_reloc(cycle_list,other_cycle_ids',1,@get_MatP_stable_at_middle,1,2.4,3);
figure
histogram(-t_diff*5,-44.5*5:1*5:40.5*5,'Normalization','probability')
xlabel("Time between V_{max}ter and ter centralisation (min)")
ylabel("Probability")
title("Difference between MatP middle frame and MatP max velocity frame")
xlim([-100 100])

%% Functions

function [focivelocity,v_max_I]=forward_velocity_matp_reloc(varargin)
cycle_list=varargin{1};
cycle_ids=varargin{2};
fc=['foci',num2str(varargin{3})];
synch_time_fn=varargin{4};
args_for_synch_func=varargin(5:nargin);


f0=60;%put the frame of the event in the this bin= max no. of frames shown before the event
f1=61;%max no. of frames to show after the event, total number of frames shown is f0+f1
fref=0.5; %assign the frame of the event

k=1;
for i=cycle_ids'
    if isfield(cycle_list(i).(['foci',num2str(varargin{3})]),'inferred_position_relative') &&...
            ~isempty(cycle_list(i).(['foci',num2str(varargin{3})]).inferred_position_relative)
        %for j=1:size(cycle_list(i).(fc).inferred_position_relative(1,:,1),2)
        k=k+1;
        % end
    end
end
focivelocity=nan(k,f0+f1);
v_max_I=nan(1,k);
%matp_middle
k=1;
I=synch_time_fn(cycle_list,cycle_ids,args_for_synch_func{:});


for i=1:numel(cycle_ids)
    if isfield(cycle_list(cycle_ids(i)).(['foci',num2str(varargin{3})]),'inferred_position_relative') &&...
            ~isempty(cycle_list(cycle_ids(i)).(['foci',num2str(varargin{3})]).inferred_position_relative)
        i
        if ~isnan(I(i)) && ~isempty(I(i)) && I(i)>0
            cycle=cycle_list(cycle_ids(i));
            cycle.cycle_id
            %for j=1:size(cycle.(fc).inferred_position_relative(1,:,1),2)%max number of foci
            if size(cycle.(fc).start_and_stop,1)<3 % Consider only cells that has one or two foci tracks
                pos=nan(size(cycle.(fc).inferred_position_relative(:,1,1)));
                if size(cycle.(fc).start_and_stop,1)==2 && cycle.(fc).start_and_stop(1)~=cycle.(fc).start_and_stop(2) % Exclude cells that has two tracks both starting from frame one - reason: unlikely event
                    pos(1:cycle.(fc).start_and_stop(2,1))=cycle.(fc).inferred_position_relative(1:cycle.(fc).start_and_stop(2,1),1,1)*cycle.pole; % Velocity towards oldpole
                elseif size(cycle.(fc).start_and_stop,1)==1
                    pos=cycle.(fc).inferred_position_relative(:,1,1)*cycle.pole;
                end
                
                growth=cycle.lengths(2:end)./cycle.lengths(1:end-1);%change in length
                
                v_corr=pos(2:end)-pos(1:end-1).*growth; %corrected forward velocity
                if ~isnan((max(v_corr)))
                v_max_I(k)=I(i)-find(max(v_corr)==v_corr,1);
                end
                %v_corr=-v_corr.*sign(cycle.(fc).position_relative(1:end-1,j,1));
                focivelocity(k,(f0-I(i)+1):(f0-1))=(v_corr(1:I(i)-1))';%velocity on frames before the event
                focivelocity(k,f0:(f0+(min(f1,length(v_corr))-I(i))))=(v_corr(I(i):min(f1,length(v_corr))))';%% corrected to take only a maximum of 61 frames after sync time in case of longer cell cycles
                
            end
        end
        k=k+1;
    end
end

focivelocity=focivelocity./5;

std=nanstd(focivelocity);

sem=nanstd(focivelocity)./sqrt(sum(~isnan(focivelocity)));
% figure
% shadederror_std_sem((fref+[(1-f0):1:(f1)])*5,nanmean(focivelocity),sem,"#0072BD")
xlim([-100 100])
ax=gca; ax.YAxis.Exponent = -2;
ylabel("Long axis velocity(\mu m/min)")
xlabel("Time (min)")

end

function shadederror_std_sem(x,d,sem,color)

xx=[x,fliplr(x)];
%ss1=[d+std ,fliplr(d-std)];
ss2=[d+sem,fliplr(d-sem)];
xx=xx(~isnan(ss2));
%ss1=ss1(~isnan(ss1));
ss2=ss2(~isnan(ss2));
hold on
h=plot(x,d,'.-','Color',color);
color=get(h,'Color');
%patch(xx, ss1, color,'linestyle', 'none','FaceColor',color,'FaceAlpha','0.3');
patch(xx,ss2,color,'linestyle', 'none','FaceColor',color,'FaceAlpha','0.5');
hold off
box on;
end