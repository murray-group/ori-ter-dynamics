%% Complete cycle ids
complete_cycle_ids = get_complete_cycle_ids(cycle_list);

%% Separate complete cycle ids into mother cells and other cycle ids
idx_complete=ismember(1:numel(cycle_list),complete_cycle_ids); % idx is logical indices

mothers=get_mother_cycle_ids(channel_list,cycle_list);
idx_mothers=ismember(1:numel(cycle_list),mothers); % idx is logical indices
idx=idx_complete & ~idx_mothers; %complete but not mothers
cycle_ids=1:numel(cycle_list);
other_cycle_ids=cycle_ids(idx);

%% Kymograph of ori foci positions - cell age

%Ori
foci_kymograph(cycle_list,other_cycle_ids,100,24,2,-1);
colormap(viridis(500))
%%
%MatP
foci_kymograph(cycle_list,other_cycle_ids,100,24,1,0);
colormap(viridis(500))

%% Paired difference - frames/minutes
a=get_ori_split(cycle_list,other_cycle_ids,2,4,9);
b=get_MatP_stable_at_middle(cycle_list,other_cycle_ids,1,2.4,3);
% The above functions return frames where the condition is true for the
% first time.
% -1 - Second last frame of the parent cell cycle
% 0 - Last frame of parent cell cycle
% 1 - First frame of cycle
% This means, when the frame is converted into time, a subtraction of 1 frame has to be carried out
% because, lets say the condition is true in the first frame of the cell
% cycle, then the value returned would be 1, which when converted into
% times becomes 5 minutes, but in reality it should be 0 because only then
% it makes sense to say the cell was first detected satisfying the
% condition upon the birth of a cell
a=a-1; b=b-1; % Subtraction of one frame before conversion into time/minutes
paired_difference(a*5,...
    b*5,[-50,400],[0,0.99],{'T_{ori}','T_{MatP}','T_{MatP}-T_{ori}'},'Time (min)')

%% Paired difference - cell lengths (SI)
a=get_ori_split_length(cycle_list,other_cycle_ids,2,4,9);
b=get_MatP_stable_at_middle_length(cycle_list,other_cycle_ids,1,2.4,3);
% The above functions return frames where the condition is true for the
% first time.
a=a*.067;
b=b*.067;
paired_difference(a,...
    b,[0,4],[0,0.99],{'L_{ori}','L_{MatP}','L_{MatP}-L_{ori}'},'Length(um)')

%% Demographs (SI)
%ori

foci_demograph(cycle_list,other_cycle_ids',2,0)
ylim([1.3 4])
%% MatP

foci_demograph(cycle_list,other_cycle_ids',1,0)
ylim([1.3 4])
%% Foci positions of MatP inside cell (SI)

all_foci_pos=all_foci_positions(cycle_list,other_cycle_ids,1,[37 45]);

figure
[f,xi] = ksdensity(all_foci_pos);
area(xi,f);
%histogram(all_foci_pos,'Normalization','pdf')
xlabel("Position relative to midcell (pixels)")
ylabel("Density")
title("MatP foci positions (WT)")
xlim([-15 15])
hold on
xline(-2.7,':')
xline(2.7,':')
hold off
%% Growth rates of cell cycles (SI)

growth_rates=growth_rate(cycle_list,other_cycle_ids);
%%
figure
hist_to_line(growth_rates./5)
%histogram(growth_rates./5);
xlabel("Growth rate (min^-1)")
ylabel("Count")
xlim([0 .01])
ax=gca; ax.XAxis.Exponent = -3;
%% Cell cycle durations (SI)

durations = zeros(1,numel(other_cycle_ids));
birth_lengths = zeros(1,numel(other_cycle_ids));
division_lengths = zeros(1,numel(other_cycle_ids));
for i = 1:numel(other_cycle_ids)
    cycle_id = other_cycle_ids(i);
    cycle = cycle_list(cycle_id);
    durations(i) = cycle.duration;
    birth_lengths(i) = cycle.lengths(1);
    division_lengths(i) = cycle.lengths(end);
end

figure
hist_to_line(durations*5);
% histogram(durations*5);
xlabel ("Cell cycle durations (min)")
ylabel ("Count")

figure
[~,N]=histcounts(birth_lengths);
[f,xi] = ksdensity(birth_lengths*.067,'NumPoints',numel(N)-1);
area(xi,f);
%histogram(birth_lengths*.067,28,'Normalization','pdf')
hold on
[~,N]=histcounts(division_lengths);
[f,xi] = ksdensity(division_lengths*.067,'NumPoints',numel(N)-1);
area(xi,f);
% histogram(division_lengths*.067,45,'Normalization','pdf')
xlabel("Cell length")
ylabel("PDF")
legend('Birth','Division')

%% Synch kymograph (SI)

%MatP relative to ori duplication
synched_kymograph2(cycle_list,other_cycle_ids,1,@get_ori_split,2,4,9);

%% Ori relative to ori duplication
synched_kymograph2(cycle_list,other_cycle_ids,2,@get_ori_split,2,4,9);
hold on
xline(0,'--r')
hold off
set(gca,'Xtick',-100:25:100)
set(gca,'XtickLabel',-100:25:100)
colormap(viridis(500))
colorbar


%% Alluvial Plot (SI)

% Run startrack with following parameters before running this: 

% D = 2e-4/0.0668^2*60*5;%diffusion in pixel^2 per minute
% false_negative = 0.1;
% false_positive = 0.005;
% frame_skips = 3;%number of maximum frame skips
% cmax = 0.0001;%this is the probability at which a connection is deemed to improbable to be true and wont be considred further
% thresh_possible_connections = false_negative^(frame_skips+1);
% thresh_possible_connections = max(cmax,thresh_possible_connections);
% ------------------------------------------------------------------------
%%
[ori_foci_at_birth,ori_foci_at_division]=ori_foci_nos_startrack(cycle_list,other_cycle_ids,2);

CreateSankeyPlot([ori_foci_at_birth',ori_foci_at_division'],[],{'birth','division'},2) 

% The percentage that the plot gives out is wrong. It didn't exclude the
% nan values when calculating the total number of elements. Edited in
% actual value in Illustrator
%%
function hist_to_line(data)

% Plot the histogram
% h=histogram(data,'Normalization', 'count'); % plot the histogram with count normalization
% hold on % hold on to the current plot so we can add to it
% Convert the histogram to an area plot
[counts, centers] = histcounts(data); % get the bin counts and centers
width = (centers(1:end-1) + centers(2:end))/2; % calculate the width of each bin
area(width, smooth(counts)) % plot the area plot

% Add labels and legend
% xlabel('Data')
% ylabel('Count')
% legend('Histogram', 'Area Plot')
end

function [mother_cycle_ids] = get_mother_cycle_ids(channel_list,cycle_list)
    mother_cycle_ids = [];
    for channel_it = 1:numel(channel_list)
        mother_cycle_ids_tmp = [];
        channel = channel_list(channel_it);
        for frame_it = 1:channel.n_frames
            max_level = 0;
            max_id = nan;
            for cycle_it = 1:size(channel.cycle_ids,2)
                cycle_id = channel.cycle_ids(frame_it,cycle_it);
                if ~isnan(cycle_id)
                    cycle = cycle_list(cycle_id);
                    bb = cycle.bbs(channel.cycle_indices(frame_it,cycle_it),:,:);
                    if bb(1) > max_level
                        max_level = bb(1);
                        max_id = cycle_id;
                    end
                end
            end
            if ~isnan(max_id) && cycle_list(max_id).flags.complete && ~ismember(max_id, mother_cycle_ids_tmp)
                mother_cycle_ids_tmp(numel(mother_cycle_ids_tmp)+1) = max_id;
            end
        end
        mother_cycle_ids = cat(2,mother_cycle_ids,mother_cycle_ids_tmp);
    end
end
