%% Plotting violins
a_trimmed= growth_rate_ori_mtq;%(growth_rate_ori_cfp_all>=prctile(growth_rate_ori_cfp_all,0) & growth_rate_ori_cfp_all<=prctile(growth_rate_ori_cfp_all,99.99));
b_trimmed= growth_rate_ori_mtq_hu;%(growth_rate_ori_cfp_matp_pole>=prctile(growth_rate_ori_cfp_matp_pole,0) & growth_rate_ori_cfp_matp_pole<=prctile(growth_rate_ori_cfp_matp_pole,99.99));
c_trimmed= growth_rate_ori_mtq_zapB;%(growth_rate_ori_cfp_matp_mid>=prctile(growth_rate_ori_cfp_matp_mid,0) & growth_rate_ori_cfp_matp_mid<=prctile(growth_rate_ori_cfp_matp_mid,99.99));
d_trimmed= growth_rate_ori_mtq_c20;
e_trimmed= growth_rate_ori_cfp_all;
a=nan(1,numel(growth_rate_ori_mtq));
b=nan(1,numel(growth_rate_ori_mtq));
c=nan(1,numel(growth_rate_ori_mtq));
d=nan(1,numel(growth_rate_ori_mtq));
e=nan(1,numel(growth_rate_ori_mtq));

a(1:length(a_trimmed))=a_trimmed;
b(1:length(b_trimmed))=b_trimmed;
c(1:length(c_trimmed))=c_trimmed;
d(1:length(d_trimmed))=d_trimmed;
e(1:length(e_trimmed))=e_trimmed;

d=[a' b' c' d' e'];
figure
violinplot(d./5,{'IS 131','IS 130','IS XXX', 'IS XXX', 'IS XXX'},'ShowData',false);
%ylim([0 0.015])