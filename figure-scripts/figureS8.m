%% get complete cell cycle ids
complete_cycle_ids = get_complete_cycle_ids(cycle_list);

%% Separate complete cycle ids into mother cells and other cycle ids
idx_complete=ismember(1:numel(cycle_list),complete_cycle_ids); % idx is logical indices

mothers=get_mother_cycle_ids(channel_list,cycle_list);
idx_mothers=ismember(1:numel(cycle_list),mothers); % idx is logical indices
idx=idx_complete & ~idx_mothers; %complete but not mothers
cycle_ids=1:numel(cycle_list);
other_cycle_ids=cycle_ids(idx);


%% Growth rates of cell cycles

growth_rates=growth_rate(cycle_list,complete_cycle_ids);
%%
figure
hist_to_line(growth_rates./5,1)
%histogram(growth_rates./5);
xlabel("Growth rate (min^-1)")
ylabel("Count")
xlim([0 .01])
ax=gca; ax.XAxis.Exponent = -3;

%% Cell age kymograph of MatP for no ori label dataset

% population with MatP at mid-cell during division

foci_kymograph(cycle_list,complete_cycle_ids,100,32,1);
colormap(viridis(500))
title("MatP in del_mukB - No ori label")

%% Cell age kymograph of ori and MatP for ori label dataset

%ori

foci_kymograph(cycle_list,[complete_cycle_ids(~ismember(complete_cycle_ids,unique(anucleate)))],100,30,2);
colormap(viridis(500))

%matp

foci_kymograph(cycle_list,[complete_cycle_ids(~ismember(complete_cycle_ids,unique(anucleate)))],100,30,1);
colormap(viridis(500))


