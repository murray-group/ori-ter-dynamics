function [matp_mid,matp_pole,matp_mixd,matp_rest,ori_ter_orientation]=sort_matp_localization_at_division_with_birth_orientation(cycle_list,complete_cycle_ids,fluor_channel)
matp_mid=[];
matp_pole=[];
matp_mixd=[];
matp_rest=[];
ori_ter_orientation=nan(numel(complete_cycle_ids),2);
count=0;
fc=strcat('foci',num2str(fluor_channel));
for i=1:numel(complete_cycle_ids)
    cycle=cycle_list(complete_cycle_ids(i));
    cycle.cycle_id
    if isfield(cycle.(fc),'inferred_position_relative') && ~isempty(cycle.(fc).inferred_position_relative) && isfield(cycle.foci2,'inferred_position_relative') && ~isempty(cycle.foci2.inferred_position_relative)
        pos=abs(cycle.(fc).inferred_position_relative(end,:,1));
        pos_start_ori=-cycle.foci2.inferred_position_relative(1,:,1).*cycle.pole;
        pos_start_matp=-cycle.(fc).inferred_position_relative(1,:,1).*cycle.pole;
        if sum(~isnan(cycle.(fc).inferred_position_relative(1,:,1)))==1 && sum(~isnan(cycle.foci2.inferred_position_relative(1,:,1)))==1
        if any(~isnan(pos))
            count=count+1;
            if sign(pos_start_matp(~isnan(pos_start_matp))-pos_start_ori(~isnan(pos_start_ori)))==1
                ori_ter_orientation(i,1)=1;
            elseif sign(pos_start_matp(~isnan(pos_start_matp))-pos_start_ori(~isnan(pos_start_ori)))==-1
                ori_ter_orientation(i,1)=2;
            end
            if size(cycle.(fc).start_and_stop,1)==2 & sum(~isnan(pos))==2 & pos(~isnan(pos))>=cycle.lengths(end)/6
                matp_pole=[matp_pole cycle.cycle_id];
                ori_ter_orientation(i,2)=2;
            elseif pos(~isnan(pos))<cycle.lengths(end)/6
                matp_mid=[matp_mid cycle.cycle_id];
                ori_ter_orientation(i,2)=1;
            elseif size(cycle.(fc).start_and_stop,1)==2 & sum(~isnan(pos))==2 & any(pos(~isnan(pos))>=cycle.lengths(end)/6) & any(pos(~isnan(pos))<cycle.lengths(end)/6)
                matp_mixd=[matp_mixd cycle.cycle_id];
                ori_ter_orientation(i,2)=3;
            else
                matp_rest=[matp_rest cycle.cycle_id];
            end
        end
        end
    end
end
count

disp("Population with MatP at pole")
numel(matp_pole)/count
disp("Population with MatP at mid-cell")
numel(matp_mid)/count
disp("Population with MatP at mid-cell and pole")
numel(matp_mixd)/count
end