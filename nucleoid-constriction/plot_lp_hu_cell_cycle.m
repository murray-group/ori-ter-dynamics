%% Find the valleys (nucleoid constrictions) using Sean's script (modified)
function plot_lp_hu_cell_cycle(cycle,fluor_channel,threshold)
peaks_array_single=[];
modulation=[];
fc=['fluor',num2str(fluor_channel),'_lp'];
% cycle_num=complete_cycle_ids(m(2));
% k=cycle_list(cycle_num).duration;
k=cycle.duration;
color=flipud((parula(k)));

constriction=cell(1,cycle.duration);
line_profiles=cycle.(fc);
for frame=1:cycle.duration
    ys=((line_profiles{1,frame})./max(line_profiles{1,frame}));
    xdata=linspace(0,1,numel(line_profiles{1,frame}));
    constriction{1,frame}=find_valleys(ys,xdata,threshold);
end

j=size(constriction,2);
while ~isempty(constriction{1,j})
    j=j-1;
    if j==0
        break;
    end
end

disp(['Constriction frame = ' num2str(j+1)]);
disp(['Cycle duration = ' num2str(cycle.duration)]);

frames=j-2:1:min(j+4,cycle.duration)
% if ~any(frames==j+1)
%     frames=2:2:cycle.duration;
% end
alpha=(linspace(0,1,numel(frames)));
k=1;
for frame=frames
    %frame
    ys=(((cycle.(fc){1,frame})./max(cycle.(fc){1,frame})));
    xdata=linspace(-0.5,0.5,numel(cycle.(fc){1,frame}));
    [ht,loc,wd,dp]=find_valleys(ys,xdata,threshold);
    
    
    peaks_array_single{1}{frame}=[ht,loc,wd,dp];
    %close figure(6)
    figure (6)
    hold on
    %subplot(6,6,frame);
        if frame==j+1
             plot(xdata,ys,'LineWidth',2,'Color',[0 0 0]);
        plot(loc,ht,'or','LineWidth',3)
        yline(ht,'--k')
        yline(ht+dp,'--k')
        yline(ht+dp-0.12898*(max(ys)-min(ys)),'-k')
        (ht+dp)-0.12898*(max(ys)-min(ys))
       
    %     elseif frame<j+1
    %     plot(xdata,ys,'LineWidth',1.3,'Color',[[1 0 0.5] alpha(frame).*4] );
    %     plot(loc,ht,'o','Color',[0 0.6 0],'LineWidth',1.3)
        else
    plot(xdata,ys,'LineWidth',1.3,'Color',[[0 0.4470 0.7410] alpha(k)/2]);
    
        %plot(loc,ht,'ok','LineWidth',1.3)
        end
    hold off
    xlabel("Relative long axis position")
    ylabel("Normalized intensity")
    k=k+1;
end
%         pause(5)
%        close figure 5
ylim([0 1.05])
end
%%

function [height,loc,w,depth]=find_valleys(ys,x,threshold)

valleys=watershed(ys);%breaks the profile up into valleys
%

[height,loc,left,right,depth]=measure_valleys(valleys,ys);


%
%find the deepest valley


while length(depth)>1
    [min_d,i]=min(depth);
    d=min_d+0.0001;
    
    
    if((ys(left(i))-height(i))<d)%first check if left side shallow
        valleys(left(i))=valleys(left(i)-1);
        temp=valleys(left(i)+1:end);
        temp(temp>0)=temp(temp>0)-1;
        valleys(left(i)+1:end)=temp;
        
    else %if not, check if right side is shallow
        valleys(right(i))=valleys(right(i)-1);
        temp=valleys(right(i)+1:end);
        temp(temp>0)=temp(temp>0)-1;
        valleys(right(i)+1:end)=temp;
    end
    [height,loc,left,right,depth]=measure_valleys(valleys,ys);
end

w=right-left;

if ~isempty(x)
    loc=x(loc);
    w=x(right)-x(left);
end

if (depth/(max(ys)-min(ys)))<threshold
    height=[];
    loc=[];
    w=[];
    depth=[];
end

end



function [height,loc,left,right,depth]=measure_valleys(valleys,ys)
n_valleys=max(valleys);
if n_valleys<=2
    height=[];
    loc=[];
    left=[];
    right=[];
    depth=[];
    return;
end
for i=2:(n_valleys-1) %ignore valleys at the boundary
    [height(i-1),I]=min(ys(valleys==i));
    loc(i-1)=I+find(valleys==i,1)-1;%index of minimum
    left(i-1)=find(valleys==i,1)-1;%index of left edge
    right(i-1)=find(valleys==i,1,'last')+1;%index of right edge
    depth(i-1)=min(ys(left(i-1)),ys(right(i-1)))-height(i-1);%depth
end


end