function data_cell=find_depth_threshold(cycle_list,complete_cycle_ids,x_bins,fluor_channel)

med_duration=x_bins; % Median duration of all cell cycles
%fluor_channel=1; % Fluorescence channel number which have the line profiles of Hu-mCh 
fc=['fluor',num2str(fluor_channel),'_lp'];
modulation=cell(1,size(complete_cycle_ids,2));
k=1;
fieldsToKeep = {'cycle_id', 'duration','pole',fc};
cycle_list=rmfield(cycle_list, setdiff(fieldnames(cycle_list), fieldsToKeep)); % To reduce ram usage when cycle_list is broadcasted in a parfor loop

parfor i=1:numel(complete_cycle_ids)
    i
    cycle=cycle_list(complete_cycle_ids(i));
    for frame=1:cycle.duration
        ys=(((cycle.(fc){1,frame})./max(cycle.(fc){1,frame})));
        xdata=linspace(0,1,numel(cycle.(fc){1,frame}));
        [~,~,~,dp]=find_valleys(ys,xdata,0);

        if ~isempty(dp)
            modulation{i}(frame)=dp./(max(ys)-min(ys)); % Find the nucleoid constriction depth normalized to cell signal
        else
            modulation{i}(frame)=0;
        end

        k=k+1; % Counter to later use for initializing matrix to determine standard deviation
    end
end
%% Binning the constriction depth according to cell age
bin_edges=linspace(-0.01,1.01,med_duration+1);
data_cell=nan(k,med_duration);
data=zeros(1,med_duration);
n_data=zeros(1,med_duration);
k=1;
for i=1:numel(complete_cycle_ids)% Loop to query the entire complete cell cycles
    cycle=cycle_list(complete_cycle_ids(i));
    [~,~,bins]=histcounts(linspace(0,1,cycle.duration),bin_edges);
    for j=1:length(modulation{i})
        data(1,bins(j))=data(1,bins(j))+modulation{i}(j);
        data_cell(k,bins(j))=modulation{i}(j);
        n_data(bins(j))=n_data(bins(j))+1;
        k=k+1;
    end
end

data=data./n_data; % Mean
std_data=nanstd(data_cell); % Standard deviation
sem_data=std_data./sqrt(n_data); % Standard error of the mean

%% Plotting
figure
shadederror_std_sem(linspace(0,1,med_duration),data,std_data,sem_data)
ylabel("Normalized nucleoid constriction depth")
xlabel("Relative cell age")
hold on
yline(prctile(data_cell(:,1),95),'--')
hold off
thr=['Threshold (',num2str(prctile(data_cell(:,1),95)),')'];
legend('Depth','SD','SEM',thr)
end


%% Functions

function [height,loc,w,depth]=find_valleys(ys,x,depth_thresholding)

valleys=watershed(ys);%breaks the profile up into valleys
%

[height,loc,left,right,depth]=measure_valleys(valleys,ys);


%
%find the deepest valley


while length(depth)>1
    [min_d,i]=min(depth);
    d=min_d+0.000001;
    
    
    if((ys(left(i))-height(i))<d)%first check if left side shallow
        valleys(left(i))=valleys(left(i)-1);
        temp=valleys(left(i)+1:end);
        temp(temp>0)=temp(temp>0)-1;
        valleys(left(i)+1:end)=temp;
        
    else %if not, check if right side is shallow
        valleys(right(i))=valleys(right(i)-1);
        temp=valleys(right(i)+1:end);
        temp(temp>0)=temp(temp>0)-1;
        valleys(right(i)+1:end)=temp;
    end
    [height,loc,left,right,depth]=measure_valleys(valleys,ys);
end

w=right-left;

if ~isempty(x)
    loc=x(loc);
    w=x(right)-x(left);
end

% Define a depth threshold

if (depth_thresholding ~= 0)
    if depth/(max(ys)-min(ys))<depth_thresholding
        height=[];
        loc=[];
        w=[];
        depth=[];
    end
end
end



function [height,loc,left,right,depth]=measure_valleys(valleys,ys)
n_valleys=max(valleys);
if n_valleys<=2
    height=[];
    loc=[];
    left=[];
    right=[];
    depth=[];
    return;
end
for i=2:(n_valleys-1) %ignore valleys at the boundary
    [height(i-1),I]=min(ys(valleys==i));
    loc(i-1)=I+find(valleys==i,1)-1;%index of minimum
    left(i-1)=find(valleys==i,1)-1;%index of left edge
    right(i-1)=find(valleys==i,1,'last')+1;%index of right edge
    depth(i-1)=min(ys(left(i-1)),ys(right(i-1)))-height(i-1);%depth
end


end