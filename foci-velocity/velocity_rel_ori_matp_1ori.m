%function velocity_rel_ori_matp(cycle_list,cycle_ids,fluor_channel_matP,fluor_channel_ori,fr)
fluor_channel_matp=1;
fluor_channel_ori=2;
fr=1;
cycle_ids=complete_cycle_ids';
fc1=strcat("foci",string(fluor_channel_matp)); %MatP foci
fc2=strcat("foci",string(fluor_channel_ori)); %Ori foci

edges=linspace(-40,40,80); % bin edges to sort ori velocity based on ori position relative to MatP position
middles=(edges(1:end-1)+edges(2:end))/2;

ori_rel_matp=NaN(30000,1);
v_rel=NaN(30000,1); % Vector that would contain ori velocities w.r.t ori position relative to MatP
cnt=0;
v_mean=[];
v_std=[];
v_sem=[];
matp_pos_all=[];
for i=cycle_ids'
    if isfield(cycle_list(i).(fc1),'position_relative') && isfield(cycle_list(i).(fc2),'position_relative') &&...
            ~isempty(cycle_list(i).(fc1).position_relative) && ~isempty(cycle_list(i).(fc2).position_relative)
        matp_foci_counts=sum(~isnan(cycle_list(i).(fc1).position_relative(:,:,1)),2); % counts number of MatP foci in each frame
        I=find(matp_foci_counts>1,1); % Find the first time in the cell cycle when more than one MatP foci was found
        
        if ~isempty(I)
            I=I-1; % Index of frame until two MatP foci appear in a cell cycle
        else
            I=cycle_list(i).duration; % when there was only one MatP foci throughout the cell cycle
        end
        
        temp1=cycle_list(i).(fc1).position_relative(1:I,:,1)*cycle_list(i).pole;
        temp1(isnan(temp1))=0;
        temp1=sum(temp1,2);
        temp1(temp1==0)=nan;
        matp_pos=temp1; % vector with MatP focus positions that will be used to calculate relative distance of Ori later
        ori_foci_counts=sum(~isnan(cycle_list(i).(fc2).position_relative(:,:,1)),2); % counts number of Ori foci in each frame
        I1=find(ori_foci_counts==2,1); % Find the first frame in the cell cycle before with more than one ori
        if I1>find(ori_foci_counts>2,1) % If 3 foci appear before 2 foci, skip the cell cycle
            continue;
        else
            if ~isempty(I1)
                I1=I1-1; % Index of frame until two Ori foci appear in a cell cycle
            else
                I1=cycle_list(i).duration; % when there was only one Ori foci throughout the cell cycle
            end
            if I>I1
                I=I1; % If 2 Ori foci occurs before seeing 2 MatP foci (which mostly does), then query only until 2 Ori occur
            end
            if I>3
                v=((cycle_list(i).(fc2).position_relative(fr+1:I,:,1)-cycle_list(i).(fc2).position_relative(1:I-fr,:,1))*cycle_list(i).pole); % velocity of Ori by frame-shift
                x=cycle_list(i).(fc2).position_relative(1:I-fr,:,1)*cycle_list(i).pole-matp_pos(1:I-fr); % ori positions relative to MatP, % matrix-column, row-wise subtraction
                m=matp_pos(1:I-fr);
                v=v(:);
                x=x(:);
                I=~isnan(x) & ~isnan(v);
                x=x(I);
                v=v(I);
                
                v_ori(cnt+1:cnt+numel(v))=v;
                ori_rel_matp(cnt+1:cnt+numel(v))=x;
                matp_pos_all=[matp_pos_all;m];
                cnt=cnt+numel(v);
            end
        end
    end
end

[~,~,bins]=histcounts(ori_rel_matp,edges); % Binning ori positions relative to matP

for k=1:length(edges)-1
    v_mean(k)=nanmean(v_ori(bins==k));
    v_std(k)=nanstd(v_ori(bins==k));
    v_sem(k)=v_std(k)./sqrt(numel(v_ori(bins==k)));
end

figure
shadederror_sem(middles*0.067,v_mean*0.067./fr/5,(v_mean*0.067./fr/5)+(v_std*0.067./fr/5),(v_mean*0.067./fr/5)-(v_std*0.067./fr/5),v_sem*0.067/fr/5,"Foci distances from frame of splitting");
%plot(middles*0.067,v_mean*0.067./fr)
hold on
xline(0)
yline(0)
hold off
xlim([-1.5 1.5])

xlabel("Ori position relative to MatP (um)")
ylabel("Ori speed (um/min)")
title("Ori speed - 1 Ori")
ylim([-0.1 0.1])
figure
histogram(ori_rel_matp*0.067)
xlim([-1.5 1.5])
xlabel("Ori positions relative to MatP (um)")
ylabel("Count")
title("Ori positions relative to MatP - 1 Ori")
%end

