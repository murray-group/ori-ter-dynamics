%% To find ori foci velocity towards poles synched to an event
% arg 1 : cycle_list
% arg 2 : cycle_ids
% arg 3 : fluor_channel
% arg 4 : synchronizing function
% arg 5 until the last arg : arguments for sync function

function [forwardvelocity1,forwardvelocity2]=forward_velocity_oldvsnewpole(varargin)
cycle_list=varargin{1};
cycle_ids=varargin{2};
fc=['foci',num2str(varargin{3})];
synch_time_fn=varargin{4};
args_for_synch_func=varargin(5:nargin);


f0=60;%put the frame of the event in the this bin= max no. of frames shown before the event
f1=61;%max no. of frames to show after the event, total number of frames shown is f0+f1
fref=0.5; %assign the frame of the event

k=1;
for i=cycle_ids'
    
    if isfield(cycle_list(i).(['foci',num2str(varargin{3})]),'inferred_position_relative') &&...
            ~isempty(cycle_list(i).(['foci',num2str(varargin{3})]).inferred_position_relative)
        for j=1:size(cycle_list(i).(fc).inferred_position_relative(1,:,1),2)
            k=k+1;
        end
    end
end
forwardvelocity1=nan(k,f0+f1);
forwardvelocity2=nan(k,f0+f1);

k=1;
I=synch_time_fn(cycle_list,cycle_ids,args_for_synch_func{:});


for i=1:numel(cycle_ids)
    if isfield(cycle_list(cycle_ids(i)).(['foci',num2str(varargin{3})]),'inferred_position_relative') &&...
            ~isempty(cycle_list(cycle_ids(i)).(['foci',num2str(varargin{3})]).inferred_position_relative)
       
        if ~isnan(I(i)) && ~isempty(I(i)) && I(i)>0
          
            cycle=cycle_list(cycle_ids(i));
            
            if size(cycle.(fc).inferred_position_relative(1,:,1),2)==2
                pos=cycle.(fc).inferred_position_relative(:,:,1).*-cycle.pole; % positions in the new pole will have positive values to make it same as kymographs
                
                if cycle.(fc).start_and_stop(2,1)~=cycle.(fc).start_and_stop(1,1)
                    pos(1:cycle.(fc).start_and_stop(2,1)-1,2)=cycle.(fc).inferred_position_relative(1:cycle.(fc).start_and_stop(2,1)-1,1,1);...
                        % the inferred_position_relative matrix already has
                        % the same position from adjacent column for the frame
                        % before the split occurs
                end
                c=nanmean(pos);
                [~,index1] = max(c);
                [~,index2] = min(c);
                pos1 = pos(:,index1);
                pos2=pos(:,index2);
                
                growth=cycle.lengths(2:end)./cycle.lengths(1:end-1);%change in length
                
                v_corr1=pos1(2:end)-pos1(1:end-1).*growth; %corrected forward velocity
                %v_corr=-v_corr.*sign(cycle.(fc).inferred_position_relative(1:end-1,j,1));
                forwardvelocity1(k,(f0-I(i)+1):(f0-1))=(v_corr1(1:I(i)-1))';%velocity on frames before the event
                forwardvelocity1(k,f0:(f0+(min(f1,length(v_corr1))-I(i))))=(v_corr1(I(i):min(f1,length(v_corr1))))';%% corrected to take only a maximum of 61 frames after sync time in case of longer cell cycles
                
                v_corr2=pos2(2:end)-pos2(1:end-1).*growth; %corrected forward velocity
                %v_corr=-v_corr.*sign(cycle.(fc).inferred_position_relative(1:end-1,j,1));
                forwardvelocity2(k,(f0-I(i)+1):(f0-1))=(v_corr2(1:I(i)-1))';%velocity on frames before the event
                forwardvelocity2(k,f0:(f0+(min(f1,length(v_corr2))-I(i))))=(v_corr2(I(i):min(f1,length(v_corr2))))';%% corrected to take only a maximum of 61 frames after sync time in case of longer cell cycles
                
                k=k+1;
            end
        end
    end
end
forwardvelocity1=0.067.*forwardvelocity1./5;
forwardvelocity2=-0.067.*forwardvelocity2./5;

std1=nanstd(forwardvelocity1);
std2=nanstd(forwardvelocity2);
sem1=nanstd(forwardvelocity1)./sqrt(sum(~isnan(forwardvelocity1)));
sem2=nanstd(forwardvelocity2)./sqrt(sum(~isnan(forwardvelocity2)));
figure
shadederror_std_sem((fref+[(1-f0):1:(f1)])*5,nanmean(forwardvelocity1),sem1,[0    0.4470    0.7410])%blue; new pole
hold on
shadederror_std_sem((fref+[(1-f0):1:(f1)])*5,nanmean(forwardvelocity2),sem2,"#D95319" )%brown; old pole
hold off

xlim([-100 100])
legend('New pole','SEM','Old pole','SEM')
ax=gca; ax.YAxis.Exponent = -2;
ylabel("Long axis velocity(\mu m/min)")
xlabel("Time (min)")
end

function shadederror_std_sem(x,d,sem,color)

xx=[x,fliplr(x)];
%ss1=[d+std ,fliplr(d-std)];
ss2=[d+sem,fliplr(d-sem)];
xx=xx(~isnan(ss2));
%ss1=ss1(~isnan(ss1));
ss2=ss2(~isnan(ss2));
hold on
h=plot(x,d,'.-','Color',color,'LineWidth',1.3);
color=get(h,'Color');
%patch(xx, ss1, color,'linestyle', 'none','FaceColor',color,'FaceAlpha','0.3');
patch(xx,ss2,color,'linestyle', 'none','FaceColor',color,'FaceAlpha','0.5');
hold off
box on;
end
