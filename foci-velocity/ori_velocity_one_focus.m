%function ori_velocity(cycle_list,cycle_ids,fluor_channel,fn)

fc=strcat("foci",string(2));

edges=linspace(-40,40,80); % bin edges to sort ori velocity based on ori position relative to MatP position
middles=(edges(1:end-1)+edges(2:end))/2;

v_mean=[];
v_std=[];
v_sem=[];
cnt=0;

for i=one_ori_cycle_ids'
    if isfield(cycle_list(i).(fc),'position_relative') &&...
            ~isempty(cycle_list(i).(fc).position_relative)
        ori_foci_counts=sum(~isnan(cycle_list(i).(fc).position_relative(:,:,1)),2); % counts number of MatP foci in each frame
        I=find(ori_foci_counts==2,1); % Find the first frame in the cell cycle before with more than one ori
        if I>find(ori_foci_counts>2,1)
            continue;
        else
            if ~isempty(I)
                I=I-1; % Index of frame until two Ori foci appear in a cell cycle
            else
                I=cycle_list(i).duration; % when there was only one Ori foci throughout the cell cycle
            end
            temp1=cycle_list(i).(fc).position_relative(1:I,:,1)*cycle_list(i).pole;%*cycle_list(i).pole;
            temp1(isnan(temp1))=0;
            temp1=sum(temp1,2);
            temp1(temp1==0)=nan;
            x=temp1;
            
            v=(diff(x)); % velocity of Ori
            v=v(:);
            x=x(1:end-1);
            I=~isnan(x) & ~isnan(v);
            x=x(I);
            v=v(I);
            v_ori(cnt+1:cnt+numel(v))=v;
            ori_pos(cnt+1:cnt+numel(v))=x;
            cnt=cnt+numel(v);
        end
    end
end

[~,~,bins]=histcounts(ori_pos,edges); % Binning ori positions relative to matP

for k=1:length(edges)-1
    v_mean(k)=nanmean(v_ori(bins==k));
    v_std(k)=nanstd(v_ori(bins==k));
    v_sem(k)=v_std(k)./sqrt(numel(v_ori(bins==k)));
end

figure
shadederror_sem(middles*0.067,v_mean*0.067/5,(v_mean*0.067/5)+(v_std*0.067/5),(v_mean*0.067/5)-(v_std*0.067/5),v_sem*0.067/5,"Foci distances from frame of splitting");
%plot(middles*0.067,v_mean*0.067./fr)
hold on
xline(0)
yline(0)
hold off
xlim([-1 1])
ylim([-0.4 0.3])
xlabel("Ori position(um)")
ylabel("Ori speed (um/min)")
title("Speed of Ori (1 Focus)")


