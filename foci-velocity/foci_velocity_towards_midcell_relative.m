function focivelocity=foci_velocity_towards_midcell_relative(varargin)
cycle_list=varargin{1};
cycle_ids=varargin{2};
fc=['foci',num2str(varargin{3})];
synch_time_fn=varargin{4};
args_for_synch_func=varargin(5:nargin);



k=1;
for i=cycle_ids'
    if isfield(cycle_list(i).(['foci',num2str(varargin{3})]),'position_relative') &&...
            ~isempty(cycle_list(i).(['foci',num2str(varargin{3})]).position_relative)
        for j=1:size(cycle_list(i).(fc).position_relative(1,:,1),2)
            k=k+1;
        end
    end
end
focivelocity=nan(k,121);
k=1;
I=synch_time_fn(cycle_list,cycle_ids,args_for_synch_func{:});
for i=1:numel(cycle_ids)
    if isfield(cycle_list(cycle_ids(i)).(['foci',num2str(varargin{3})]),'position_relative') &&...
            ~isempty(cycle_list(cycle_ids(i)).(['foci',num2str(varargin{3})]).position_relative)
        
        if ~isnan(I(i)) && ~isempty(I(i)) && I(i)>0
            for j=1:size(cycle_list(cycle_ids(i)).(fc).position_relative(1,:,1),2)
                v=gradient(cycle_list(cycle_ids(i)).(fc).position_relative(:,j,1)./cycle_list(cycle_ids(i)).lengths);
                v=-v.*sign(cycle_list(cycle_ids(i)).(fc).position_relative(:,j,1));
                focivelocity(k,60-I(i)+1:59)=(v(1:I(i)-1))';
                focivelocity(k,60:60+(length(v)-I(i)))=(v(I(i):end))';
                k=k+1;
            end
        end
    end
end
focivelocity=focivelocity./5;
figure
errorbar((-59:1:61)*5,nanmean(focivelocity),nanstd(focivelocity)./sqrt(sum(~isnan(focivelocity))))
xlim([-100 100])
ylabel("Relative velocity")
xlabel("Time (min)")
end
