%% To find foci foci velocity towards mid-cell that is accounted for growth rate
function focivelocity=foci_velocity_towards_midcell3(varargin)
cycle_list=varargin{1};
cycle_ids=varargin{2};
fc=['foci',num2str(varargin{3})];
synch_time_fn=varargin{4};
args_for_synch_func=varargin(5:nargin);



k=1;
for i=cycle_ids'
    if isfield(cycle_list(i).(['foci',num2str(varargin{3})]),'position_relative') &&...
            ~isempty(cycle_list(i).(['foci',num2str(varargin{3})]).position_relative)
        for j=1:size(cycle_list(i).(fc).position_relative(1,:,1),2)
            k=k+1;
        end
    end
end
focivelocity=nan(k,121);
k=1;
I=synch_time_fn(cycle_list,cycle_ids,args_for_synch_func{:});
for i=1:numel(cycle_ids)
    if isfield(cycle_list(cycle_ids(i)).(['foci',num2str(varargin{3})]),'position_relative') &&...
            ~isempty(cycle_list(cycle_ids(i)).(['foci',num2str(varargin{3})]).position_relative)
       
        if ~isnan(I(i)) && ~isempty(I(i)) && I(i)>0
            cycle=cycle_list(cycle_ids(i));
            for j=1:size(cycle.(fc).position_relative(1,:,1),2)%max number of foci
                pos=cycle.(fc).position_relative(:,j,1));

                growth=cycle.lengths(3:end)./cycle.lengths(1:end-2);%change in length

                v_corr=nan(length(pos),1);
                v_corr(2:end-1)=(pos(3:end)-pos(1:end-2).*growth)/2; %corrected step-wise velocity
                v_corr(1)=pos(2)-cycle.lengths(2)/cycle.lengths(1)*pos(1);
                v_corr(end)=pos(end)-cycle.lengths(end)/cycle.lengths(end-1)*pos(end-1);
                v_corr=-v_corr.*sign(cycle.(fc).position_relative(:,j,1));
                focivelocity(k,60-I(i)+1:59)=(v_corr(1:I(i)-1))';
                focivelocity(k,60:60+(length(v_corr)-I(i)))=(v_corr(I(i):end))';
                k=k+1;
            end
        end
    end
end
focivelocity=focivelocity./5;
figure
errorbar((-59:1:61)*5,nanmean(focivelocity)*.067,nanstd(focivelocity)*.067./sqrt(sum(~isnan(focivelocity))))
xlim([-100 100])
ylabel("Micrometers per minute")
xlabel("Time (min)")
end
