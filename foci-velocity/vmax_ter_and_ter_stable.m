cycle_ids=complete_cycle_ids;
vmax_stable_difference=nan(length(cycle_ids),1);

for i=1:length(cycle_ids)
    cycle=cycle_list(cycle_ids(i));
if ~isempty(cycle.foci1.inferred_position_relative)
    if size(cycle.foci1.start_and_stop,1)<3 % Consider only cells that has one or two foci tracks
                pos=nan(size(cycle.foci1.inferred_position_relative(:,1,1)));
                if size(cycle.foci1.start_and_stop,1)==2 && cycle.foci1.start_and_stop(1)~=cycle.foci1.start_and_stop(2) % Exclude cells that has two tracks both starting from frame one - reason: unlikely event
                    pos(1:cycle.foci1.start_and_stop(2,1))=cycle.foci1.inferred_position_relative(1:cycle.foci1.start_and_stop(2,1),1,1); % Velocity towards oldpole
                elseif size(cycle.foci1.start_and_stop,1)==1
                    pos=cycle.foci1.inferred_position_relative(:,1,1);
                end
    end
end

    I=get_stable_time_at_middle(cycle,1,2.4);
    f=max(diff(pos));
    vmax_stable_difference(i)=(f-I)*5;
end
figure
histogram(vmax_stable_difference);