%% To find foci foci velocity towards mid-cell with a flag to have growth correction or not
% arg 1 : cycle_list
% arg 2 : cycle_ids
% arg 3 : fluor_channel
% arg 4 : synchronizing function
% arg 5 until the second last arg : arguments for sync function
% arg last : flag to indicate whether to account for growth rate or not
function focivelocity=forward_velocity_towards_midcell_inferred(varargin)
cycle_list=varargin{1};
cycle_ids=varargin{2};
fc=['foci',num2str(varargin{3})];
synch_time_fn=varargin{4};
args_for_synch_func=varargin(5:nargin-1);
gc=cell2mat(varargin(nargin));

f0=60;%put the frame of the event in the this bin= max no. of frames shown before the event
f1=61;%max no. of frames to show after the event, total number of frames shown is f0+f1
fref=0.5; %assign the frame of the event

k=1;
for i=cycle_ids'
    i
    if isfield(cycle_list(cycle_ids(i)).(['foci',num2str(varargin{3})]),'inferred_position_relative') &&...
            ~isempty(cycle_list(cycle_ids(i)).(['foci',num2str(varargin{3})]).inferred_position_relative)
        for j=1:size(cycle_list(i).(fc).inferred_position_relative(1,:,1),2)
            k=k+1;
        end
    end
end
focivelocity=nan(k,f0+f1);
k=1;
I=synch_time_fn(cycle_list,cycle_ids,args_for_synch_func{:});

if gc
    for i=1:numel(cycle_ids)
        if isfield(cycle_list(cycle_ids(i)).(['foci',num2str(varargin{3})]),'inferred_position_relative') &&...
                ~isempty(cycle_list(cycle_ids(i)).(['foci',num2str(varargin{3})]).inferred_position_relative)
            
            if ~isnan(I(i)) && ~isempty(I(i)) && I(i)>0
                cycle=cycle_list(cycle_ids(i));
                for j=1%:size(cycle.(fc).inferred_position_relative(1,:,1),2)%max number of foci
                    pos=cycle.(fc).inferred_position_relative(:,j,1)*cycle.pole;
                    
                    growth=cycle.lengths(2:end)./cycle.lengths(1:end-1);%change in length
                    
                    v_corr=pos(2:end)-pos(1:end-1).*growth; %corrected forward velocity
                    %v_corr=-v_corr.*sign(cycle.(fc).inferred_position_relative(1:end-1,j,1));
                    focivelocity(k,(f0-I(i)+1):(f0-1))=(v_corr(1:I(i)-1))';%velocity on frames before the event
                    focivelocity(k,f0:(f0+(min(f1,length(v_corr))-I(i))))=(v_corr(I(i):min(f1,length(v_corr))))';%% corrected to take only a maximum of 61 frames after sync time in case of longer cell cycles
                    k=k+1;
                end
            end
        end
    end
else
    for i=1:numel(cycle_ids)
        if isfield(cycle_list(cycle_ids(i)).(['foci',num2str(varargin{3})]),'inferred_position_relative') &&...
                ~isempty(cycle_list(cycle_ids(i)).(['foci',num2str(varargin{3})]).inferred_position_relative)
            
            if ~isnan(I(i)) && ~isempty(I(i)) && I(i)>0
                for j=1:size(cycle_list(cycle_ids(i)).(fc).inferred_position_relative(1,:,1),2)
                    v=diff(cycle_list(cycle_ids(i)).(fc).inferred_position_relative(:,j,1));
                    v=-v.*sign(cycle_list(cycle_ids(i)).(fc).inferred_position_relative(1:end-1 ,j,1));
                    focivelocity(k,(f0-I(i)+1):(f0-1))=(v(1:I(i)-1))';
                    focivelocity(k,f0:(f0+(min(f1,length(v))-I(i))))=(v(I(i):min(f1,length(v))))';%% corrected to take only a maximum of 61 frames after sync time in case of longer cell cycles
                    k=k+1;
                end
            end
        end
    end
    
end
focivelocity=focivelocity./5;
figure
subplot(2,1,1)
errorbar((fref+[(1-f0):1:(f1)])*5,nanmean(focivelocity)*.067,nanstd(focivelocity)*.067./sqrt(sum(~isnan(focivelocity))))
xlim([-100 100])
ax=gca; ax.YAxis.Exponent = -2;
ylabel("Mean velocity (\mu m/min)")
xlabel("Time (min)")

subplot(2,1,2)
errorbar((fref+[(1-f0):1:(f1)])*5,nanmedian(focivelocity)*.067,nanstd(focivelocity)*.067./sqrt(sum(~isnan(focivelocity))))
xlim([-100 100])
ax=gca; ax.YAxis.Exponent = -2;
ylabel("Median velocity (\mu m/min)")
xlabel("Time (min)")
end
