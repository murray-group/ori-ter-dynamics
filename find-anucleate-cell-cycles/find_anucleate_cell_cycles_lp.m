
% Add line profiles to all the cycles before executing the rest of the script
% [cycle_list] = add_lineprofiles(channel_list,cycle_list,1,[1 0],[0,0]);

%% Get the distribution of average fluorescence intensity for all daughter cell cycles


mean_signal=[];
for i=1:numel(complete_cycle_ids)
    i
    cycle=cycle_list(complete_cycle_ids(i));
    children=cycle.relation_graph.children;
    
    for j=children
        cell_signal=0;
        for k=1:cycle_list(j).duration
            cell_signal=cell_signal+mean(cycle_list(j).fluor1_lp{k}.*sum(cycle_list(j).masks{k},2));
           
        end
        mean_signal=[mean_signal cell_signal./cycle_list(j).duration];
        
    end
    %end
end


%%

anucleate=[];

k=1;


for i=1:numel(complete_cycle_ids)
    i
    cycle=cycle_list(complete_cycle_ids(i));
    children=cycle.relation_graph.children;
    
    for j=children
        cell_signal=0;
        for k=1:cycle_list(j).duration
            cell_signal=cell_signal+mean(cycle_list(j).fluor1_lp{k}.*sum(cycle_list(j).masks{k},2));
        end
        mean_signal=cell_signal./cycle_list(j).duration;
        
        if mean_signal<36
            anucleate=[anucleate cycle.cycle_id];
        end
    end
    %end
end