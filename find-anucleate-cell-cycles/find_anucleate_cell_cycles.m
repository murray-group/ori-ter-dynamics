%% Script to find anucleate daughter cells using ori foci information

k=1;
anucleate=[];
for i=1:numel(complete_cycle_ids)
    i
    cycle=cycle_list(complete_cycle_ids(i));
    if ~isempty(cycle.foci2.position)
        %n=sum(~isnan(cycle.foci2.position(end,:,1)));
        %if n==1
            children=cycle.relation_graph.children;
            
            for j=children
                if isempty(cycle_list(j).foci2.position) && cycle_list(j).duration >=3
                    %if sum(~isnan(cycle_list(j).foci2.position(,:,1)))==0                        
                        anucleate = [anucleate cycle.cycle_id children];
                        k=k+1;
                        continue;
                    %end
                end
            end
        %end
    end
end