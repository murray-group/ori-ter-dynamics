function foci_kymograph_cell_age(cycle_list,complete_cycle_ids,nbins_y,nbins_x,fluor_channel)

fc=strcat("foci",string(fluor_channel));

complete_cycles=cycle_list(complete_cycle_ids);
growth_rate = 1.01;
data_bins_nos_y= round(power(growth_rate,linspace(0,log(2)/log(growth_rate),nbins_x))*nbins_y/2); %exponential
bin_edges_x=linspace(0,1,nbins_x+1);

data={};
ndata=zeros(1,nbins_x);
for i=1:numel(data_bins_nos_y)
    bin_edges_y{i}=linspace(-0.51,0.51,data_bins_nos_y(i)+1);
    data{i}=zeros(data_bins_nos_y(i),1);
end


for i=1:numel(complete_cycles)
    if isfield(complete_cycles(i).(['foci',num2str(fluor_channel)]),'position_relative') && isfield(complete_cycles(i).(['foci',num2str(fluor_channel)]),'position_relative') &&...
            ~isempty(complete_cycles(i).(['foci',num2str(fluor_channel)]).position_relative) && ~isempty(complete_cycles(i).(['foci',num2str(fluor_channel)]).position_relative)
        [complete_cycles(i).(fc).('foci_position_major')]=complete_cycles(i).(fc).position_relative(:,:,1)./complete_cycles(i).lengths;
        norm_length=linspace(0,1,complete_cycles(i).duration);
        [~,~,bins_x]=histcounts(norm_length,bin_edges_x);
        ndata=ndata+histcounts(bins_x,(0.5:1:nbins_x+0.5));
        for k=1:size(complete_cycles(i).(fc).foci_position_major,2)
            for j=1:complete_cycles(i).duration
                if ~isnan(complete_cycles(i).(fc).foci_position_major(j,k)) &&...
                        abs(complete_cycles(i).(fc).foci_position_major(j,k))<0.51
                    [~,~,bins_y]=histcounts(complete_cycles(i).(fc).foci_position_major(j,k,1).*complete_cycles(i).pole,bin_edges_y{bins_x(j)});
                    data{bins_x(j)}(bins_y)=data{bins_x(j)}(bins_y)+1;
                end
            end
            
            
        end
    end
end
canvas=nan(nbins_y,nbins_x);
for i=1:size(data,2)
    lb=round((nbins_y-data_bins_nos_y(i))/2)+1;
    canvas(lb:lb+data_bins_nos_y(i)-1,i)=data{i};
end

%ndata=nansum(canvas);
canvas=canvas./ndata;

figure
imagesc(canvas);

imAlpha=ones(size(canvas));
imAlpha(isnan(canvas))=0;
imagesc(canvas,'AlphaData',imAlpha);
set(gca,'color',[1 1 1]);

x = linspace(0,1,5);
xticks = x*(nbins_x-1)+1;
xlabel("Relative cell age")
set(gca,'XTick',xticks, 'XTickLabel', x);

y = linspace(0,2,5);
yticks = x*(nbins_y-1)+1;
set(gca,'YTick',yticks, 'YTickLabel', -1*y+1);
ylabel("Relative cell length")

colorbar

% figure
% imagesc(linspace(0,1,nbins_x),linspace(0.51,-0.51,nbins_y),data)
% ax=gca;
% ax.YDir='normal';
%
% xlabel("Relative cell age")
% ylabel("Relative position inside cell")

end