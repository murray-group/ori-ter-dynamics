function canvas=foci_kymograph(cycle_list,complete_cycle_ids,nbins_y,nbins_x,fluor_channel,offset)

fc=strcat("foci",string(fluor_channel));

complete_cycles=cycle_list(complete_cycle_ids);

bin_edges_y=linspace(-1,1,nbins_y+1);
bin_edges_x=linspace(0,1,nbins_x+1);

ndata=zeros(1,nbins_x);

data=zeros(nbins_y,nbins_x);



for i=1:numel(complete_cycles)
    if isfield(complete_cycles(i).(['foci',num2str(fluor_channel)]),'position_relative') && isfield(complete_cycles(i).(['foci',num2str(fluor_channel)]),'position_relative') &&...
            ~isempty(complete_cycles(i).(['foci',num2str(fluor_channel)]).position_relative) && ~isempty(complete_cycles(i).(['foci',num2str(fluor_channel)]).position_relative)
        positions=-(complete_cycles(i).(fc).position_relative(:,:,1)-offset)./complete_cycles(i).lengths*complete_cycles(i).pole;
        age=linspace(0,1,complete_cycles(i).duration);
        [counts,~,tbins]=histcounts(age,bin_edges_x);
        ndata=ndata+counts;
        
       for j=1:length(tbins)%loop over the time bins relevant for this cell cycle
                    ycounts=histcounts(positions(j,:,1).*exp(log(2)*(tbins(j)-1)/(nbins_x-1)),bin_edges_y);
                    data(:,tbins(j))=data(:,tbins(j))+ycounts';
       end

    end
end


for i=1:nbins_x
    N1=histcounts(-0.5*exp(log(2)*(i-1)/(nbins_x-1)),bin_edges_y);
    I1=find(N1,1);
    N2=histcounts(0.5*exp(log(2)*(i-1)/(nbins_x-1)),bin_edges_y);
    I2=find(N2,1);
    data(1:(I1-1),i)=NaN;
    data((I2+1):end,i)=NaN;
end

canvas=data./ndata;

figure
%imagesc(canvas);


imAlpha=~isnan(canvas);
imagesc(linspace(0,1,nbins_x),linspace(-1,1,nbins_y),canvas,'AlphaData',imAlpha);
set(gca,'color',[1 1 1]);

xticks = linspace(0,1,5);
%xticks = x*(nbins_x-1)+1;
xlabel("Cell age")
set(gca,'XTick',xticks);

yticks = linspace(-1,1,5);
%yticks = x*(nbins_y-1)+1;
set(gca,'YTick',yticks);
ylabel("Relative positions (-1 oldpole, +1 newpole)")

set(gca,'YDir','normal');
set(gca,'TickDir','out');
colorbar

% figure
% imagesc(linspace(0,1,nbins_x),linspace(0.51,-0.51,nbins_y),data)
% ax=gca;
% ax.YDir='normal';
%
% xlabel("Relative cell age")
% ylabel("Relative position inside cell")

end