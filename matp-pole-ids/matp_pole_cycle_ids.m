function matp_pole_ids=matp_pole_cycle_ids(cycle_list,complete_cycle_ids)
matp_pole_ids=[];
complete_cycles=cycle_list(complete_cycle_ids);
for i=1:numel(complete_cycles)
    if isfield(complete_cycles(i).foci2,'position_relative') && ~isempty(complete_cycles(i).foci2.position_relative)
    temp1=complete_cycles(i).foci2.position_relative./complete_cycles(i).lengths;
    if abs(temp1(find(~isnan(temp1),1))) >= 0.25
        matp_pole_ids=[matp_pole_ids;complete_cycles(i).cycle_id];
    end
    end
end
end
