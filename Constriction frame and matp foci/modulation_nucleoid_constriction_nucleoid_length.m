[~,all_nucleoid_lengths]=nucleoid_length(cycle_list,complete_cycle_ids);
bin_edges=linspace(min(all_nucleoid_lengths)-0.5,max(all_nucleoid_lengths)+0.5,23);
data=zeros(1,length(bin_edges)-1);
data_cell=nan(505529,length(bin_edges)-1);
n_data=zeros(1,length(bin_edges)-1);
std_data=[];
sem_data=[];
k=1;

modulation=nucleoid_modulation(cycle_list,complete_cycle_ids,peaks_array);
for i=1:numel(modulation) % Loop to query the entire complete cell cycles
    %if (max(modulation{i}(3,:))<=49 && min(modulation{i}(3,:)>10))
        [~,~,bins]=histcounts(modulation{i}(3,:),bin_edges);
        for j=1:length(modulation{i})
            data(1,bins(j))=data(1,bins(j))+modulation{i}(2,j);
            data_cell(k,bins(j))=modulation{i}(2,j);
            n_data(bins(j))=n_data(bins(j))+1;
            k=k+1;
        end
    %end
end
data=data./n_data;
std_data=nanstd(data_cell);
sem_data=std_data./sqrt(n_data);

%% Plotting
figure
shadederror(linspace(9,51,length(bin_edges)-1)*.067,data,data+sem_data,data-sem_data,'Modulation_curve')
ylabel("Modulation with SEM")
xlabel("FWHM Nucleoid lengths (um)")

figure
shadederror(linspace(9,51,length(bin_edges)-1)*.067,data,data+std_data,data-std_data,'Modulation_curve')
ylabel("Modulation with SD")
xlabel("FWHM Nucleoid lengths (um)")


%% Distribution of depths at the frame in which they become continuous

for i=1:numel(peaks_array)
    depth_distribution(i)=peaks_array{i}{constriction_frames_cont(i)}(4);
end

