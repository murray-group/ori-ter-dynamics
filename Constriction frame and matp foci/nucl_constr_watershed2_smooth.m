%% Find the valleys (nucleoid constrictions) using Sean's script (modified)

function [peaks_array,modulation]=nucl_constr_watershed2_smooth(cycle_list,complete_cycle_ids,nucleoid_lengths)
peaks_array=[];
modulation=[];
for ccc=1:numel(complete_cycle_ids) %Note: the variable ccc stands for Completed Cell Cycles
    cycle_num=complete_cycle_ids(ccc);
    for frame=1:numel(cycle_list(cycle_num).fluor1_lp)
        ys=smooth(((cycle_list(cycle_num).fluor1_lp{1,frame})./max(cycle_list(cycle_num).fluor1_lp{1,frame})));
        xdata=linspace(0,1,numel(cycle_list(cycle_num).fluor1_lp{1,frame}));
        [ht,loc,wd,dp]=find_valleys(ys,xdata);
        
        peaks_array{ccc}{frame}=[ht,loc,wd,dp];
        
        if isempty(dp)
            modulation{ccc}(1,frame)=frame./cycle_list(cycle_num).duration;
            modulation{ccc}(2,frame)=0;
            modulation{ccc}(3,frame)=nucleoid_lengths{ccc}(frame);
        else
            modulation{ccc}(1,frame)=frame./cycle_list(cycle_num).duration;
            modulation{ccc}(2,frame)=dp./(max(ys)-min(ys));
            modulation{ccc}(3,frame)=nucleoid_lengths{ccc}(frame);

        end
        %               figure (5)
        %             subplot(5,5,frame);
        %             plot(xdata,ys);
        %             hold on
        %             plot(loc,ht,'o')
        %             hold off
    end
    %         pause(5)
    %        close figure 5
end
end
%%

function [height,loc,w,depth]=find_valleys(ys,x)

valleys=watershed(ys);%breaks the profile up into valleys
%

[height,loc,left,right,depth]=measure_valleys(valleys,ys);


%
%find the deepest valley


while length(depth)>1
    [min_d,i]=min(depth);
    d=min_d+0.000001;


    if((ys(left(i))-height)<d)%first check if left side shallow
        valleys(left(i))=valleys(left(i)-1);
        temp=valleys(left(i)+1:end);
        temp(temp>0)=temp(temp>0)-1;
        valleys(left(i)+1:end)=temp;
        
    else %if not, check if right side is shallow
        valleys(right(i))=valleys(right(i)-1);
        temp=valleys(right(i)+1:end);
        temp(temp>0)=temp(temp>0)-1;
        valleys(right(i)+1:end)=temp;
    end
    [height,loc,left,right,depth]=measure_valleys(valleys,ys);
end

w=right-left;

if ~isempty(x)
    loc=x(loc);
    w=x(right)-x(left);
end
end



function [height,loc,left,right,depth]=measure_valleys(valleys,ys)
n_valleys=max(valleys);
if n_valleys<=2
    height=[];
    loc=[];
    left=[];
    right=[];
    depth=[];
    return;
end
for i=2:(n_valleys-1) %ignore valleys at the boundary
    [height(i-1),I]=min(ys(valleys==i));
    loc(i-1)=I+find(valleys==i,1)-1;%index of minimum
    left(i-1)=find(valleys==i,1)-1;%index of left edge
    right(i-1)=find(valleys==i,1,'last')+1;%index of right edge
    depth(i-1)=min(ys(left(i-1)),ys(right(i-1)))-height(i-1);%depth
end


end