%% Plot cell lengths upon continuous constriction

[all_cell_lengths]=all_cell_length(cycle_list,complete_cycle_ids);
[~,cell_len_at_cont_constr]=length_at_constr_frames_cont(cycle_list,complete_cycle_ids,peaks_array);

%%

h=histogram(all_cell_lengths,'Normalization','pdf')%,'FaceColor','#0072BD');
hold on
h=histogram(cell_len_at_cont_constr,'Normalization','pdf')%,'FaceColor','#D95319');
hold off
xlabel("Distribution of cell lengths during continuous constriction (pixels)");
ylabel("PDF");

%%
all_cell_lengths=all_cell_lengths*0.067;
cell_len_at_cont_constr=cell_len_at_cont_constr*0.067;

%%
figure
histogram(all_cell_lengths,22,'Normalization','pdf')%,'FaceColor','#0072BD');
hold on
histogram(cell_len_at_cont_constr,22,'Normalization','pdf')%,'FaceColor','#D95319');
hold off
xlabel("Distribution of cell lengths during continuous constriction (um)");
ylabel("PDF");
