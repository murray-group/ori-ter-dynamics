function all_relative_foci_pos=all_foci_positions_relative(cycle_list,cycle_ids,fluor_channel,sz)

% sz is used to specify cell length restrictions
all_relative_foci_pos=[];
fc=['foci',num2str(fluor_channel)];
for i=cycle_ids
    i
    if isfield(cycle_list(i).(fc),'position_relative') &&...
            ~isempty(cycle_list(i).(fc).position_relative)
        
        if length(sz)==1 && sz>0
            pos=(cycle_list(i).(fc).position_relative(:,:,1)./cycle_list(i).lengths);
            pos=pos(cycle_list(i).lengths>=sz);
            pos=pos(:);
            all_relative_foci_pos=[all_relative_foci_pos;pos(~isnan(pos))];
        elseif length(sz)==2
            pos=(cycle_list(i).(fc).position_relative(:,:,1)./cycle_list(i).lengths);
            pos=pos(cycle_list(i).lengths>=sz(1) & cycle_list(i).lengths<=sz(2));
            pos=pos(:);
            all_relative_foci_pos=[all_relative_foci_pos;pos(~isnan(pos))];
        else
            pos=(cycle_list(i).(fc).position_relative(:,:,1)./cycle_list(i).lengths);
            pos=pos(:);
            all_relative_foci_pos=[all_relative_foci_pos;pos(~isnan(pos))];
        end
    end
end