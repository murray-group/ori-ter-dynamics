function [all_relative_pos_until_I,all_relative_pos_from_I]=all_foci_positions_relative2(varargin) % To find all foci positions relative to midcell (0-0.5) until a frame specified by a function

cycle_list = varargin{1};
cycle_ids = varargin{2};
fluor_channel = varargin{3};
time_fn = varargin{4};
args_for_time_fn= varargin(5:nargin);
all_relative_pos_until_I=[];
all_relative_pos_from_I=[];
for i=cycle_ids
    i
    I=time_fn(cycle_list(i),args_for_time_fn{:});
    if isfield(cycle_list(i).(['foci',num2str(fluor_channel)]),'position_relative') &&...
            ~isempty(cycle_list(i).(['foci',num2str(fluor_channel)]).position_relative) && ~isnan(I)
        
        
        pos=(cycle_list(i).(['foci',num2str(fluor_channel)]).position_relative(:,:,1));%./cycle_list(i).lengths);
        pos_till_I=pos(1:I-1,:);
        pos_from_I=pos(I:end,:);
        
        pos_till_I=pos_till_I(:);
        pos_from_I=pos_from_I(:);
        
        all_relative_pos_until_I=[all_relative_pos_until_I;pos_till_I(~isnan(pos_till_I))];
        all_relative_pos_from_I=[all_relative_pos_from_I;pos_from_I(~isnan(pos_from_I))];
        
        
    end
end
end