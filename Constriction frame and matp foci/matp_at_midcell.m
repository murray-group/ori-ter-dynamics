%% First frame in which MatP foci comes to middle

% In the cycle_list data structure, foci position is represented between as
% absolute length from one pole

% To define a region as middle, a cell is split into 3 parts.

% In the next step query each cell cycle to identify the first frame in
% which the MatP foci comes to the middle 1/3rd part

% Make a logical array that tells in each frame whether the matp foci is in
% the middle section or not

function [first_frames,cell_age_matp_at_middle]=matp_at_midcell(complete_cycles,duration_complete_cycles,a,b)
matp_foci=struct([]);
for i=1:numel(complete_cycles)
    j=1;
    matp_foci(i).cycle_id=complete_cycles(i).cycle_id;
    temp3=complete_cycles(i).foci1.position_relative(:,1,1)./complete_cycles(i).major; % generates foci position relative to mid-cell
    matp_foci(i).mid_loc=abs(temp3)>a & abs(temp3)<b; % Creates a logical array for when MatP reaches middle first
    while matp_foci(i).mid_loc(j)==0
        j=j+1;
        if j==numel(matp_foci(i).mid_loc)+1 % Fail safe if no foci has been detected in the cell cycle
            j=nan;
            break
        end
    end
    matp_foci(i).first_frame=j;
end
first_frames=[];
cell_age_matp_at_middle=[];
for i=1:numel(matp_foci)
    first_frames(i)=matp_foci(i).first_frame;
    cell_age_matp_at_middle(i)=matp_foci(i).first_frame./duration_complete_cycles(i);
end
end
