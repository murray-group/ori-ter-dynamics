cycle_ids=complete_cycle_ids;
a=nan(numel(cycle_ids),1);
b=nan(numel(cycle_ids),1);
c=nan(numel(cycle_ids),1);
for i=1:length(cycle_ids)
    cycle=cycle_list(cycle_ids(i));
    a(i)=get_frame_diff_stable_and_first(cycle,1,2.4);
b(i)=get_stable_time_at_middle(cycle,1,2.4);
c(i)=get_first_time_at_middle(cycle,1,2.4);
end

%%
%%
a2=b-c;
I=isnan(a2);
a2=a2(~I);

a(a==0)=NaN;
a=a(~I);

figure
histogram(a2,'Normalization','pdf','FaceColor','r')
hold on
histogram(a,'Normalization','pdf','FaceColor','b','FaceAlpha',0.5)
hold off
%set(gca, 'YScale', 'log')
xlabel("MatP_{stable} - MatP_{first}")
ylabel("PDF")
title("Difference between MatP_{stable} and MatP_{first} frames - C20")
ylim([0 0.6])
xlim([-1 25])
