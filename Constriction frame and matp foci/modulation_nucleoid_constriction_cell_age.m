med_duration=23;
data=zeros(1,med_duration);
cell_lengths=all_cell_length(cycle_list,complete_cycle_ids);
data_cell=nan(numel(cell_lengths),med_duration);
n_data=zeros(1,med_duration);
bin_edges=linspace(-0.01,1.01,med_duration+1);
std_data=[];
sem_data=[];
k=1;
[peaks_array]=nucleoid_constriction_watershedv2(cycle_list,complete_cycle_ids,0);
modulation=nucleoid_modulation(cycle_list,complete_cycle_ids,peaks_array);
for i=1:numel(modulation) % Loop to query the entire complete cell cycles
    [~,~,bins]=histcounts(modulation{i}(1,:),bin_edges);
    for j=1:length(modulation{i})
        data(1,bins(j))=data(1,bins(j))+modulation{i}(2,j);
        data_cell(k,bins(j))=modulation{i}(2,j);
        n_data(bins(j))=n_data(bins(j))+1;
        k=k+1;
    end
end

data=data./n_data;
std_data=nanstd(data_cell);
sem_data=std_data./sqrt(n_data);
%%
percentile_95=prctile(data_cell(:,1),95)

%% Plotting

figure
shadederror(linspace(0,1,med_duration),data,data+sem_data,data-sem_data,'Modulation_curve')
ylabel("Modulation with SEM")
xlabel("Relative cell age")

figure
shadederror(linspace(0,1,med_duration),data,data+std_data,data-std_data,'Modulation_curve')
ylabel("Modulation with SD")
xlabel("Relative cell age")

%% Distribution of depths at the frame in which they become continuous

for i=1:numel(peaks_array)
    depth_distribution(i)=peaks_array{i}{constriction_frames_cont(i)}(4);
end

