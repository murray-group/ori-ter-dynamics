complete_cycles=cycle_list(matp_pole_ids);
cell_length_first_frame=[];
relative_length_first_frame=[];
for i=1:numel(complete_cycles)
    if ~isnan(first_frames(i))
        
        cell_length_first_frame(i)=complete_cycles(i).lengths(first_frames(i));
        relative_length_first_frame(i)=complete_cycles(i).lengths(first_frames(i))./max(complete_cycles(i).lengths);
    else
        cell_length_first_frame(i)=nan;
        relative_length_first_frame(i)=nan;
    end
end