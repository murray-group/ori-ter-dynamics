function [all_nucleoid_lengths,all_cell_lengths,length_at_cont_constriction,nucleoid_lengths,nucleoid_length_first_frame,nucleoid_length_first_frame_relative,cell_length_at_cont_constriction]=nuc_length(cycle_list,complete_cycle_ids,constriction_frames_cont,matp_first_frames)

% Inputs:
% cycle_list: All cycles
% complete_cycle_ids: Array of cycle_id's that should be passed through for the analysis
% constriction_frames: Array of frame numbers in which nucleoid constriction starts to occur for each cycle in complete_cycle_ids variable of this function
% matp_first_frames: Array of frame numbers in which MatP first arrives at the mid-cell for each cycle in complete_cycle_ids variable of this function

lp_normalized=[]; % Line profiles in frame normalized to the highest value
lp_to_nucleoid_length=[]; % Produces a logical array in a cell structure for each frame where indices that has intensity value
                          % greater than or equal to FWHM is true
nucleoid_lengths=[]; % Produces a cell structure having the FWHM nucleoid lengths of cells in each cycle
length_at_cont_constriction=[]; % FWHM nucleoid length in the frame of each cycle in which continuous nucleoid constriction starts
all_nucleoid_lengths=[]; % Produces an array of all the FWHM of nucleoid lengths
all_cell_lengths=[]; % Produces an array of all the cell lengths
z=1;
nucleoid_length_first_frame=[]; % FWHM nucleoid length in that frame in which MatP first localizes to mid-cell position
nucleoid_length_first_frame_relative=[]; % FWHM nucleoid length relative to the in that frame in which MatP first localizes to mid-cell position
[nucleoid_lengths,all_nucleoid_lengths]=nucleoid_length(cycle_list,complete_cycle_ids);
for ccc=1:numel(complete_cycle_ids) % Note: the variable ccc stands for Completed Cell Cycles
    cycle_num=complete_cycle_ids(ccc);
    if ~isnan(matp_first_frames(ccc))
        nucleoid_length_first_frame(ccc)=nucleoid_lengths{ccc}(matp_first_frames(ccc));
        nucleoid_length_first_frame_relative(ccc)=nucleoid_lengths{ccc}(matp_first_frames(ccc))./max(nucleoid_lengths{ccc});
    else
        nucleoid_length_first_frame(ccc)=nan;
        nucleoid_length_first_frame_relative(ccc)=nan;
    end
    if ~isnan(constriction_frames_cont(ccc))
        length_at_cont_constriction(ccc)=nucleoid_lengths{ccc}(constriction_frames_cont(ccc));
        cell_length_at_cont_constriction(ccc)=cycle_list(cycle_num).lengths(constriction_frames_cont(ccc));
    else
        cell_length_at_cont_constriction(ccc)=nan;
        length_at_cont_constriction(ccc)=nan;
    end
end
end