%% Find the valleys (nucleoid constrictions) using Sean's script (modified)

peaks_array_single=[];
modulation=[];
 
cycle_num=complete_cycle_ids(m(57));
k=cycle_list(cycle_num).duration;


for frame=1:numel(cycle_list(cycle_num).fluor1_lp)
    ys=(((cycle_list(cycle_num).fluor1_lp{1,frame})./max(cycle_list(cycle_num).fluor1_lp{1,frame})));
    xdata=linspace(0,1,numel(cycle_list(cycle_num).fluor1_lp{1,frame}));
    [ht,loc,wd,dp]=find_valleys(ys,xdata);
    
    
    peaks_array_single{1}{frame}=[ht,loc,wd,dp];
    %close figure(6)
    figure (7)
    subplot(6,6,frame);
    hold on
    
    plot(xdata,ys);
    plot(loc,ht,'or')
    hold off
end
%         pause(5)
%        close figure 5


%%

function [height,loc,w,depth]=find_valleys(ys,x)

valleys=watershed(ys);%breaks the profile up into valleys
%

[height,loc,left,right,depth]=measure_valleys(valleys,ys);


%
%find the deepest valley


while length(depth)>1
    [min_d,i]=min(depth);
    d=min_d+0.0001;


    if((ys(left(i))-height(i))<d)%first check if left side shallow
        valleys(left(i))=valleys(left(i)-1);
        temp=valleys(left(i)+1:end);
        temp(temp>0)=temp(temp>0)-1;
        valleys(left(i)+1:end)=temp;
        
    else %if not, check if right side is shallow
        valleys(right(i))=valleys(right(i)-1);
        temp=valleys(right(i)+1:end);
        temp(temp>0)=temp(temp>0)-1;
        valleys(right(i)+1:end)=temp;
    end
    [height,loc,left,right,depth]=measure_valleys(valleys,ys);
end

w=right-left;

if ~isempty(x)
    loc=x(loc);
    w=x(right)-x(left);
end

if depth/(max(ys)-min(ys))<0.12898
    height=[];
    loc=[];
    w=[];
    depth=[];
end
    
end



function [height,loc,left,right,depth]=measure_valleys(valleys,ys)
n_valleys=max(valleys);
if n_valleys<=2
    height=[];
    loc=[];
    left=[];
    right=[];
    depth=[];
    return;
end
for i=2:(n_valleys-1) %ignore valleys at the boundary
    [height(i-1),I]=min(ys(valleys==i));
    loc(i-1)=I+find(valleys==i,1)-1;%index of minimum
    left(i-1)=find(valleys==i,1)-1;%index of left edge
    right(i-1)=find(valleys==i,1,'last')+1;%index of right edge
    depth(i-1)=min(ys(left(i-1)),ys(right(i-1)))-height(i-1);%depth
end


end