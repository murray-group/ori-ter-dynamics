peaks_array=[];
    cycle_num=matp_pole_ids(1)
    for frame=1:numel(cycle_list(cycle_num).fluor1_lp)
        ys=smooth(((cycle_list(cycle_num).fluor1_lp{1,frame})./max(cycle_list(cycle_num).fluor1_lp{1,frame})));
        %ys2=smooth(((cycle_list(cycle_num).fluor2_lp{1,frame})./max(cycle_list(cycle_num).fluor2_lp{1,frame})));
        xdata=linspace(0,1,numel(cycle_list(cycle_num).fluor1_lp{1,frame}));
        [l,r]=mid_cell(ys,xdata);
        loc_min=logical(zeros(numel(ys),1));
        loc_min(l:r)=islocalmin(ys(l:r),'MinSeparation',5,'MaxNumExtrema',1);
       
        
        %peaks_array{1}{frame}=[xdata(loc_min)' ys(loc_min)];
               figure (7)
            subplot(5,5,frame);
            plot(xdata,ys,'r');
            hold on
            %plot(xdata,ys2,'Color','#EDB120');
            plot(xdata(loc_min)', ys(loc_min),'ob')
            hold off
     end
%         pause(5)
%        close figure 5


% function to query for constriction only near the center of the nucleoid
% signal
function [l,r]=mid_cell(ys,xdata)
fwhm_lp=(xdata>=0.25 & xdata<=0.75);
l=1;
while fwhm_lp(l)==0
    l=l+1;
end
r=numel(fwhm_lp);
while fwhm_lp(r)==0
    r=r-1;
end
end