%% First frame in which MatP appears at the center based on line profiles
function [first_frames]=matp_foci_lp(complete_cycles,a,b) % a , b specifies the lower and upper bound of relative positions within a cell

matp_lp={complete_cycles.fluor2_lp}';
matp_foci=cell(numel(matp_lp),1);
temp1={};
matp_mid_lp=struct([]);
for i=1:numel(matp_lp)
    temp1=matp_lp{i};
    matp_foci{i,1}=nan(1,numel(temp1));
    for j=1:numel(temp1)
        matp_foci{i,1}(j)=find(temp1{j}==max(temp1{j}),1)/numel(temp1{j});
    end
    matp_mid_lp(i).mid_loc=matp_foci{i,1}>a & matp_foci{i,1}<b;
    j=1;
    while matp_mid_lp(i).mid_loc(j)==0
        j=j+1;
        if j==numel(matp_mid_lp(i).mid_loc)
            j=nan;
            break
        end
    end
    matp_mid_lp(i).first_frame=j;
end
for i=1:numel(matp_mid_lp)
    first_frames(i)=matp_mid_lp(i).first_frame;
end
end