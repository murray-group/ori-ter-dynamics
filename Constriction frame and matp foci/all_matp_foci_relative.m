matp_focis=[];
parfor i=1:numel(cycle_list)
    for j=1:numel(cycle_list(i).foci2.foci_position_major)
        if ~isnan(cycle_list(i).foci2.foci_position_major(j))
            matp_focis=[matp_focis;abs(cycle_list(i).foci2.foci_position_major(j))];
        end
    end
end