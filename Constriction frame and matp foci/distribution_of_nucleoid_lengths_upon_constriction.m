%% Plot nucleoid lengths upon continuous constriction

[~,all_nucleoid_lengths]=nucleoid_length(cycle_list,complete_cycle_ids);
[nuc_len_at_cont_constr,~]=length_at_constr_frames_cont(cycle_list,complete_cycle_ids,peaks_array);

%%

histogram(all_nucleoid_lengths,'Normalization','pdf')%,'FaceColor','#0072BD');
hold on
h=histogram(nuc_len_at_cont_constr,'Normalization','pdf')%,'FaceColor','#D95319');
hold off
xlabel("Distribution of nucleoid lengths during continuous constriction (pixels)");
ylabel("PDF");

%%
all_nucleoid_lengths=all_nucleoid_lengths*0.067;
nuc_len_at_cont_constr=nuc_len_at_cont_constr*0.067;

%%
histogram(all_nucleoid_lengths,22,'Normalization','pdf')%,'FaceColor','#0072BD');
hold on
histogram(nuc_len_at_cont_constr,22,'Normalization','pdf')%,'FaceColor','#D95319');
hold off
xlabel("Distribution of nucleoid lengths during continuous constriction (um)");
ylabel("PDF");
