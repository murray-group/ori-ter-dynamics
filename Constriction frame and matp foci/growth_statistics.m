cell_cycle_ids=complete_cycle_ids;
    durations = zeros(1,numel(cell_cycle_ids));
    mean_areas = zeros(1,numel(cell_cycle_ids));
    min_area = zeros(1,numel(cell_cycle_ids));
    max_area = zeros(1,numel(cell_cycle_ids));
    birth_lengths = zeros(1,numel(cell_cycle_ids));
    division_lengths = zeros(1,numel(cell_cycle_ids));
    for i = 1:numel(cell_cycle_ids)
        cycle_id = cell_cycle_ids(i);
        cycle = cycle_list(cycle_id);
        durations(i) = cycle.duration;
        mean_areas(i) = mean(cycle.areas);
        min_area(i) = min(cycle.areas);
        max_area(i) = max(cycle.areas);
        birth_lengths(i) = cycle.lengths(1);
        division_lengths(i) = cycle.lengths(end);
    end
    