cycle_ids=complete_cycle_ids;
matp_os=nan(1,length(cycle_ids));
b=get_MatP_stable_at_middle(cycle_list,cycle_ids,1,2.4,3);
for i=1:length(cycle_ids)
    i
    cycle=cycle_list(cycle_ids(i));
    %b(i)
    if ~isempty(cycle.foci1.inferred_position_relative) && b(i)>0
       
        % if size(cycle.foci1.start_and_stop,1)==2
        temp=diff(sign(cycle.foci1.inferred_position_relative(1:b(i)-1,1,1)));
        % elseif size(cycle.foci1.start_and_stop,1)==1
        % temp=diff(sign(cycle.foci1.inferred_position_relative(1:b,1,1)));
        %end
        %find(abs(temp)==2,1)
        if ~isempty(find(abs(temp)==2,1))
            matp_os(i)=b(i)-1-find(abs(temp)==2,1);
        end
    end
    
end