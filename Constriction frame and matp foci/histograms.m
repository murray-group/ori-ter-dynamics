%% 

%% Run one of the two scripts below. Each one uses a different algorithm to find the nucleoid constrictions
%[peaks_array]=nucl_constr_watershed(cycle_list,complete_cycle_ids);
[peaks_array]=nucleoid_constriction_watershedv2(cycle_list,complete_cycle_ids,0.1125);
%% Histogram of nucleoid length at frame of constriction
% Also, find the frame in which nucleoid constriction starts ( first frame and continuous) 
% and Location of matP foci at that frame of constriction based on line profiles
complete_cycles=cycle_list(complete_cycle_ids);
[constriction_frames_first,constriction_frames_cont,constriction_frames_relative_duration]=nucleoid_and_matp(peaks_array,complete_cycles);
%%
[all_nucleoid_lengths,all_cell_lengths,length_at_cont_constriction,nucleoid_lengths,nucleoid_length_first_frame,nucleoid_length_first_frame_relative,cell_length_at_cont_constriction]=nuc_length(cycle_list,complete_cycle_ids,constriction_frames_cont,first_frames);
%%
constriction_frame_lengths=[];
for ccc=1:numel(complete_cycles) %Note: the variable ccc stands for Completed Cell Cycles
    if ~isnan(constriction_frames_cont(ccc))
        constriction_frame_lengths(ccc)=complete_cycles(ccc).lengths(constriction_frames_cont(ccc));
    else
        constriction_frame_lengths(ccc)=nan;
    end
end
%%
figure
histogram(all_nucleoid_lengths,'Normalization','pdf')
ylabel("PDF")
xlabel("Nucleoid lengths (pixels) at FWHM")

% figure
% histogram(length_at_first_constriction)
% xlabel("Nucleoid lengths (pixels) at FWHM when nucleoid first constricts")
%%
figure
histogram(length_at_cont_constriction)
xlabel("Nucleoid lengths (pixels) at FWHM when nucleoid start to continuously constrict")


%% Histogram of time between nucleoid constriction and MatP at midcell normalized to cell cycle
duration_complete_cycles=[];
complete_cycles=cycle_list(matp_pole_ids);
for i=1:numel(complete_cycles)
duration_complete_cycles(i)=complete_cycles(i).duration;
end

%%
[first_frames,cell_age_matp_at_middle]=matp_at_midcell(complete_cycles,duration_complete_cycles,0,0.0516); % function code is present in matp_foci_lp.m or matp_at_midcell.m file
%%
t_constr_minus_t_matP=(constriction_frames_cont-first_frames)./duration_complete_cycles;
figure
histogram(t_constr_minus_t_matP,22);
xlabel("Time until constriction formation after MatP moves to mid-cell (relative cell cycle)")
%%
% frames_until_division=duration_complete_cycles-constriction_frames_cont;
% figure
% histogram(frames_until_division)
% xlabel("Frames (10 min) until division after nucleoid constriction")
% 
t_constr_first_minus_t_matP=(constriction_frames_first-first_frames)./duration_complete_cycles;
figure
histogram(t_constr_first_minus_t_matP,22)
xlabel("T (nucleoid constriction first) - T (MatP foci at mid) / duration")
%% 
 figure
 histogram(constriction_frames_relative_duration,22)%,'Normalization','pdf')
xlabel("Cell age at which continuous nucleoid constriction starts")
%%
%figure
histogram(first_frames./duration_complete_cycles,22)%,'Normalization','pdf')
xlabel("Cell age at which MatP (foci) first appears in mid cell")


%% Functions

function [constriction_frames_first,constriction_frames_cont,constriction_frames_relative_duration]=nucleoid_and_matp(peaks_array,complete_cycles)
constriction_frames_first=nan(1,numel(peaks_array));
constriction_frames_cont=nan(1,numel(peaks_array));
constriction_frames_relative_duration=[];
for i=1:numel(peaks_array)
    temp1=peaks_array{1,i};
    j=numel(temp1);
    while ~isempty(temp1{1,j})
        j=j-1;
        if j==0;
            break;
        end
    end
    if j>=numel(temp1) % || j==0
        constriction_frames_cont(i)=nan;
    else
        constriction_frames_cont(i)=j+1;
    end
    k=1;
    while isempty(temp1{1,k})
        k=k+1;
        if k==numel(temp1)
            break
        end
    end
    if k==numel(temp1)
        constriction_frames_first(i)=nan;
    else
        constriction_frames_first(i)=k;
    end
    
    constriction_frames_relative_duration(i)=constriction_frames_cont(i)./complete_cycles(i).duration;
end

end


