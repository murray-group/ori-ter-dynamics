%% Nucleoid modulation (Ratio of depth of constriction over height of maximum signal)

% In the data structure:

% Row 1 - Relative cell age
% Row 2 - Nucleoid modulation for that particular cell for that particular frame; zero if no constriction is detected
% Row 3 - Nucleoid length for that particular frame
% Row 4 - Cell length for that particular frame
 

function [modulation]=nucleoid_modulation(cycle_list,complete_cycle_ids,peaks_array)
[nucleoid_lengths,~]=nucleoid_length(cycle_list,complete_cycle_ids);
for ccc=1:numel(complete_cycle_ids) %Note: the variable ccc stands for Completed Cell Cycles
    cycle_num=complete_cycle_ids(ccc);
    for frame=1:cycle_list(cycle_num).duration
        ys=(((cycle_list(cycle_num).fluor1_lp{1,frame})./max(cycle_list(cycle_num).fluor1_lp{1,frame})));
        if ~isempty(peaks_array{ccc}{frame})
            
            dp=peaks_array{ccc}{frame}(4);
        else
            dp=[];
        end
        
        if isempty(dp)
            modulation{ccc}(1,frame)=frame./cycle_list(cycle_num).duration;
            modulation{ccc}(2,frame)=0;
            modulation{ccc}(3,frame)=nucleoid_lengths{ccc}(frame);
            modulation{ccc}(4,frame)=cycle_list(cycle_num).lengths(frame);
            
        else
            modulation{ccc}(1,frame)=frame./cycle_list(cycle_num).duration;
            modulation{ccc}(2,frame)=dp./(max(ys)-min(ys));
            modulation{ccc}(3,frame)=nucleoid_lengths{ccc}(frame);
            modulation{ccc}(4,frame)=cycle_list(cycle_num).lengths(frame);
            
        end
    end
end