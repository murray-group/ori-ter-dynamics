complete_cycles=cycle_list(matp_pole_ids);
nbins_x=26;
nbins_y=100;
bin_edges_x=linspace(-0.01,1.01,nbins_x+1);
bin_edges_y=linspace(-0.51,0.51,nbins_y+1);
data=zeros(nbins_x,nbins_y);
ndata=zeros(1,nbins_y);

for i=1:numel(complete_cycles)
    norm_duration=linspace(0,1,complete_cycles(i).duration);
    [~,~,bins_x]=histcounts(norm_duration,bin_edges_x);
    if (complete_cycles(i).pole==1)
        [~,~,bins_y]=histcounts(complete_cycles(i).foci2.foci_position_major,bin_edges_y);
    elseif(complete_cycles(i).pole==-1)
        [~,~,bins_y]=histcounts(-complete_cycles(i).foci2.foci_position_major,bin_edges_y);
    end
    
    
    for k=1:size(bins_y,2)
        for j=1:complete_cycles(i).duration
            if ~isnan(complete_cycles(i).foci2.foci_position_major(j,k)) &&...
                      abs(complete_cycles(i).foci2.foci_position_major(j,k))<0.51
                data(bins_x(j),bins_y(j,k))=data(bins_x(j),bins_y(j,k))+1;
                %ndata(bins_x(j),bins_y(j))=ndata(bins_x(j),bins_y(j))+1;
            end
        end
    end
end
ndata=sum(data,2);
data=data./ndata;
figure
imagesc(linspace(0,1,nbins_x),linspace(-0.51,0.51,nbins_y),data')
xlabel("Relative cell age")
ylabel("Relative position inside cell")