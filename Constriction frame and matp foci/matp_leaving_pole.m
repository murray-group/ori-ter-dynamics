%% First frame in which MatP foci comes to middle

% In the cycle_list data structure, foci position is represented between as
% absolute length from one pole

% To define a region as middle, a cell is split into 3 parts.

% In the next step query each cell cycle to identify the first frame in
% which the MatP foci comes to the middle 1/3rd part

% Make a logical array that tells in each frame whether the matp foci is in
% the middle section or not

%[first_frames_nonpole,cell_age_matp_at_middle]=matp_at_midcell_stable(complete_cycles,duration_complete_cycles,a,b)
matp_foci=struct([]);
for i=1:numel(complete_cycles)
    j=1;
    matp_foci(i).cycle_id=complete_cycles(i).cycle_id;
    temp3=complete_cycles(i).foci2.foci_position_major(:,1); % generates foci position relative to mid-cell
    matp_foci(i).pole_loc=abs(temp3)>=0.25; % Creates a logical array for when MatP is at poles
    while matp_foci(i).pole_loc(j)==1 || isnan(temp3(j))
        j=j+1;
        if j==numel(matp_foci(i).pole_loc)+1 % Fail safe if no foci has been detected in the cell cycle
            j=nan;
            break
        end
    end
    matp_foci(i).first_frame_nonpole=j;
end
first_frames_nonpole=[];
cell_age_matp_at_nonpole=[];
for i=1:numel(matp_foci)
    first_frames_nonpole(i)=matp_foci(i).first_frame_nonpole;
    cell_age_matp_at_nonpole(i)=matp_foci(i).first_frame_nonpole./duration_complete_cycles(i);
end

