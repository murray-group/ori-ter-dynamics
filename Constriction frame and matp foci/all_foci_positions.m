function all_foci_pos=all_foci_positions(cycle_list,cycle_ids,fluor_channel,sz)

% sz is used to specify cell length restrictions
all_foci_pos=[];
fc=['foci',num2str(fluor_channel)];
for i=cycle_ids
    
    if isfield(cycle_list(i).(fc),'position_relative') &&...
            ~isempty(cycle_list(i).(fc).position_relative)
       
        pos=cycle_list(i).(fc).position_relative(:,:,1);%.*cycle_list(i).pole;
               
        if length(sz)==1 && sz>0
            pos=pos(cycle_list(i).lengths>=sz);
            pos=pos(:);
            all_foci_pos=[all_foci_pos;pos(~isnan(pos))];
        elseif length(sz)==2
            pos=pos(cycle_list(i).lengths>=sz(1) & cycle_list(i).lengths<=sz(2));
            pos=pos(:);
            all_foci_pos=[all_foci_pos;pos(~isnan(pos))];
        else
            pos=pos(:);
            all_foci_pos=[all_foci_pos;pos(~isnan(pos))];
        end
    end
end