function [nucleoid_lengths,all_nucleoid_lengths]=nucleoid_length(cycle_list,complete_cycle_ids)
z=1;
for ccc=1:numel(complete_cycle_ids) % Note: the variable ccc stands for Completed Cell Cycles
    cycle_num=complete_cycle_ids(ccc);
    for frame=1:numel(cycle_list(cycle_num).fluor1_lp)
        lp_normalized{ccc}{frame}=(cycle_list(cycle_num).fluor1_lp{1,frame})./max(cycle_list(cycle_num).fluor1_lp{1,frame}); % Normalization of line profiles to maximum value
        lp_to_nucleoid_length{ccc}{frame}=lp_normalized{ccc}{frame}>=0.5; % Query for indices that has a (normalized) value greater than 0.5
        j=1;
        while lp_to_nucleoid_length{ccc}{frame}(j)==0 % To identify the point along the long axis from the start (left side) in which the nucleoid signal rise above 0.5 of max
            j=j+1;
        end
        k=numel(lp_normalized{ccc}{frame});
        while lp_to_nucleoid_length{ccc}{frame}(k)==0 % To identify the point along the long axis from the end (right side) in which the nucleoid signal is above 0.5 of max
            k=k-1;
        end
        nucleoid_lengths{ccc}(frame)=k-j+1;
        all_nucleoid_lengths(z)=k-j+1;
        all_cell_lengths(z)=cycle_list(cycle_num).lengths(frame);
        z=z+1;
    end
end