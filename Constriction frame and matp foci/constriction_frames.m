function [constriction_frames_first,constriction_frames_cont]=constriction_frames(peaks_array)

constriction_frames_first=nan(1,numel(peaks_array));
constriction_frames_cont=nan(1,numel(peaks_array));
for i=1:numel(peaks_array)
    temp1=peaks_array{1,i};
    j=numel(temp1);
    while ~isempty(temp1{1,j})
        j=j-1;
        if j==0;
            break;
        end
    end
    if j>=numel(temp1) % || j==0
        constriction_frames_cont(i)=nan;
    else
        constriction_frames_cont(i)=j+1;
    end
    k=1;
    while isempty(temp1{1,k})
        k=k+1;
        if k==numel(temp1)
            break
        end
    end
    if k==numel(temp1)
        constriction_frames_first(i)=nan;
    else
        constriction_frames_first(i)=k;
    end
end
end