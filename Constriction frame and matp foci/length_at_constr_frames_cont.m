%% Generate a cell array containing frames at which continuous constriction starts

function [nuc_len_at_cont_constr,cell_len_at_cont_constr]=length_at_constr_frames_cont(cycle_list,complete_cycle_ids,peaks_array)
nucleoid_lengths=nucleoid_length(cycle_list,complete_cycle_ids);
[~,constriction_frames_cont]=constriction_frames(peaks_array);
for ccc=1:numel(complete_cycle_ids) % Note: the variable ccc stands for Completed Cell Cycles
    cycle_num=complete_cycle_ids(ccc);
    if ~isnan(constriction_frames_cont(ccc))
        nuc_len_at_cont_constr(ccc)=nucleoid_lengths{ccc}(constriction_frames_cont(ccc));
        cell_len_at_cont_constr(ccc)=cycle_list(cycle_num).lengths(constriction_frames_cont(ccc));
    else
        cell_len_at_cont_constr(ccc)=nan;
        nuc_len_at_cont_constr(ccc)=nan;
    end
end