%% Produces an array of all the cell lengths
function all_cell_lengths=cell_lengths(cycle_list,complete_cycle_ids)
all_cell_lengths=[];
for cycle=complete_cycle_ids
    
    
        all_cell_lengths=[all_cell_lengths;cycle_list(cycle).lengths];
      
   
end
end