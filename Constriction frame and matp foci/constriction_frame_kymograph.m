   
%%
nbins_x=22;
nbins_y=50;
bin_edges_x=linspace(-0.01,1.01,nbins_x+1);
bin_edges_y=linspace(-0.01,1.01,nbins_y+1);
data=zeros(nbins_x,nbins_y);
ndata=ones(nbins_x,nbins_y);
for i=1:numel(peaks_array)
    norm_duration=linspace(0,1,size(peaks_array{i},2));
    [~,~,bins_x]=histcounts(norm_duration,bin_edges_x);
    
    for j=1:size(peaks_array{i},2)
        if ~isempty(peaks_array{i}{j})
            [~,~,bins_y]=histcounts(peaks_array{i}{j}(2),bin_edges_y);
            data(bins_x(j),bins_y)=data(bins_x(j),bins_y)+1;%complete_cycles(i).foci2.intensity(i,1);
            %ndata(bins_x(j),bins_y)=ndata(bins_x(j),bins_y)+1;
        end
    end
end
 ndata=sum(data,2);
 data=data./ndata;
imagesc(linspace(0,1,nbins_x),linspace(-0.5,0.5,nbins_y),data')
xlabel("Relative cell age")
ylabel("Position inside cell")
