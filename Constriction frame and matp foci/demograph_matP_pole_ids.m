c=cycle_list(complete_cycle_ids); % Import details of those cell cycles that are identified as complete cycles
fluor_index=3;
min_len=20;
max_len=50;
n_bins=max_len-min_len+1;
data=zeros(n_bins,max_len);
n_data=zeros(1,n_bins);
bin_edges=min_len:1:max_len+1;
lp=strcat("fluor",num2str(fluor_index),"_lp");
for i=1:numel(c)
    if(c(i).lengths>=20 & c(i).lengths<=50)
        [~,~,bins]=histcounts(c(i).lengths,bin_edges);
        for j=1:c(i).duration
            lp_length=numel(c(i).(lp){j});
            lp_offset=round((max_len-lp_length)/2)+1;
            if (c(i).pole == 1) % preferred pole
                data(bins(j),lp_offset:(lp_offset+lp_length-1))=data(bins(j),lp_offset:(lp_offset+lp_length-1))+c(i).(lp){j}';
            elseif (c(i).pole == -1) % if pole is -1, then flip the line profile elements to match the pole as +1
                data(bins(j),lp_offset:(lp_offset+lp_length-1))=data(bins(j),lp_offset:(lp_offset+lp_length-1))+flip(c(i).(lp){j}');
            end
            n_data(bins(j))=n_data(bins(j))+1;
        end
    end
end
data=data./n_data';
figure
imagesc(data)