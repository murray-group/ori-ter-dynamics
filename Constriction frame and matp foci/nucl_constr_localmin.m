
function [peaks_array]=nucl_constr_localmin(cycle_list,complete_cycle_ids)

peaks_array=[];
for ccc=1:numel(complete_cycle_ids)%Note: the variable ccc stands for Completed Cell Cycles
    cycle_num=(complete_cycle_ids(ccc));
    for frame=1:numel(cycle_list(cycle_num).fluor1_lp)
        ys=smooth(((cycle_list(cycle_num).fluor1_lp{1,frame})./max(cycle_list(cycle_num).fluor1_lp{1,frame})));
        xdata=linspace(0,1,numel(cycle_list(cycle_num).fluor1_lp{1,frame}));
        [l,r]=mid_cell(ys,xdata);
        loc_min=logical(zeros(numel(ys),1));
        loc_min(l:r)=islocalmin(ys(l:r),'MinSeparation',5,'MaxNumExtrema',1);
       
        
        peaks_array{ccc}{frame}=[xdata(loc_min)' ys(loc_min)];
%                figure (5)
%             subplot(5,4,frame);
%             plot(xdata,ys);
%             hold on
%             plot(xdata(loc_min)', ys(loc_min),'o')
%             hold off
     end
%         pause(5)
%        close figure 5
end
end

% function to query for constriction only near the center of the nucleoid
% signal
function [l,r]=mid_cell(ys,xdata)
fwhm_lp=(xdata>=0.25 & xdata<=0.75);
l=1;
while fwhm_lp(l)==0
    l=l+1;
end
r=numel(fwhm_lp);
while fwhm_lp(r)==0
    r=r-1;
end
end