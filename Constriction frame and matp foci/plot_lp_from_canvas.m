for i=1:size(canvas,2)
    figure (9)
    subplot(4,4,i)
    plot(linspace(-0.5,0.5,numel(canvas(:,i))),canvas(:,i)./max(canvas(:,i)),'Color',	'#EDB120')
    hold on
    plot(linspace(-0.5,0.5,numel(canvas2(:,i))),canvas2(:,i)./max(canvas2(:,i)),'Color','r')
%     plot(linspace(-0.5,0.5,numel(canvas(:,i))),canvas(:,i))
%     xlim([-0.5 0.5])
    hold off
end

%%
width=fwhm(linspace(-0.5,0.5,100),canvas(:,16));

%%
lp=cycle8_lp;
for i=2:size(lp,2)
    figure(9)
    subplot(5,4,i-1)
    plot(linspace(-0.5,0.5,size(lp,1)),lp(:,i)./max(lp(:,i)),'Color','r');
end

%%
lp=cycle8_lp;
for i=2:size(lp,2) %Note: the variable ccc stands for Completed Cell Cycles
    
        ys=smooth(lp(:,i)./max(lp(:,i)));
        xdata=linspace(0,1,size(lp,1));
        loc_min=logical(zeros(numel(ys),1));
        loc_min=islocalmin(ys,'MinSeparation',5);
        
        
        figure (5)
                    subplot(5,4,i-1);
                    plot(xdata,ys);
                    hold on
                    plot(xdata(loc_min)', ys(loc_min),'o')
                    hold off
    
    %         pause(5)
    %        close figure 5
end
%% functions

function width = fwhm(x,y)

% function width = fwhm(x,y)
%
% Full-Width at Half-Maximum (FWHM) of the waveform y(x)
% and its polarity.
% The FWHM result in 'width' will be in units of 'x'
%
%
% Rev 1.2, April 2006 (Patrick Egan)


y = y / max(y);
N = length(y);
lev50 = 0.5;
if y(1) < lev50                  % find index of center (max or min) of pulse
    [garbage,centerindex]=max(y);
    Pol = +1;
    disp('Pulse Polarity = Positive')
else
    [garbage,centerindex]=min(y);
    Pol = -1;
    disp('Pulse Polarity = Negative')
end
i = 2;
while sign(y(i)-lev50) == sign(y(i-1)-lev50)
    i = i+1;
end                                   %first crossing is between v(i-1) & v(i)
interp = (lev50-y(i-1)) / (y(i)-y(i-1));
tlead = x(i-1) + interp*(x(i)-x(i-1));
i = centerindex+1;                    %start search for next crossing at center
while ((sign(y(i)-lev50) == sign(y(i-1)-lev50)) & (i <= N-1))
    i = i+1;
end
if i ~= N
    Ptype = 1;  
    disp('Pulse is Impulse or Rectangular with 2 edges')
    interp = (lev50-y(i-1)) / (y(i)-y(i-1));
    ttrail = x(i-1) + interp*(x(i)-x(i-1));
    width = ttrail - tlead;
else
    Ptype = 2; 
    disp('Step-Like Pulse, no second edge')
    ttrail = NaN;
    width = NaN;
end
end