function I=get_last_time_at_middle(cycle,fluor_channel,w)
pos=cycle.(['foci',num2str(fluor_channel)]).position_relative(:,:,1)./cycle.lengths;%change to relative positions (position_relative is just relative to mid-cell)
c=any(abs(pos)<w/2,2);% is any spot one each in the middle wth of the cell
I=find(c,1,'last');

if any(sum(~isnan(pos),2)==2) || isempty(I)
    
    I=nan;
end

end