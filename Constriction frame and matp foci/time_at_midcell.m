function midcell_time=time_at_midcell(cycle_list,cycle_ids,fluor_channel)

midcell_time=[];
for i=cycle_ids
    i
    if isfield(cycle_list(i).(['foci',num2str(fluor_channel)]),'position_relative') &&...
            ~isempty(cycle_list(i).(['foci',num2str(fluor_channel)]).position_relative)
        pos=any(abs(cycle_list(i).(['foci',num2str(fluor_channel)]).position_relative(:,:,1))<=3.6,2);
        nan_check=diff(cycle_list(i).(['foci',num2str(fluor_channel)]).position_relative(:,:,1));
        diff_pos=diff(pos);
        diff_pos(sum(~isnan(nan_check),2)==0)=nan;
        midcell_time=[midcell_time;sum(diff_pos==0)];
    end
end

end