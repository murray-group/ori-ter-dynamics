function I=get_last_time_to_middle(cycle,fluor_channel,w)
        pos=cycle.(['foci',num2str(fluor_channel)]).position_relative(:,:,1)./cycle.major;%change to relative positions (position_relative is just relative to mid-cell)
        c=any(abs(pos)>w/2,2);% is any spot one each in the middle wth of cell
        I=find(c,1,'last')+1;%the first
        if (I>cycle.duration)
            I=nan;
        elseif isempty(I)
            I=nan;
        end
end

