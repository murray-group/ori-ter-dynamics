%% Histogram for all matp absolute relative foci positions - population average

all_relative_foci_pos=all_foci_positions_relative(cycle_list,one_ori_cycle_ids',1);

figure
histogram(all_relative_foci_pos,'Normalization','pdf');
xlabel("Relative position inside cell (midcell - pole)");
ylabel("Density");
xlim([-0.005 0.55]);
title("Relative foci positions - population average")
%% Histogram for all matp absolute relative foci positions until ori split
[all_relative_pos_until_I,~]=all_foci_positions_relative2(cycle_list,complete_cycle_ids,1,@get_time_first_1_to_2_split,2);

figure
histogram(all_relative_pos_until_I,'Normalization','pdf');
xlabel("Relative position inside cell (midcell - pole)");
ylabel("Density");
xlim([-0.005 0.55]);
title("Relative foci positions until Ori foci duplication")

%% Histogram for all matp absolute relative foci positions from ori split

[~,all_relative_pos_from_I]=all_foci_positions_relative2(cycle_list,complete_cycle_ids,1,@get_time_first_1_to_2_split,2);

figure
histogram(all_relative_pos_from_I,'Normalization','pdf');
xlabel("Relative position inside cell (midcell - pole)");
ylabel("Density");
xlim([-0.005 0.55]);
%ylim([0 21]);
title("Relative foci positions post Ori foci duplication")


%% Histogram for all matp absolute relative foci positions from matp relocalization

[~,all_relative_pos_from_I2]=all_foci_positions_relative2(cycle_list,ids_with_high_split_age,1,@get_stable_time_at_middle,1,3.6);

figure
histogram(all_relative_pos_from_I2,'Normalization','pdf');
xlabel("Relative position inside cell (midcell - pole)");
ylabel("Density");
xlim([-0.005 0.55]);
%ylim([0 21]);
title("Relative foci positions post MatP relocalization")

%% Histogram for all matp absolute relative foci positions from matp relocalization

[all_relative_pos_until_I2,~]=all_foci_positions_relative2(cycle_list,ids_with_high_split_age,1,@get_stable_time_at_middle,1,3.6);

figure
histogram(all_relative_pos_until_I2,'Normalization','pdf');
xlabel("Relative position inside cell (midcell - pole)");
ylabel("Density");
xlim([-0.005 0.55]);
%ylim([0 21]);
title("Relative foci positions before MatP relocalization")