subplot (2,4,1)
matp_ori_distance_sync(cycle_list,complete_cycle_ids',2,@get_time_first_1_to_2_split,1);
title("All cycles")

subplot (2,4,2)
matp_ori_distance_sync(cycle_list,matp_pole',2,@get_time_first_1_to_2_split,1);
title("MatP at poles")

subplot (2,4,3)
matp_ori_distance_sync(cycle_list,matp_mid',2,@get_time_first_1_to_2_split,1);
title("MatP at mid-cell")

subplot (2,4,4)
matp_ori_distance_sync(cycle_list,matp_mixd',2,@get_time_first_1_to_2_split,1);
title("MatP at mid-cell & pole")

%% Relative distance subplots - Execute after manually changing the rel -> 1 in ori_matp_distance function
subplot (2,4,5)
matp_ori_distance_sync(cycle_list,complete_cycle_ids',2,@get_time_first_1_to_2_split,1);
title("All cycles")

subplot (2,4,6)
matp_ori_distance_sync(cycle_list,matp_pole',2,@get_time_first_1_to_2_split,1);
title("MatP at poles")

subplot (2,4,7)
matp_ori_distance_sync(cycle_list,matp_mid',2,@get_time_first_1_to_2_split,1);
title("MatP at mid-cell")

subplot (2,4,8)
matp_ori_distance_sync(cycle_list,matp_mixd',2,@get_time_first_1_to_2_split,1);
title("MatP at mid-cell & pole")