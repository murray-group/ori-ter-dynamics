%% Script to generate histogram of number of frames where MatP and Ori appear together at mid-cell

subplot(2,4,1)
[ori_matp_midcell,~]=ori_matp_midcell_coloc(cycle_list,complete_cycle_ids);
histogram(cellfun(@nansum, ori_matp_midcell))
xlabel("No. of frames")
title("All cells")

subplot(2,4,2)
[ori_matp_midcell,~]=ori_matp_midcell_coloc(cycle_list,matp_pole);
histogram(cellfun(@nansum, ori_matp_midcell))
xlabel("No. of frames")
title("MatP at pole")

subplot(2,4,3)
[ori_matp_midcell,~]=ori_matp_midcell_coloc(cycle_list,matp_mid);
histogram(cellfun(@nansum, ori_matp_midcell))
xlabel("No. of frames")
title("MatP at mid-cell")

subplot(2,4,4)
[ori_matp_midcell,~]=ori_matp_midcell_coloc(cycle_list,matp_mixd);
histogram(cellfun(@nansum, ori_matp_midcell))
xlabel("No. of frames")
title("MatP at mid-cell & pole")

subplot(2,4,5)
[ori_matp_midcell,cyc_duration]=ori_matp_midcell_coloc(cycle_list,complete_cycle_ids);
histogram(cellfun(@nansum, ori_matp_midcell)./cyc_duration)
xlabel("Normalized duration")
title("All cells")

subplot(2,4,6)
[ori_matp_midcell,cyc_duration]=ori_matp_midcell_coloc(cycle_list,matp_pole);
histogram(cellfun(@nansum, ori_matp_midcell)./cyc_duration)
xlabel("Normalized duration")
title("MatP at pole")

subplot(2,4,7)
[ori_matp_midcell,cyc_duration]=ori_matp_midcell_coloc(cycle_list,matp_mid);
histogram(cellfun(@nansum, ori_matp_midcell)./cyc_duration)
xlabel("Normalized duration")
title("MatP at mid-cell")

subplot(2,4,8)
[ori_matp_midcell,cyc_duration]=ori_matp_midcell_coloc(cycle_list,matp_mixd);
histogram(cellfun(@nansum, ori_matp_midcell)./cyc_duration)
xlabel("Normalized duration")
title("MatP at mid-cell")