%% Script to generate histograms for consecutive time that MatP spend at mid-cell in different populations

subplot(2,4,1)
[matp_middle_durations,~]=find_matp_consecutive_time_middle(cycle_list,complete_cycle_ids,1);
histogram(matp_middle_durations)
xlabel("Time (min)")
title("All cells")

subplot(2,4,2)
[matp_middle_durations,~]=find_matp_consecutive_time_middle(cycle_list,matp_pole,1);
histogram(matp_middle_durations)
xlabel("Time (min)")
title("MatP at pole")

subplot(2,4,3)
[matp_middle_durations,~]=find_matp_consecutive_time_middle(cycle_list,matp_mid,1);
histogram(matp_middle_durations)
xlabel("Time (min)")
title("MatP at mid-cell")

subplot(2,4,4)
[matp_middle_durations,~]=find_matp_consecutive_time_middle(cycle_list,matp_pole,1);
histogram(matp_middle_durations)
xlabel("Time (min)")
title("MatP at pole & mid-cell")

subplot(2,4,5)
[~,matp_middle_durations]=find_matp_consecutive_time_middle(cycle_list,complete_cycle_ids,1);
histogram(matp_middle_durations)
xlabel("Normalized duration")
title("All cells")

subplot(2,4,6)
[~,matp_middle_durations]=find_matp_consecutive_time_middle(cycle_list,matp_pole,1);
histogram(matp_middle_durations)
xlabel("Normalized duration")
title("MatP at pole")

subplot(2,4,7)
[~,matp_middle_durations]=find_matp_consecutive_time_middle(cycle_list,matp_mid,1);
histogram(matp_middle_durations)
xlabel("Normalized duration")
title("MatP at mid-cell")

subplot(2,4,8)
[~,matp_middle_durations]=find_matp_consecutive_time_middle(cycle_list,matp_pole,1);
histogram(matp_middle_durations)
xlabel("Normalized duration")
title("MatP at pole & mid-cell")

sgtitle("Consecutive time periods MatP spend at mid-cell")