subplot (2,4,1)
foci_kymograph_cell_age(cycle_list,complete_cycle_ids,100,24,1)
colormap(viridis(500))
title("MatP")

subplot (2,4,5)
foci_kymograph_cell_age(cycle_list,complete_cycle_ids,100,24,2);
colormap(viridis(500))
title("Ori")

subplot (2,4,2)
synched_kymograph(cycle_list,matp_pole,1,@get_time_first_1_to_2_split,1);
colormap(viridis(500))
title("MatP at pole - MatP")

subplot (2,4,3)
synched_kymograph(cycle_list,matp_mid,1,@get_time_first_1_to_2_split,1);
colormap(viridis(500))
title("MatP at mid-cell - MatP")

subplot (2,4,4)
synched_kymograph(cycle_list,matp_mixd,1,@get_time_first_1_to_2_split,1);
colormap(viridis(500))
title("MatP at pole and mid-cell - MatP")

subplot (2,4,6)
synched_kymograph(cycle_list,matp_pole,2,@get_time_first_1_to_2_split,1);
colormap(viridis(500))
title("MatP at pole - Ori")

subplot (2,4,7)
synched_kymograph(cycle_list,matp_mid,2,@get_time_first_1_to_2_split,1);
colormap(viridis(500))
title("MatP at mid-cell - Ori")

subplot (2,4,8)
synched_kymograph(cycle_list,matp_mixd,2,@get_time_first_1_to_2_split,1);
colormap(viridis(500))
title("MatP at pole and mid-cell - Ori")
