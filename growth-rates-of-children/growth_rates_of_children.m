%% Script to find growth rate of daughter cells in an attempt to find anucleate daughter cells

k=1;
growth_rates=[];
for i=1:numel(complete_cycle_ids)
    i
    cycle=cycle_list(complete_cycle_ids(i));
    children=cycle.relation_graph.children;
    for j=children
        if cycle_list(j).duration>=4
            areas = log(cycle.areas);
            mdl = fitlm(1:numel(areas),areas);
            growth_rate = mdl.Coefficients{2,1};
            growth_rates = [growth_rates; [j growth_rate]];
            k=k+1;
        end
    end
end
            