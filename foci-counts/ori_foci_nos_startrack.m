function [ori_foci_at_birth,ori_foci_at_division]=ori_foci_nos_startrack(cycle_list,complete_cycle_ids,fluor_channel)

fc=strcat("foci",string(fluor_channel));

for i=1:numel(complete_cycle_ids)
    cycle=cycle_list(complete_cycle_ids(i));
    if ~isempty(cycle.(fc).position)
    ori_foci_at_birth(i)=sum(cycle.(fc).start_and_stop(:,1)==1);
    ori_foci_at_division(i)=size(cycle.(fc).start_and_stop,1);
    else
    ori_foci_at_birth(i)=nan;
    ori_foci_at_division(i)=nan;
end
end
