cycle_ids=complete_cycle_ids;
fc='foci2';
missed_foci_nos=nan(1,length(cycle_ids));
missed_foci_nos_percentage=nan(1,length(cycle_ids));
missed_foci_nos_after_duplication=nan(1,length(cycle_ids));
missed_foci_nos_after_duplication_percentage=nan(1,length(cycle_ids));
good_cycles_id=[];
sumnan=[];
tracks=[];
for i=1:length(cycle_ids)
    %i
    cycle=cycle_list(cycle_ids(i));
    if ~isempty(cycle.(fc).inferred_position_relative)
        pos=cycle.(fc).inferred_position_relative(:,:,1);
        sumnan=0;
        tracks=cycle.(fc).start_and_stop;
        for j=1:size(tracks,1)
            sumnan=sumnan+sum(isnan(pos(tracks(j,1):tracks(j,2),j)));
        end
        missed_foci_nos(i)=sumnan;
        total_foci=sum(diff(tracks,[],2)+1);
        missed_foci_nos_percentage(i)=missed_foci_nos(i)./total_foci;
        %             missed_foci_nos_after_duplication(i)=sum(isnan(cycle.(fc).inferred_position_relative(cycle.(fc).start_and_stop(2,1):end,1,1)))...
        %                 +sum(isnan(cycle.(fc).inferred_position_relative(cycle.(fc).start_and_stop(2,1):end,2,1)));
        %             missed_foci_nos_after_duplication_percentage(i)=missed_foci_nos_after_duplication(i)./numel(cycle.(fc).start_and_stop(2,1):cycle.duration);
        if  missed_foci_nos_percentage(i)<0.2
            good_cycles_id=[good_cycles_id cycle.cycle_id];
        end
    end
end
