figure
histogram(missed_foci_nos_percentage_WT,(-0.0:0.05:1.0),'Normalization','pdf')
hold on
histogram(missed_foci_nos_percentage_C20,(-0.0:0.05:1.0),'Normalization','pdf')
histogram(missed_foci_nos_percentage_ZapB,(-0.0:0.05:1.0),'Normalization','pdf')
hold off
xlabel("Fraction of false negative ori foci")
ylabel("PDF")
title("Fraction of missed ori foci")

%%

figure
histogram(missed_foci_nos_percentage_WT,(-0.0:0.05:1.0),'Normalization','pdf', 'FaceColor', '#1f77b4')
hold on
histogram(missed_foci_nos_percentage_C20,(-0.0:0.05:1.0),'Normalization','pdf', 'FaceColor', '#ff7f0e')
histogram(missed_foci_nos_percentage_ZapB,(-0.0:0.05:1.0),'Normalization','pdf', 'FaceColor', '#2ca02c')
hold off
xlabel("Fraction of false negative ori foci")
ylabel("PDF")
title("Fraction of missed ori foci")
%%

figure
histogram(missed_foci_nos_percentage_WT,(-0.0:0.05:1.0),'Normalization','pdf', 'FaceColor', 'b') % MATLAB blue
hold on
histogram(missed_foci_nos_percentage_C20,(-0.0:0.05:1.0),'Normalization','pdf', 'FaceColor', [1, 0.5, 0]) % Orange
histogram(missed_foci_nos_percentage_ZapB,(-0.0:0.05:1.0),'Normalization','pdf', 'FaceColor', 'y') % Yellow
hold off
xlabel("Fraction of false negative ori foci")
ylabel("PDF")
title("Fraction of missed ori foci")

%% Line plot

% Define the bin edges
binEdges = (-0.0:0.05:1.0);

figure
hold on

% Calculating and plotting line for WT
[counts1, ~] = histcounts(missed_foci_nos_percentage_WT, binEdges, 'Normalization', 'pdf');
midpoints1 = binEdges(1:end-1) + diff(binEdges)/2;
plot(midpoints1, counts1);

% Calculating and plotting line for C20
[counts2, ~] = histcounts(missed_foci_nos_percentage_C20, binEdges, 'Normalization', 'pdf');
midpoints2 = binEdges(1:end-1) + diff(binEdges)/2;
plot(midpoints2, counts2);

% Calculating and plotting line for ZapB
[counts3, ~] = histcounts(missed_foci_nos_percentage_ZapB, binEdges, 'Normalization', 'pdf');
midpoints3 = binEdges(1:end-1) + diff(binEdges)/2;
plot(midpoints3, counts3);

hold off
xlabel("Fraction of false negative ori foci")
ylabel("PDF")
title("Fraction of missed ori foci")