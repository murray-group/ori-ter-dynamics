function shadederror_std_sem(x,d,std,sem)

xx=[x,fliplr(x)];
ss1=[d+std ,fliplr(d-std)];
ss2=[d+sem,fliplr(d-sem)];
xx=xx(~isnan(ss1));
ss1=ss1(~isnan(ss1));
ss2=ss2(~isnan(ss2));
hold on
h=plot(x,d);
color=get(h,'Color');
patch(xx, ss1, color,'linestyle', 'none','FaceColor','b','FaceAlpha','0.2');
patch(xx,ss2,color,'linestyle', 'none','FaceColor','b','FaceAlpha','0.7');
hold off
box on;
end