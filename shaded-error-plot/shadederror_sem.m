function shadederror_sem(x,d,bottombar,topbar,sem,name)

xx=[x,fliplr(x)];
ss=[topbar ,fliplr(bottombar)];
xx=xx(~isnan(ss));
ss=ss(~isnan(ss));
hold on
h=errorbar(x,d,sem,'-k','DisplayName',name);
color=get(h,'Color');
patch(xx, ss, color,'linestyle', 'none','FaceColor','b','FaceAlpha','0.2');
hold off
box on;
end