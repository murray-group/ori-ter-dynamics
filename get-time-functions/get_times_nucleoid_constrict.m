function a=get_times_nucleoid_constrict(cycle_list,cycle_ids,fluor_channel,depth_thresholding)
fc=['fluor',num2str(fluor_channel),'_lp'];
a=NaN(size(cycle_ids));
fieldsToKeep = {'cycle_id', 'duration','pole',fc};
cycle_list=rmfield(cycle_list, setdiff(fieldnames(cycle_list), fieldsToKeep)); % To reduce ram usage when cycle_list is broadcasted in a parfor loop
parfor i=1:numel(cycle_ids)
    %i
    cycle=cycle_list(cycle_ids(i));
    constriction=cell(1,cycle.duration);
    line_profiles=cycle.(fc);
    for frame=1:cycle.duration
        ys=((line_profiles{1,frame})./max(line_profiles{1,frame}));
        xdata=linspace(0,1,numel(line_profiles{1,frame}));
        constriction{1,frame}=find_valleys(ys,xdata,depth_thresholding);
    end
    
    j=size(constriction,2);
    while ~isempty(constriction{1,j})
        j=j-1;
        if j==0
            break;
        end
    end
    if j~=0 && j~=size(constriction,2)
    a(i)=(j+1);
    end
end
end

%%

function [height,loc,w,depth]=find_valleys(ys,x,depth_thresholding)

valleys=watershed(ys);%breaks the profile up into valleys
%

[height,loc,left,right,depth]=measure_valleys(valleys,ys);


%
%find the deepest valley


while length(depth)>1
    [min_d,i]=min(depth);
    d=min_d+0.000001;
    
    
    if((ys(left(i))-height(i))<d)%first check if left side shallow
        valleys(left(i))=valleys(left(i)-1);
        temp=valleys(left(i)+1:end);
        temp(temp>0)=temp(temp>0)-1;
        valleys(left(i)+1:end)=temp;
        
    else %if not, check if right side is shallow
        valleys(right(i))=valleys(right(i)-1);
        temp=valleys(right(i)+1:end);
        temp(temp>0)=temp(temp>0)-1;
        valleys(right(i)+1:end)=temp;
    end
    [height,loc,left,right,depth]=measure_valleys(valleys,ys);
end

w=right-left;

if ~isempty(x)
    loc=x(loc);
    w=x(right)-x(left);
end

% Define a depth threshold

if (depth_thresholding ~= 0)
    if depth/(max(ys)-min(ys))<depth_thresholding
        height=[];
        loc=[];
        w=[];
        depth=[];
    end
end
end



function [height,loc,left,right,depth]=measure_valleys(valleys,ys)
n_valleys=max(valleys);
if n_valleys<=2
    height=[];
    loc=[];
    left=[];
    right=[];
    depth=[];
    return;
end
for i=2:(n_valleys-1) %ignore valleys at the boundary
    [height(i-1),I]=min(ys(valleys==i));
    loc(i-1)=I+find(valleys==i,1)-1;%index of minimum
    left(i-1)=find(valleys==i,1)-1;%index of left edge
    right(i-1)=find(valleys==i,1,'last')+1;%index of right edge
    depth(i-1)=min(ys(left(i-1)),ys(right(i-1)))-height(i-1);%depth
end


end