function I=get_frame_diff_stable_and_first(cycle,fluor_channel,w)
if isfield(cycle.(['foci',num2str(fluor_channel)]),'position_relative') && ~isempty(cycle.(['foci',num2str(fluor_channel)]).position_relative)
    pos=cycle.(['foci',num2str(fluor_channel)]).inferred_position_relative(:,:,1);
    c=any(abs(pos)<w,2);% is any spot in the middle wth of cell in each frame
    temp1=[];
    for i=1:length(c)-2
        temp1(i)=sum(c(i:i+2)); %creates a temporary variable with  the sum of logical values for 3 indices from c
    end
    I_stable=find(temp1==3,1);%the first time when 3 consecutive frames have matp at middle wth
    if (I_stable>cycle.duration)
        I_stable=nan;
    elseif isempty(I_stable) || I_stable==1
        I_stable=nan;
    end
    
    I_first=find(c==1,1);%the first time matp at middle wth
    if (I_first>cycle.duration)
        I_first=nan;
    elseif isempty(I_first)
        I_first=nan;
    end
    
    temp2=I_stable-I_first;
    if temp2~=0 && ~isnan (temp2)
        d=abs(pos((I_first+1):(I_stable-1),:))>w;
        
        if ~any(d(:))
           I=I_stable-I_first;
        else
            I=NaN;
        end
    else
        I=temp2;
    end
    
else 
    I=nan;
end
end