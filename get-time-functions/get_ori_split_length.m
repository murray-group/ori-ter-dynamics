function len=get_ori_split_length(cycle_list,cycle_ids,fluor_channel,cut_off,nframes)

%%%Finds the frame at which 2 ori are first observed. First uses
%%%get_time_first_1_to_2_split. Then for cells in which that function
%%%returned NaN or a frame less than or equal to cut_off, the function goes back nframes into to the parent cell and looks
%%%in the appropriate cell half of that cell. The parent cell need not be
%%%in cycle_ids

%nominal values cut_off=4, nframes=9

%fluor_channel=2;
c=0;
c2=0;
t=NaN(size(cycle_ids));
len=NaN(size(cycle_ids));


for i=1:length(cycle_ids)
    t(i)=get_time_first_1_to_2_split(cycle_list(cycle_ids(i)),2);
    cycle=cycle_list(cycle_ids(i));
    if ~isnan(t(i)) && ~isempty(t(i))
        len(i)=cycle.lengths(t(i));
    end
    if isnan(t(i)) || t(i) <=cut_off
        %if isnan(b(i)) && sum(~isnan(cycle.(['foci',num2str(fluor_channel)]).position(1,:,1)),2)==2
        
        c=c+1;
        parent=cycle.relation_graph.parent;
        if isnan(parent)
            c2=c2+1;
            continue;
        end
        I=find(cycle_list(parent).relation_graph.children==cycle_ids(i));% I=1, the child is the lower cell (large position values), for I=2 the child is the upper cell (lower position values)
        if isempty(cycle_list(parent).(['foci',num2str(fluor_channel)]).position)
            continue;
        end
        
        positions=cycle_list(parent).(['foci',num2str(fluor_channel)]).position_relative(:,:,1);
        if I==1
            positions(positions<0)=nan;
        else
            positions(positions>0)=nan;
        end
        
        n_spots_parent=sum(~isnan(positions),2);
        n_spots_child=sum(~isnan(cycle.(['foci',num2str(fluor_channel)]).position(:,:,1)),2);
        
        %nframes=9;%the number of frames to go back into the mother cell
        n=max(1,length(n_spots_parent)-nframes);
        n_spots_parent=n_spots_parent(n:end);
        n_spots=[n_spots_parent; n_spots_child];%combine both cell cycles
        temp_len=[cycle_list(parent).lengths(n:end);cycle.lengths];
        
        I=find(n_spots==2,1);%%the first frame at which there are 2 foci
        if ~isempty(I)
            if isempty(find(n_spots==1,1)) || find(n_spots==1,1)>I || any(n_spots(1:I)>2)
                I=nan;
            end
        end
        if isempty(I) || isnan(I)
            len(i)=nan;
        else
            len(i)=temp_len(I);
        end
        
        
        %             I=find(n_spots==2,1,'first')-length(n_spots);% 0 if the last frame, -1 of the second last
        %             if ~isempty(I)
        %                 b(i)=I;
        %             end
    end
end
end