function I=get_first_time_at_middle(cycle,fluor_channel,w)
if isfield(cycle.(['foci',num2str(fluor_channel)]),'position_relative') && ~isempty(cycle.(['foci',num2str(fluor_channel)]).position_relative)
        pos=cycle.(['foci',num2str(fluor_channel)]).inferred_position_relative(:,:,1);
        c=any(abs(pos)<w,2);% is any spot in the middle wth of cell in each frame
        I=find(c==1,1);%the first time when 3 consecutive frames have matp at middle wth
        if (I>cycle.duration)
            I=nan;
        elseif isempty(I)
            I=nan;
        end
else
    I=nan;
end

end