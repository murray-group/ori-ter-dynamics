function b=get_MatP_stable_at_middle(cycle_list,cycle_ids,fc,w,nframes)

%%% Finds the frame at which MatP is first stably in the middle. This
%%% occurs when MatP is within a distance w pixels of mid-cell. The code goes
%%% back nframes into the mother cell and looks for when a MatP focus is
%%% within w pixels of the appropriate quarter position. This is appended
%%% to the result from the cell itself and the frame from which 3 consecutive
%%% satisfy the condition is returned.

%%% The parent cell need not be in cycle_ids

%%%nominam values: nframes=5, w=3.6

fluor_channel=fc;
%w=3.6;

b=NaN(size(cycle_ids));
c=0;
c2=0;

for i=1:length(cycle_ids)
    %i
    cycle=cycle_list(cycle_ids(i));
    if isfield(cycle.(['foci',num2str(fluor_channel)]),'position_relative') && ~isempty(cycle.(['foci',num2str(fluor_channel)]).position_relative)
        b(i)=get_stable_time_at_middle(cycle,fluor_channel,w);
        if b(i)==1
            c=c+1;
            parent=cycle.relation_graph.parent;
            if isnan(parent)
                c2=c2+1;
                continue;
            end
            I=find(cycle_list(parent).relation_graph.children==cycle_ids(i));% I=1, the child is the lower cell (large position values), for I=2 the child is the upper cell (lower position values)
            
            if isempty(cycle_list(parent).(['foci',num2str(fluor_channel)]).position) %skip if it has no foci (in which case position_relative may not have been created)
                continue;
            end
            
            positions_parent=cycle_list(parent).(['foci',num2str(fluor_channel)]).position_relative(:,:,1);
            
            q=0.5*cycle_list(parent).lengths/2;%quarter positions
            I2=any(abs(positions_parent+q*(2*I-3))<w,2);% is any spot in the appropriate quarter
            
            positions=cycle.(['foci',num2str(fluor_channel)]).position_relative(:,:,1);
            I3=any(abs(positions)<w,2);% is any spot in the middle
            
            %nframes=5;%the number of frames to go back into the mother cell
            n=max(1,length(I2)-nframes);
            I2=I2(n:end);
            I4=[I2; I3];%combine both cell cycles
            
            for j=1:length(I4)-2
                temp1(j)=sum(I4(j:j+2)); %creates a temporary variable with  the sum of logical values for 3 indices from c
            end

            I=find(temp1==3,1);%the first time when 3 consecutive frames have matp at middle wth (stably at middle)
            
%             temp1=I4;%first time at middle
%             I=find(temp1==1,1);%first time at middle
            
            I=I-length(I2);
            
            if isempty(I)
                b(i)=nan;
            else
                b(i)=I;
            end
        end
    end
end
end