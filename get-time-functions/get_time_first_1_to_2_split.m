function I=get_time_first_1_to_2_split(cycle,fluor_channel)

        n_spots=sum(~isnan(cycle.(['foci',num2str(fluor_channel)]).position(:,:,1)),2);
        I=find(n_spots==2,1);%%the first frame at which there are 2 foci
        if ~isempty(I)
        if isempty(find(n_spots==1,1)) || find(n_spots==1,1)>I || any(n_spots(1:I)>2)
            I=nan;
        end
        end
        if isempty(I)
            I=nan;
        end
end