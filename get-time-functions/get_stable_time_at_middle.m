function I=get_stable_time_at_middle(cycle,fluor_channel,w)
if isfield(cycle.(['foci',num2str(fluor_channel)]),'position_relative') && ~isempty(cycle.(['foci',num2str(fluor_channel)]).position_relative)
        pos=cycle.(['foci',num2str(fluor_channel)]).position_relative(:,:,1);
        c=any(abs(pos)<w,2);% is any spot in the middle wth of cell in each frame
        temp1=[];
        for i=1:length(c)-2
            temp1(i)=sum(c(i:i+2)); %creates a temporary variable with  the sum of logical values for 3 indices from c
        end
        I=find(temp1==3,1);%the first time when 3 consecutive frames have matp at middle wth
        if (I>cycle.duration)
            I=nan;
        elseif isempty(I) %|| I==1
            I=nan;
        end
else
    I=nan;
end
end