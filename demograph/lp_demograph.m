function demograph(cycle_list,complete_cycle_ids,fluor_index)

complete_cycles=cycle_list(complete_cycle_ids); % Import details of those cell cycles that are identified as complete cycles
min_len=20;
max_len=60;
n_bins=max_len-min_len+1;
data=zeros(n_bins,max_len);
n_data=zeros(1,n_bins);
bin_edges=min_len:1:max_len+1;
lp=strcat("fluor",num2str(fluor_index),"_lp");
for i=1:numel(complete_cycles)
    if(complete_cycles(i).lengths>=20 & complete_cycles(i).lengths<=60)
        [~,~,bins]=histcounts(complete_cycles(i).lengths,bin_edges);
        for j=1:complete_cycles(i).duration
            lp_length=numel(complete_cycles(i).(lp){j});
            lp_offset=round((max_len-lp_length)/2)+1;
            if (complete_cycles(i).pole == 1) % preferred pole
                data(bins(j),lp_offset:(lp_offset+lp_length-1))=data(bins(j),lp_offset:(lp_offset+lp_length-1))+complete_cycles(i).(lp){j}';
            elseif (complete_cycles(i).pole == -1) % if pole is -1, then flip the line profile elements to match the pole as +1
                data(bins(j),lp_offset:(lp_offset+lp_length-1))=data(bins(j),lp_offset:(lp_offset+lp_length-1))+flip(complete_cycles(i).(lp){j}');
            end
            n_data(bins(j))=n_data(bins(j))+1;
        end
    end
end
data=data./n_data';
y=bin_edges*0.067;
x=linspace(0.5,-0.5,n_bins-1);
figure
imagesc(x,y,data)
ylabel("Cell length (um)")
xlabel("Relative position inside cell")
end